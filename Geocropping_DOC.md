# Documentation GeoCropping

[Sources du site](https://framagit.org/renaudzigmann/Geocropping/tree/master/Website)

[Sources de l'app](https://framagit.org/renaudzigmann/Geocropping/tree/master/MobileApp/GeocroppingApp)

[Doc en ligne](https://framagit.org/renaudzigmann/Geocropping/tree/master/)

[Site](https://geocropping.xsalto.com/)

[Site dev](https://geocropping-dev.xsalto.com/)

# Site Web

## Présentation du projet

GeoCropping est une site web permettant la validation de données
[OpenStreetMap](https://www.openstreetmap.org/) autour de l’utilisateur,
permettant ainsi une maintenance des bases de données facilement. Il
utilise la library [Leaflet](http://leafletjs.com/) ainsi que des
[plugins](http://leafletjs.com/plugins.html) proposés par cette library.

Le fonctionnement est le suivant : lors de la géolocalisation de
l’utilisateur, on scanne les alentours pour afficher les objets s’y
trouvant, on efface ensuite les objets lorsque l’utilisateur s’en
éloigne trop (noter que la distance d’affichage est plus petite que la
distance de suppression).

Pour l’affichage et les requêtes, voici le fonctionnement : On interroge
[Overpass API](http://wiki.openstreetmap.org/wiki/Overpass_API) sur les
positions des nodes proches, si il en existe, on interroge ensuite
directement la base de donnée d’OSM (OpenStreetMap) pour savoir la
couleur de l’élément à afficher.

Le code couleur est le suivant :

La validation se fait en ajoutant une tag survey:date (ou source =survey
& source:date=…) à l’élément sur OSM ou en le mettant à jour.

L’invalidation quant à elle se fait en ajoutant un tag
[fixme](http://wiki.openstreetmap.org/wiki/Key:fixme) à l’objet

La connexion à la base de donnée d’OSM se fait par le protocole
[Oauth](https://fr.wikipedia.org/wiki/OAuth). Celui-ci se fait en trois
étapes :

1.  Demande d’identifiants de connexion au près du server à l’aide des
    identifiants d’application.
2.  Demande de connexion de l’utilisateur sur OSM. On envoi
    l’utilisateur sur le site pour qu’il s’y connecte, puis il est
    redirigé sur GeoCropping (le callback).
3.  Demande de l’access token à OSM pour récupérer la session
    de l’utilisateur.

Une fois ces étapes accomplie, l’utilisateur peu enfin executer des
requêtes sur le server OSM.

Lors d’une invalidation, l’utilisateur a la possibilité de cocher une
checkbox pour « suivre » le point, lorsque cela est fait, on ajout une
nouvelle préférence OSM à l’utilisateur. Les préférences sont de la
forme clé,valeur avec la clé qui est l’identifiant du node à suivre et
la valeur la version du node au moment de son ajout.

En cours : Lorsque l’on dézoom suffisament, la carte devrait afficher
les nodes avec juste un plus petit point de couleur non clickable au
lieu des cercles habituels. Et ce dans un cercle bien plus large que
lorsque l’on parcours tout simplement les rues. Cela a pour but de
pouvoir suivre l’avancement de la zone et d’avoir une vue d’ensemble.

## IHM

L’IHM est volontairement simplifié, nous avons donc une carte avec des
boutons de zoom/dézoom en haut à gauche, l’utilisateur localisé au
centre de l’écran. Le déplacement sur la carte est bloqué, il se
recentre automatiquement sur l’utilisateur.

Lors de ses déplacements, l’utilisateur va voir apparaître des cercles
de couleurs sur la carte. Ces cercles représentent les objets (nodes)
déclaré autour de lui et, selon le code couleur, à valider ou non.

Le menu de droite est ouvrable en clickant sur le bouton en haut à
droite. Lorsque celui-ci s’ouvre, on y découvre :

- un bouton de connexion/déconnexion

- des statistiques sur l’utilisateur : le nombre de
validations/signalements qu’il a fait

- le choix du server (Mondial ou Français actuellement)

- une option « Utiliser la localisation automatique » permettant de désactiver/activer la
géolocalisation et le blocage de la carte sur l’utilisateur. Une fois
cette option décochée, l’utilisateur peut alors changer sa position sur la
carte par un simple clic à l’endroit où il souhaite être localisé.

- une option « Dev API », disponible uniquement sur la version dev du
site et permettant de se connecter au server dev de OSM pour pouvoir
faire des tests (désormais cachée car obsolète mais peut être
dé-commentée et mise à jour).

- un lien « Mode d’emploi » permettant d’accéder à une explication sur
le fonctionnement du site, le code couleur...etc…

- un lien « Framagit » redirigeant vers le dépôt en ligne avec les
sources du site.

- un lien vers la liste des commits

Si l’utilisateur est connecté à son compte OpenStreetMap, il verra son
avatar apparaître sous l’icône du menu.

Des mini fenêtre de notification apparaissent dans le coin inférieur
droit de l’écran pour confirmer ou non si les actions que l’utilisateur
fait s’effectuent bien.

## Code et overpass API

Documentation générée par typedoc disponible
[ici](https://geocropping-dev.xsalto.com/doc/)

.ts : sources en typescript ou juste en javascript.

-   Geocropping.ts :

Fichier principale du projet, il contient la class Geocropping ainsi que
des fonctions concernant : l’initialisation de la carte, la connexion et
le renseignement sur l’utilisateur, la localisation, la recherche et
l’affichage des données ainsi que quelques autre fonctions utiles.

IsMapCreated() : qui vérifie si la map est instanciée

createMap(Map) : crée la map et l’affiche

connect() : Connecte l’utilisateur à OSM

isConnect() : Vérifie si l’utilisateur est connecté

userDisconnect() : Déconnecte l’utilisateur d’OSM

setClickPosition(active:boolean) : activer/désactiver la position au
click

\_locationFoundClickFunction() : Créer ou récupérer la fonction de call
back du clic (pour pouvoir arrêter l'event)

startLocateUser() : Commence la geolocalisation

stopLocateUser() : Arrête la geolocalisation

\_locationFound(data : L.LocationEvent, options?) : Définir la position
du marker de l'utilisateur et faire la recherche des éléments autour

\_locationError(options, data) : Afficher l'erreur de géolocalisation

\_searchDataAround(pos: L.LatLng) : Recherche des éléments autour de
l’utilisateur

\_searchInfoElementsOSM() : Va récupérer les infos des éléments
directement sur OSM

\_updateElementsDisplayed(data: { any?}) : Met à jour les éléments
autour de l’utilisateur.

\_checkChanges() : Recherche les éléments que l’utilisateur a demandé à
suivre. Pour cela on récupère les préférencesde l’utilisateur où sont
stockés les nodes que celui-ci veut suivre, puis on regarde si ces nodes
ont une version supérieure actuellement. Si oui on les affiches dans une
notifications. Enfin on supprime des préférences les éléments qui ont
été affichés dans la notification.

\_removeChanges(resPref, changes: number\[\]) : Enlève de resPref les
éléments présent dans changes puis envoi les nouvelles préférences.

\_ajoutPreferenceUnique(id, version) : Ajoute un élément à suivre aux
préférences de l’utilisateur

\_getStats() : Récupère les statistiques du nombre de validations et
signalement dans les préférences utilisateur osm et les met à jour dans
l’affichage. Si l’utilisateur n’a pas pas encore de préférences, les
crée.

isFixmeFromUs(contenuDuFixme: string) : renvoi vrai si la chaine de
caractères donnée en paramètre est égale à l’une des valeurs que
l’application donne au tag fixme

Les deux fonctions suivantes permettent de voir les points
sur une large zone (en temps que points et non cercle de couleurs)
lorsque l’on dézoom suffisament. Toutes les fonctions non terminées
relatives à cette fonctionnalité sont repéré par le commentaire
« ENCOURS » dans le code.

\_searchDataFar(pos: L.LatLng) : même fonction que \_searchDataAround
mais sur une zone plus élargie (un zone encore plus grande pose des
problèmes de poids de requête.

\_updateElementsDisplayedFar(data: { any?}) : même fonction que
\_updateElementsDisplayed sur une zone plus élargie. Impossible
cependant de récupéré des infos directement sur osm comme cette dernière
car trop de nodes récupéré ici (solution possible → les récupérer par
tranche de 250?)

\_searchData(pos: L.LatLng) : redirige vers la bonne fonction de
recherche en fonction du zoom

onMobileApp : Booleen permattant de savoir quand le site est affiché dans l'application (faux par défaut)

\_setOnMobileApp() : Fonction applée au lancement de l'application pour mettre onMobileApp à vrai.

MobileSetGPSPosition(active : boolean) : Fonction appelée depuis l'application lorsque le GPS change de statut dans l'application. Elle a le même effet que lorsque l'on décoche l'option "Utiliser la localisation automatique" mais s'occupe de cocher/décocher automatiquement cette option.

-   MyNotification.ts :

Ce fichier contient une classe de gestion des notifications
apparraissant en bas à gauche de l’écran et validant (ou non) les
actions de l’utilisateur. Pour ce faire, nous utilisons le plugin
[MessageBox](https://github.com/tinuzz/leaflet-messagebox) de leaflet.

Le fonctionnement est très simple, dans le constructeur on fait deux
choses :

- On crée une variable box dans la classe en attanchant une messageBox à
la map. Cette message box est créée avec la variable option créée plus
haut. Celle-ci contient une position qui définit où seront affiché les
notification et un timeout définissant le temps d’affichage des
notifications.

- On crée un tableau notif où l’on stock les messages qui seront affiché
en fonction des clés qu’ont lui donne.

Enfin on a une petite liste de méthodes permettant l’affichage des
différents message simplement et rapidement.

Pour utiliser cette class et afficher des notifications, il suffit de
l’instancier avec la map leaflet en paramêtre et d’appeler les fonctions
d’affichage sur cet objet.

-   OauthOsm.ts :

Fichier contenant l’implémentation des fonctions en typescript gérant
les tokens, la connexion et les requêtes au server d’osm.

La classe OauthOsm :

isConnected() : Permet de savoir si l’utilisateur est connecté

apiRequest() : Template des requêtes qui peuvent être envoyées

\_sendRequest(apiRequest) : Envoyer une requete a OSM

getServerDetails() : Récupérer les informations du server

getUserDetails() : Récupérer les détails de l’utilisateur connecté

connect() : Ouvre la fenêtre de connection

disconnect() : déconnection de l’utilisateur

isTokenReady() :

\_deleteTokens() :

function getCookies(name: string | string\[\] = \[\]): { key: string,
value: string }\[\] : récupération des cookies

function deleteCookie(name: string, path = "") : Suppression du cookie
que l’on souhaite

function localSave(name: string, value: string, options: {
useLocalStorageOrCookie?: boolean, useLocalStorage?: boolean,
useCookie?: boolean, systemUsed?: any, cookie?: { expires?: string,
path?: string } } = {}) : Sauvegarde de donnée sur le navigateur

function localLoad(name: string, options: { useLocalStorageOrCookie?:
boolean, useLocalStorage?: boolean, useCookie?: boolean, cookie?: {},
systemUsed?: any } = {}): string : Récupération de données sur le
navigateur

function localDelete(name, options: { useLocalStorage?: boolean,
useCookie?: boolean, cookie?: { path?: string } } = {}) : Suppression de
données sur le server local

-   fr.js :

Fichier de traduction en français.

-   index.ts :

Gestion du menu extensible à droite de l’écran, et du potentiel futur
gauche.

toggleRightMenu() : Ouvre/ferme le menu de droite selon son état courant

toggleLeftMenu() : Ouvre/ferme le menu de gauche selon son état courant
(inutilisé actuellement mais disponible pour extension future)

closeLeftMenu() : ferme le menu de gauche

closeRightMenu() : ferme le menu de droite

openLeftMenu(width?: number) : ouvre le menu de gauche

openRightMenu(width?: number) : ouvre le menu de droite

-   osmtogeojson.js :

Fichier trouvé sur le [net](https://github.com/tyrasd/osmtogeojson).
Transforme les données récupérées d’overpass API en GeoJSon.

-   tsconfig.json :

Contient les options de configuration de typescript.

-   tsd.json :

Info typescript.

Others Licences : Dossier contenant les licences des codes sources
utilisés.

Auth : Ce dossier gère tout ce qui est relatif à la connexion et aux
requêtes avec OSM.

-   Array2XML.php :

Librairie trouvé sur le
[net](http://www.lalit.org/lab/convert-php-array-to-xml-with-attributes/)
permettant la conversion d’array php en XML.

-   OAuth.php :

Librairie trouvé sur le [net](https://gist.github.com/netdna/2210583)
avec plusieurs fonctions utiles à la connexion.

-   OsmConsumer.php :

Contient les identifiants de l’application pour les deux modes : dev et
prod.

-   callback.html

-   geocropping.php :

On implémente ici les étapes de la connexion de protocole Oauth.

class Geocropping

encrypteTokenValue(\$tokenValue) : encrypte le token passé en paramètre

decrypteTokenValue(\$tokenValue) : décrypte le token passé en paramètre

requestToken(\$params = \[\]) : étape 1 du protocole Oauth, demande de
récupération d’un token pour identifier la connexion au près du server

getAuthorizeUrl(\$token = null, \$callback = "") : étape 2 du protocole
Oauth, récupération de l’adresse pour la connexion de l’utilisateur

getAccessToken(\$token = null) : étape 3 du protocole Oauth, on récupère
la session de l’utilisateur

\_\_toString() : Converti la classe en string

private requestAPI(\$endpoint, \$token = null, \$params = array(),
\$method = 'GET', \$content = '') :

getMime(\$data) : récupérer le type de \$data

private parseHeaders(\$headers) : parse le header de la requête
(récupère les infos du header)

handleRequestAPI(\$command, \$options) : Gestion de toutes les
différentes requêtes

-   index.php :

Ce fichier sert à gérer les différents requêtes utilisant les tokens.

css : fichiers de style

doc : Contient les fichiers de la doc générée accessible avec le lien du
début de la partie 3 de cette doc

img : Contient les images utiles au site.

js : Contient le javascript compilé

Le fichier index.php situé à la *source* du projet contient du code
commenté permettant d’afficher une option « dev API » qui remplace
l’accès au server OSM par un accès au server OSM dev permettant de faire
des modification sans impacter la vrai carte OSM. Bien utile pour faire
des tests, décommenter pour l’utiliser. (Normalement déjà présent sur le
server geocropping-dev sans avoir besoin d’être décommenté)


# Appli Mobile

## Construction

L'application est constituée d'une [WebView](https://developer.android.com/reference/android/webkit/WebView.html) affichant [GeoCropping](https://geocropping.xsalto.com/).
Dans les sources typescript du site, un booleen _onMobileApp a été ajouté, par défaut sur faux. Lors de l'ouverture de l'application, on appelle depuis cette dernière (depuis le code java) une fonction _setOnMobileApp permettant de mettre le booleen à vrai et ainsi savoir dans le site web quand on est affiché sur appli mobile.

Lorsque le gps est désactivé sur le téléphone, cela déclenche un évenement qui appelle la fonction MobileSetGPSPosition(active: Boolean) qui désactive la position GPS sur le site (et donc active la position au click automatiquement).