package geocropping.com.geocroppingapp;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class DebugPerso {

    AlertDialog.Builder build;
    AlertDialog alert;

    public DebugPerso(Context context){
        this.build = new AlertDialog.Builder(context,R.style.MyDialogTheme);
    }

    public void afficherMessage(String str) {
        this.build.setMessage(str).setCancelable(true);
        this.alert = this.build.create();
        alert.show();
    }

    public void afficherMessage(String str, String btnOui) {
        this.build.setMessage(str).setCancelable(true).setPositiveButton(btnOui, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alert.cancel();
            }
        });
        this.alert = this.build.create();
        alert.show();
    }

}
