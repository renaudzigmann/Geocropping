package geocropping.com.geocroppingapp;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NetworkChangeReceiver extends BroadcastReceiver {

    private static OnChangeReceivedListener listener;

    public interface OnChangeReceivedListener{
        void onChangeReceived();
    }

    public void setOnChangeReceivedListener(OnChangeReceivedListener listener){
        NetworkChangeReceiver.listener = listener;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        //if(listener != null){
        boolean isConnected = NetworkUtil.getConnectivityStatusString(context);
        if(!isConnected)
            NetworkChangeReceiver.listener.onChangeReceived();
        //}
    }
}