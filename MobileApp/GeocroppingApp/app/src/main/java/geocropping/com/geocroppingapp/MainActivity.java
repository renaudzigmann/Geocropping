package geocropping.com.geocroppingapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
// TODO : TOAST
public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_GPS = 0;
    private static final int REQUEST_WIFI = 1;

    private WebView wv;
    private Context context;

    // Gestion du GPS
    private LocationManager locationManager;
    private LocationListener locationListener;
    private NetworkChangeReceiver receiver;


    // Quand on fait retour, si on l'a autorisé fait un retour sur la webview
    @Override
    public void onBackPressed() {
        if (wv.canGoBack()) {
            wv.goBack();
        } else {
            // demander si on veut vraiment quitter et le faire si c'est le cas
            final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
            builder.setMessage("Voulez-vous vraiment quitter?")
                .setCancelable(false)
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                });
            final AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private boolean isGPSOn(){
        return (((LocationManager) getSystemService( Context.LOCATION_SERVICE ))).isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void buildAlertMessageNoConnection(final Context context, final WebView wv){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        builder.setMessage("Connexion perdue, l'application ne peut pas fonctionner correctement.").setCancelable(false).setPositiveButton("Recharger", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                boolean IsConnected = NetworkUtil.getConnectivityStatusString(context);
                if(IsConnected)
                    wv.reload();
                else
                    buildAlertMessageNoConnection(context, wv);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setMessage("Le GPS semble être désactivé, l'activer?")
                .setCancelable(false)
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        //boolean IsLocalised = (((LocationManager) getSystemService( Context.LOCATION_SERVICE ))).isProviderEnabled(LocationManager.GPS_PROVIDER);
                        if ( !isGPSOn() ) {
                            callJS("geocropping.MobileSetGPSPosition(false);");
                        }else{
                            callJS("geocropping.MobileSetGPSPosistion(true);");
                        }
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void callJS(String js) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            wv.evaluateJavascript(js, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                    // nothing to see here
                }
            });
        } else {
            wv.loadUrl("javascript:" + js);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_GPS: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // La permission est donnée
                    configureGPS();
                } else {
                    // La permission est refusée
                    this.finish();
                }
                return;
            }
            case REQUEST_WIFI: {
                // ajouter ici le paramétrage du wifi si il y en a un un jour

            }
        }
    }

    private void configureGPS() {
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
                callJS("geocropping.MobileSetGPSPosition(true);");
            }

            @Override
            public void onProviderDisabled(String s) {
                callJS("geocropping.MobileSetGPSPosition(false);");
            }
        };
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        // Détection du on/off du GPS
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);

    }

    @Override
    protected  void onStop() {
        super.onStop();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.removeUpdates(locationListener);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        configureGPS();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.context = this;
        setContentView(R.layout.activity_main);
        wv = findViewById(R.id.wv);

        // vérification que les droits sont bien donné pour l'accès au gps
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_GPS);
        }else{
            configureGPS();
        }

        // De même avec le wifi
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, REQUEST_WIFI);
        }

        // Vérification de la connexion internet au lancement
        boolean IsConnected = NetworkUtil.getConnectivityStatusString(this.context);
        if(!IsConnected) {
            buildAlertMessageNoConnection(this.context, this.wv);
        }

        // Vérification du GPS au lancement
        //boolean IsLocalised = (((LocationManager) getSystemService( Context.LOCATION_SERVICE ))).isProviderEnabled(LocationManager.GPS_PROVIDER);
        if ( !isGPSOn() ) {
            buildAlertMessageNoGps();
        }

        // Parametrage de la WebView
        WebSettings wvSettings = wv.getSettings();

        wv.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);
                return true;
            }
        });

        // Activer javascript
        wvSettings.setJavaScriptEnabled(true);

        wv.setFocusable(true);
        wv.setFocusableInTouchMode(true);

        // Mettre la render priority en haute
        wvSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);

        wvSettings.setDomStorageEnabled(true);
        wvSettings.setGeolocationEnabled(true);
        wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        wv.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
            }
        });

        // Charger la page
        wv.loadUrl("https://geocropping.xsalto.com/");

        // Pour appler du Js, il faut que la page ai fini de charger
        wv.setWebViewClient(new WebViewClient(){
            // TODO : Ici quand on a finit de charger la wv on peut envoyer un event au splash screen et du coup afficher à ce moment là
            @Override
            public void onPageFinished(WebView wv, String url){
                callJS("geocropping._setOnMobileApp();");
            }
        });

        this.receiver = new NetworkChangeReceiver();
        this.receiver.setOnChangeReceivedListener(new NetworkChangeReceiver.OnChangeReceivedListener(){
            @Override
            public void onChangeReceived(){
                buildAlertMessageNoConnection(context, wv);
            }
        });

        // Permet de récupérer les rapports d'erreur : https://appcenter.ms/orgs/Xsalto/apps/GeoCropping-1/crashes/groups
        AppCenter.start(getApplication(), "136e9a5d-b497-4a82-9169-008ef2a8830f", Analytics.class, Crashes.class);

        // Pour pouvoir débug sur chrome (chrome://inspect)
        //if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
        //    WebView.setWebContentsDebuggingEnabled(true);
        //}

    }

}