/// <reference path="typings/tsd.d.ts" />
/// <reference path="myTypings/tsd.d.ts" />
/// <reference path="myTypings/OauthOsm.d.ts" />

/**
 * Gerer les requete pour OSM
 * 
 * @class OauthOsm
 */
class OauthOsm {

    /**
     * Url pour effectuer les requetes
     * 
     * @type {string}
     * @memberof OauthOsm
     */
    _baseUrl: string;

    /**
     * Utiliser le serveur OSM de dev
     * 
     * @type {boolean}
     * @memberof OauthOsm
     */
    _devApi: boolean = false;

    /**
     * Savoir si l'utilisateur est connecté
     * 
     * @type {boolean}
     * @memberof OauthOsm
     */
    _connected: boolean = false;

    /**
     * Savoir si l'utilisateur est connecté
     * 
     * @readonly
     * @memberof OauthOsm
     */
    get isConnected() { return this._connected };

    /**
     * Creates an instance of OauthOsm.
     * @memberof OauthOsm
     */
    constructor() {
        // Vérifier si l'utilisateur était déjà connecté
        this.getUserDetails();
        this._baseUrl = window.location.href + 'auth/';
    }


    /**
     * Template des requêtes qui peuvent être envoyées
     * 
     * @readonly
     * @static
     * @type {IDictionary<OauthOsm.ApiRequest>}
     * @memberof OauthOsm
     */
    static get apiRequest(): IDictionary<OauthOsm.ApiRequest> {
        return {
            // http://wiki.openstreetmap.org/wiki/API_v0.6
            GetServerCapabilities: {
                method: 'GET',
                url: '/',
                request: 'GetServerCapabilities',
                params: {}
            },
            // online=true
            GetPermissions: {
                method: 'GET',
                url: '/',
                request: 'GetPermissions',
                params: {},
                need_connection: true
            },
            GetDataBoundsBox: {
                method: 'GET',
                url: '/',
                request: 'GetDataBoundsBox',
                params: {
                    left: 0,
                    bottom: 0,
                    right: 0.5,
                    top: 0.5
                }
            },
            // online=true
            GetUserDetails: {
                method: 'GET',
                url: '/',
                request: 'GetUserDetails',
                params: {},
                need_connection: true
            },
            // online=true
            GetUserPreferences: {
                method: 'GET',
                url: '/',
                request: 'GetUserPreferences',
                params: {},
                need_connection: true
            },
            // online=true
            UpdateUserPreferences: {
                method: 'POST',
                url: '/',
                request: 'UpdateUserPreferences',
                params: {
                    content: JSON.stringify({
                        preferences: {
                            preference: [ //enlever ça pour nettoyer les preferences
                                //{ '@attributes': {k:'', v:''} }
                            ]
                        }
                    }),
                },
                need_connection: true
            },
            // online=true
            UpdateSingleUserPreference: {
                method: 'POST',
                url: '/',
                request: 'UpdateSingleUserPreference',
                params: {
                    key:"",
                    value:""
                },
                need_connection: true
            },
            GetUserId: {
                method: 'GET',
                url: '/',
                request: 'GetUserId',
                params: {
                    id: 1
                }
            },
            GetNoteId: {
                method: 'GET',
                url: '/',
                request: 'GetNoteId',
                params: {
                    id: 1,
                    format: ''
                }
            },
            GetNotesBoundsBox: {
                method: 'GET',
                url: '/',
                request: 'GetNotesBoundsBox',
                params: {
                    left: 0,
                    bottom: 0,
                    right: 10,
                    top: 10,
                    format: ''
                }
            },
            SearchNotes: {
                method: 'GET',
                url: '/',
                request: 'SearchNotes',
                params: {
                    query: 'hello',
                    limit: 100, // 1 -> 10000
                    closed: 7, // Specifies the number of days a bug needs to be closed to no longer be returned
                    // A value of 0 means only open bugs are returned. A value of -1 means all bugs are returned.
                    format: ''
                }
            },
            GetChangesetId: {
                method: 'GET',
                url: '/',
                request: 'GetChangesetId',
                params: {
                    discussion: true,
                    id: 1
                }
            },
            GetChangeOfChangesetId: {
                method: 'GET',
                url: '/',
                request: 'GetChangeOfChangesetId',
                params: {
                    id: 1
                }
            },
            GetElementId: {
                method: 'GET',
                url: '/',
                request: 'GetElementId',
                params: {
                    type: 'node', // [node|way|relation]
                    id: 1
                }
            },
            GetElementIdHistory: {
                method: 'GET',
                url: '/',
                request: 'GetElementIdHistory',
                params: {
                    type: 'node', // [node|way|relation]
                    id: 1
                }
            },
            GetElementIdVersion: {
                method: 'GET',
                url: '/',
                request: 'GetElementIdVersion',
                params: {
                    type: 'node', // [node|way|relation]
                    id: 1
                }
            },
            GetElementsId: {
                method: 'GET',
                url: '/',
                request: 'GetElementsId',
                params: {
                    ids: [100, 102],
                    type: 'nodes' // [node|way|relation]
                }
            },
            GetElementIdRelations: {
                method: 'GET',
                url: '/',
                request: 'GetElementIdRelations',
                params: {
                    type: 'node', // [node|way|relation]
                    id: 100
                }
            },
            GetNodeIdWays: {
                method: 'GET',
                url: '/',
                request: 'GetNodeIdWays',
                params: {
                    id: 1
                }
            },
            GetElementIdFull: {
                method: 'GET',
                url: '/',
                request: 'GetElementIdFull',
                params: {
                    type: 'way', // [way|relation]
                    id: 1
                }
            },



            CreateNote: {
                method: 'POST',
                url: '/',
                request: 'CreateNote',
                params: {
                    lat: 0,
                    lon: 0,
                    text: ''
                }
            },
            CreateNoteComment: {
                method: 'POST',
                url: '/',
                request: 'CreateNoteComment',
                params: {
                    id: 0,
                    text: ''
                }
            },
            CloseNote: {
                method: 'POST',
                url: '/',
                request: 'CloseNote',
                params: {
                    id: 0,
                    text: ''
                }
            },
            ReopenNote: {
                method: 'POST',
                url: '/',
                request: 'ReopenNote',
                params: {
                    id: 0,
                    text: ''
                }
            },


            CreateChangeset: {
                method: 'POST',//PUT
                url: '/',
                request: 'CreateChangeset',
                params: {
                    element: JSON.stringify({
                        changeset: {
                            tag: [
                                //{ '@attributes': {} }
                            ]
                        }
                    })
                }
            },
            UpdateChangeset: {
                method: 'POST',//PUT
                url: '/',
                request: 'UpdateChangeset',
                params: {
                    //element: `{"changeset":{"tag":{"@attributes":{"k":"addr:housenumber","v":"33"}}}}`,
                    element: JSON.stringify({
                        changeset: {
                            tag: [
                                //{ '@attributes': {} }
                            ]
                        }
                    }),
                    id: 0
                }
            },
            CloseChangeset: {
                method: 'POST',//PUT
                url: '/',
                request: 'CloseChangeset',
                params: {
                    id: 0
                }
            },
            CreateElement: {
                method: 'POST',//PUT
                url: '/',
                request: 'CreateElement',
                params: {
                    //element: JSON.stringify({"node":{"@attributes":{"changeset":"47228727","lat":"44.0936004","lon":"6.2338778"},"tag":{"@attributes":{"k":"addr:housenumber","v":"33"}}}}),
                    element: '',
                    type: ''
                }
            },
            UpdateElement: {
                method: 'POST',//PUT
                url: '/',
                request: 'UpdateElement',
                params: {
                    //element: JSON.stringify({"node":{"@attributes":{"changeset":"47228727","lat":"44.0936004","lon":"6.2338778"},"tag":{"@attributes":{"k":"addr:housenumber","v":"33"}}}}),
                    before: '',
                    after: '',
                    changeset: 0
                }
            },
        };
    }

    /**
     * Envoyer une requete a OSM
     * 
     * @param {OauthOsm.ApiRequest} apiRequest 
     * @returns 
     * @memberof OauthOsm
     */
    _sendRequest(apiRequest: OauthOsm.ApiRequest): JQueryPromise<{}> {
        let deferred = $.Deferred();
        if (typeof apiRequest !== 'object' || apiRequest.url === undefined || apiRequest.method === undefined) {
            deferred.reject({
                message: 'Invalid argument',
                request: apiRequest
            });
            deferred.rejectWith(this);
        }

        let data = {
            action: 'request_api',
            command: apiRequest.request,
            options: apiRequest.params
        };
        let url = this._baseUrl;
        if (this._devApi) {
            url += '?devMode=true';
        }

        $.ajax(url, {
            type: apiRequest.method,
            data: data
        }).done((data) => {
            //console.trace('myGeocropping', 'request ok connected:', this._connected);

            if (this._connected === false && apiRequest.need_connection === true) {
                this._connected = true;
                this.connect();
            }
            deferred.resolveWith(this, [data]);
        }).fail((jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {

            if (jqXHR.status === 401) {
                // not login
                this.disconnect();
            } else if (jqXHR.status === 403) {
                // Login was successful but the user has been blocked, an application should display the error message (which will be translated if necessary)
                // and, if it has an end-user UI, provide an easy way to access openstreetmap.org and view any messages there.

            }
            deferred.rejectWith(this, [jqXHR]);
        });

        return deferred.promise();
    }

    /**
     * Récupérer les details du serveur
     * @public
     *
     * @return {Promise} Pour gérer les callbacks
     */
    getServerDetails() {
        return this._sendRequest(OauthOsm.apiRequest.GetServerCapabilities);
    }
    /**
     * Récupérer les information de l'utilisateur connecté
     * @public
     *
     * @return {Promise} Pour gérer les callbacks
     */
    getUserDetails() {
        return this._sendRequest(OauthOsm.apiRequest.GetUserDetails);
    }
    /**
     * Ouvrir la fenetre de connection
     * @public
     *
     * @return {JQueryPromise} Pour gérer les callbacks
     */
    connect() {
        //console.trace('myGeocropping', 'osm connect ', this.isConnected);

        let deferred = $.Deferred();
        if (this.isConnected) {

            $(this).triggerHandler("connected");
            deferred.resolveWith(this);
        } else {
            window.open(`${this._baseUrl}index.php${this._devApi ? '?devMode=true' : '?devMode=false'}`, '_self');
        }
        return deferred.promise();
    }
    /**
     * Déconnecter l'utilisateur actuel
     * @public
     */
    disconnect() {
        let deferred = $.Deferred();
        // detruire la session
        $.ajax({
            method: 'GET',
            url: './auth/',
            data: {
                action: 'logout'
            },
            success: (data) => {
                deferred.resolveWith(this);
                this._connected = false;
                $(this).trigger("disconnected");
            },
            error: (err) => {
                deferred.rejectWith(this);
            }
        });
        return deferred.promise();
    }

}

/**
 * Get all cookies or 
 * 
 * @param {(string | string[])} [name=[]] 
 * @returns {{ key: string, value: string }[]} 
 */
function getCookies(name: string | string[] = []): { key: string, value: string }[] {
    "use strict";
    if (typeof name === "string") {
        name = [name];
    } else if (!(name instanceof Array)) {
        throw new TypeError("Argument `name` is not a string or array");
    }
    let cookiesString = document.cookie;
    if (cookiesString.length === 0) {
        return [];
    }
    let cookiesList = cookiesString.split("; ");
    let cookies: { key: string, value: string }[] = [];
    for (let i = 0; i < cookiesList.length; i++) {
        let c = cookiesList[i].split("=");
        if (name.length === 0 || name.indexOf(c[0]) > -1) {
            cookies.push({
                key: c[0],
                value: c[1]
            });
        }
    }
    return cookies;
}

/**
 * Delete a cookie
 * 
 * @param {string} name 
 * @param {string} [path=""] 
 */
function deleteCookie(name: string, path = "") {
    "use strict";
    if (typeof (name) !== "string" && typeof (name) !== "undefined") {
        throw "Invalide agrument";
    }
    let cookies = getCookies(name);

    let date = new Date(0);
    for (let i = 0; i < cookies.length; i++) {
        document.cookie = (cookies[i].key === "") ?
            `${cookies[i].key}; expires=${date.toUTCString()};` + ((path.length !== 0) ? ` path=${path};` : '') :
            `${cookies[i].key}=; expires=${date.toUTCString()};` + ((path.length !== 0) ? ` path=${path};` : '');
    }
}

/**
 * Save data on browser storage
 * 
 * @param {string} name 
 * @param {string} value 
 * @param {{ useLocalStorageOrCookie?: boolean, useLocalStorage?: boolean, useCookie?: boolean, systemUsed?: any, cookie?: { expires?: string, path?: string } }} [options={}] 
 */
function localSave(name: string, value: string, options: { useLocalStorageOrCookie?: boolean, useLocalStorage?: boolean, useCookie?: boolean, systemUsed?: any, cookie?: { expires?: string, path?: string } } = {}) {
    "use strict";

    // Test of value type
    if (typeof name !== "string") {
        throw new TypeError("Argument `name` is not a string");
    }
    if (typeof value !== "string") {
        throw new TypeError("Argument `value` is not a string");
    }

    let date = new Date();
    date.setMonth(date.getMonth() + 1);
    let newOptions = $.extend({}, {
        useLocalStorageOrCookie: true,
        useLocalStorage: false,
        useCookie: false,
        cookie: {
            expires: date.toUTCString(),
            path: "/"
        }
    }, options);

    options.systemUsed = "";

    if ((newOptions.useLocalStorageOrCookie === true || newOptions.useLocalStorage === true) && typeof Storage !== "undefined") {
        localStorage.setItem(name, value);
        options.systemUsed = "localStorage";
    }
    if ((newOptions.useLocalStorageOrCookie === true && typeof Storage === "undefined") || newOptions.useCookie === true) {
        document.cookie = `${name}=${value}; expires=${newOptions.cookie.expires}; path=${newOptions.cookie.path}`;
        options.systemUsed = "cookie";
    }
}

/**
 * Load data from browser storage
 * 
 * @param {string} name 
 * @param {{ useLocalStorageOrCookie?: boolean, useLocalStorage?: boolean, useCookie?: boolean, cookie?: {}, systemUsed?:any }} [options={}] 
 * @returns {string} 
 */
function localLoad(name: string, options: { useLocalStorageOrCookie?: boolean, useLocalStorage?: boolean, useCookie?: boolean, cookie?: {}, systemUsed?: any } = {}): string {
    "use strict";

    // Test of value type
    if (typeof name !== "string") {
        throw new TypeError("Argument `name` is not a string");
    }

    let newOptions = $.extend({}, {
        useLocalStorageOrCookie: true,
        useLocalStorage: false,
        useCookie: false,
        cookie: {}
    }, options);
    options.systemUsed = "";

    let value: string;
    if ((newOptions.useLocalStorageOrCookie === true || newOptions.useLocalStorage === true) && typeof Storage !== "undefined") {
        value = localStorage.getItem(name);
        value = (value !== null) ? value : undefined;
        (value && (options.systemUsed = "localStorage"));
    }
    if ((newOptions.useLocalStorageOrCookie === true && value === undefined) || newOptions.useCookie === true) {

        let cookies = getCookies(name);
        (cookies.length && (value = cookies[0].value));
        (value && (options.systemUsed = "cookie"));
    }
    return value;
}

/**
 * Delete data on browser storage
 * 
 * @param {string} name 
 * @param {{ useLocalStorage?: boolean, useCookie?: boolean, cookie?: { path?: string } }} [options={}] 
 */
function localDelete(name: string, options: { useLocalStorage?: boolean, useCookie?: boolean, cookie?: { path?: string } } = {}) {
    "use strict";

    // Test of value type
    if (typeof name !== "string") {
        throw new TypeError("Argument `name` is not a string");
    }
    options = $.extend({}, {
        useLocalStorage: true,
        useCookie: true,
        cookie: {
            path: "/"
        }
    }, options);
    if (options.useLocalStorage === true && typeof Storage !== "undefined") {
        localStorage.removeItem(name);
    }
    if (options.useCookie === true) {
        deleteCookie(name, options.cookie.path);
    }
}
