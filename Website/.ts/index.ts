/// <reference path="typings/tsd.d.ts" />
/// <reference path="myTypings/tsd.d.ts" />

/**
 * Ouvre le menu de droite si le il est fermé et inversement
 * 
 */
function toggleRightMenu() {
    let actualRight = $('body')[0].offsetWidth - $('.side-nav.fit-right')[0].offsetLeft;
    if (actualRight !== 0)
        closeRightMenu();
    else
        openRightMenu();
}

/**
 * Ouvre le menu de gauche si le il est fermé et inversement
 * 
 */
function toggleLeftMenu() {
    if ($('.side-nav.fit-left').position().left + $('.side-nav.fit-left').width() !== 0)
        closeLeftMenu();
    else
        openLeftMenu();
}

/**
 * Fermer le menu de gauche
 * 
 */
function closeLeftMenu() {
    openLeftMenu(0);
}
/**
 * Ouvre le menu de gauche
 * 
 * @param {number} [width] Largeur du menu
 */
function openLeftMenu(width?: number) {
    (width === undefined) && (width = $('.side-nav.fit-left').width());
    if (width === 0)
        $('.side-nav.fit-left').css('left', -$('.side-nav.fit-left').width());
    else {
        $('.side-nav.fit-left').width(width);
        $('.side-nav.fit-left').css('left', 0);
    }
    $('#mapArea').css('left', width);
}
/**
 * Fermer le menu de droite
 * 
 */
function closeRightMenu() {
    openRightMenu(0);
}
/**
 * Ouvre le menu de gauche
 * 
 * @param {number} [width] Largeur du menu
 */
function openRightMenu(width?: number) {
    (width === undefined) && (width = $('.side-nav.fit-right').width());
    if (width === 0)
        $('.side-nav.fit-right').css('right', -$('.side-nav.fit-right').width());
    else {
        $('.side-nav.fit-right').width(width);
        $('.side-nav.fit-right').css('right', 0);
    }
    $('#mapArea').css('right', width);
}

    let geocropping = new Geocropping();
$(document).ready(() => {
    let $geocropping = $(geocropping);

    // Ajouter les serveur Overpass dans le menu déroulant
    /*for (var key in geocropping._app_server_interpreter_list) {
        $('#urlInterpreter').append(`<option value="${key}">${key}</option>`);
    }*/

    ////////
    // EVENTS

    // Si le serveur Overpass est changé par Geocropping.ts
    /*$geocropping.on('urlInterpreterUpdated', (e, index) => {
        $('#urlInterpreter').val(index);
    });*/

    $('#userAction #login').click(() => {
        geocropping.connect();
    });
    $('#userAction #logout').click(() => {
        geocropping.userDisconnect();
    });

    // Mode d'affichage des éléments (zoom large)
    $('#menu #modeDisplayFar').on('change','input[name=modeDisplayFar]', function (e) {
        let $this = $(this);
        if($this.is(':checked')){
            geocropping.setDisplayModeFar($this.val());
        }
    });

    // Checkbox pour se déplacer sur la carte au clic
    $('#menu #clickPos').change(function (e) {
        let val = $(this).prop('checked');
        geocropping.setGPSPosition(val);
        val ? geocropping.startLocateUser() : geocropping.stopLocateUser();
    }).prop("checked", true);

    // Si on utilisait le serveur OSM de dev, le restaurer
    let devMode = localLoad('devMode');
    if (devMode) {
        geocropping._osm._devApi = devMode == "true";
    }
    // Gerer le checkbox
    $('#menu #devMode').change(function (e) {
        let val: boolean = $(this).prop('checked');
        geocropping.userDisconnect();
        geocropping._osm._devApi = val;
        localSave('devMode', '' + val);
    }).prop("checked", geocropping._osm._devApi);

    // Changement de serveur Overpass via le select
    $('#menu #urlInterpreter').change(function (e) {
        geocropping._app_server_interpreter_index = this.value;
        geocropping.notify.ServerChanged();
    });

    /**
     * Afficher les information nécessaire a la connection
     * 
     */
    let userConnected = () => {
        $('#userAction #login').hide();
        $('#userAction #logout').show();
        $('.map-menu-item:has(#userImage)').show();
        $('#userInfo').show();
    }

    let updateStats = () => {
        $('#userNbValid').text(`Validations : ${geocropping._userNbValidated}`);
        $('#userNbInvalid').text(`Signalements : ${geocropping._userNbInvalidated}`);
    }
    /**
     * Cacher les information nécessaire a la connection
     * 
     */
    let userDisconnected = () => {

        $('#userAction #logout').hide();
        $('#userAction #login').show();

        $('.map-menu-item:has(#userImage)').hide();
        $('#userInfo').hide();
        $('#userImage').attr('src', '');
        $('#userInfo #userName').text('');
    }

    $geocropping.on('userDisconnected', userDisconnected);
    $geocropping.on('userConnected', userConnected);
    $geocropping.on('updateStats', updateStats);

    /**
     * Mettre a jour les informations de l'utilisateur qui sont affichées
     * 
     * @param {JQueryEventObject} e Event Jquery
     * @param {Geocropping.USER} data Données de l'utilisatuer
     */
    let updateUserDetails = (e: JQueryEventObject, data: Geocropping.USER) => {
        //console.trace('myGeocropping', 'updateUserDetails', e);
        $('#userImage').show();
        $('#userImage').attr('src', data.img);
        $('#userInfo #userName').text(data.display_name);
    }
    $geocropping.on('userDetailsUpdated', updateUserDetails);

    if (geocropping.isConnected) {
        userConnected();
    } else {
        userDisconnected();
    };

    geocropping.createMap('map');

    // pour permettre le redimentionnement après la création de la carte leaflet
    document.getElementById('mapArea').style.width = "auto";

    geocropping.startLocateUser();
});