/// <reference path="typings/tsd.d.ts" />
/// <reference path="myTypings/tsd.d.ts" />

/**
 * 
 * 
 * @class Geocropping
 */
class Geocropping {

    /**
     * Défini si on est sur l'appli mobile
     *
     * @type {Boolean}
     * @memberof Geocropping
     */
     _onMobileApp: boolean = false;

    /*
     * GENERAL
     */

    _userNbValidated: number;
    _userNbInvalidated: number;

    /**
     * Gestion de la connexion à osm
     * 
     * @type {OauthOsm}
     * @memberof Geocropping
     */
    _osm: OauthOsm;

    /**
     * Leaflet map
     * 
     * @type {L.Map}
     * @memberof Geocropping
     */
    _map?: L.Map = null;
    
    /**
     * Icons pour le marker
     * 
     * @memberof Geocropping
     */
    _icon = {
        red: L.icon({
            iconUrl:'./img/marker/red.png',
            iconSize: [32,50],
            iconAnchor: [16,50]
        }),
        green: L.icon({
            iconUrl:'./img/marker/green.png',
            iconSize: [32,50],
            iconAnchor: [16,50]
        }),
        blue: L.icon({
            iconUrl:'./img/marker/blue.png',
            iconSize: [32,50],
            iconAnchor: [16,50]
        })
    }

    /**
     * Osm data user
     * 
     * @type {Geocropping.USER}
     * @memberof Geocropping
     */
    _user: Geocropping.USER = Geocropping.DEFAULT.USER;
    /**
     * Marker indiquant la position de l'utilisateur sur la carte
     * 
     * @type {L.Marker}
     * @memberof Geocropping
     */
    _userMarker: L.Marker;
    /**
     * Groupe de layer des élément affiché
     * 
     * @type {L.GeoJSON}
     * @memberof Geocropping
     */
    _geoJSONLayers: L.GeoJSON;

    /**
     * Groupe de point sur la carte (pour les element loin)
     * 
     * @type {L.HeatLayer}
     * @memberof Geocropping
     */
    _heatLayerFar: L.HeatLayer;
    /**
     * Option d'affichage des element éloignés
     * 
     * @memberof Geocropping
     */
    _app_heatLayer_options = {
        minOpacity: 0.9,
        radius: 10,
        blur:5,
        gradient:{
            0.9: 'white',
            0.95: 'cyan',
            1: 'blue'
        }
    }
    
    /**
     * Dernière position de l'utilisateur enregistré
     * 
     * @type {L.LatLng}
     * @memberof Geocropping
     */
    _lastUserLocation?: L.LatLng = null;

    /**
     * Stoker le nécessaire lié a un élément dans un dictionnaire avec son id (ex: 'node/123456789') comme index
     * 
     * @type {IDictionary<{ layer: L.Layer, note?: any }>}
     * @memberof Geocropping
     */
    _featureDisplayedAround: IDictionary<{ layer: L.Layer}> = {};
    /**
     * Stoker le nécessaire lié a un élément dans un dictionnaire avec son id (ex: 'node/123456789') comme index
     * 
     * @type {IDictionary<{ layer: L.Layer, note?: any }>}
     * @memberof Geocropping
     */
    _featureDisplayedFar: IDictionary<{ latlng: L.LatLng}> = {};
    /**
     *  Callback du clic sur la carte (stocké pour pouvoir arrêter l'event)
     * 
     * @type {L.EventHandlerFn}
     * @memberof Geocropping
     */
    __locationFoundClick: L.EventHandlerFn;


    /*
     *  MAP SETTINGS
     */
    /**
     * Zoom minimum de la carte
     * 
     * @type {number}
     * @memberof Geocropping
     */
    _map_minZoom: number = 10;
    /**
     * Zoom maximum de la carte
     * 
     * @type {number}
     * @memberof Geocropping
     */
    _map_maxZoom: number = 19;


    /*
     * APP SETTINGS
     */

    /**
     * Liste de serveur Overpass http://wiki.openstreetmap.org/wiki/Overpass_API (https only)
     * 
     * @type {IDictionary<string>}
     * @memberof Geocropping
     */
    _app_server_interpreter_list: IDictionary<string> = {
        Principal: "/overpass-interpreter.php",
        //Français: "https://api.openstreetmap.fr/oapi/interpreter", HS
        //'Vi-di fr': "https://overpass.osm.vi-di.fr/api/interpreter" HS
    };
    /**
     * Serveur Overpass utilisé
     * 
     * @type {string}
     * @memberof Geocropping
     */
    _app_server_interpreter_index: string;

    /**
     * Distance of the search (meter)
     * 
     * @type {number}
     * @memberof Geocropping
     */
    _app_distance_search: number = 20;

    /**
     * Distance to hide feature (meter)
     * 
     * @type {number}
     * @memberof Geocropping
     */
    _app_distance_hide: number = 55;

    /**
     * Distance de recherche large
     * 
     * @type {number}
     * @memberof Geocropping
     */
    _app_distance_search_far: number = 1000;

    _app_display_far: boolean = null;

    /**
     * Distance need to search feature between the actual user location and the latest search location (meter)
     * 
     * @type {number}
     * @memberof Geocropping
     */
    _app_distance_travel_to_search: number = 5;

    /**
     * TODO:
     * Number maximum of feature that can be displayed (-1 limitless) 
     * 
     * @type {number}
     * @memberof Geocropping
     */
    _app_limit_feature_displayed: number = -1;

    /**
     * Time to locate user again (ms)
     * 
     * @type {number}
     * @memberof Geocropping
     */
    _app_time_locate_interval: number = 5000;

    /**
     * True if position on clic is actived
     * 
     * @memberof Geocropping
     */
    _app_click_position_actived = false;

    /**
     * Gérer les notification affiché sur la carte
     * 
     * @type {MyNotification}
     * @memberof Geocropping
     */
    notify: MyNotification;


    /*
     * STYLE SETTINGS
     */

    /**
     * Style rouge pour l'affichage large
     * 
     * @memberof Geocropping
     */
    _style_alert_far = {
        value: 1,
        color:"#FF0000",
        display: true
    };    

    /**
     * Style orange pour l'affichage large
     * 
     * @memberof Geocropping
     */
    _style_warning_far = {
        value: 0.99,
        color:"#FF7800",
        display: true
    };
    
    /**
     * Style vert pour l'affichage large
     * 
     * @memberof Geocropping
     */
    _style_ok_far = {
        value: 0.98,
        color:"#00FF00",
        display: true
    };
    
    /**
     * Style noir pour l'affichage large
     * 
     * @memberof Geocropping
     */
    _style_invalid_far = {
        value: 0.97,
        color: "#000000",
        display: true
    };  

    /**
     * Style rouge pour un element sur la carte
     * 
     * @type {L.PathOptions}
     * @memberof Geocropping
     */
    _style_alert: L.PathOptions = {
        fillColor: "#ee0000",
        color: "#ff0000",
        weight: 2,
        opacity: 1,
        fillOpacity: 0.0,
        fill: true
    };
    /**
     * Style orange pour un element sur la carte
     * 
     * @type {L.PathOptions}
     * @memberof Geocropping
     */
    _style_warning: L.PathOptions = {
        fillColor: "#ee6700",
        color: "#ff7800",
        weight: 2,
        opacity: 1,
        fillOpacity: 0.0,
        fill: true
    };
    /**
     * Style vert pour un element sur la carte
     * 
     * @type {L.PathOptions}
     * @memberof Geocropping
     */
    _style_ok: L.PathOptions = {
        fillColor: "#00ee00",
        color: "#00ff00",
        weight: 2,
        opacity: 1,
        fillOpacity: 0.0,
        fill: true
    };
    /**
     * Style bleu pour un element sur la carte
     * 
     * @type {L.PathOptions}
     * @memberof Geocropping
     */
    _style_selected: L.PathOptions = {
        fillColor: "#0000ee",
        color: "#0000ff",
        weight: 2,
        opacity: 0.3,
        fillOpacity: 0.3,
        fill: true
    };
    /**
     * Style noir pour un element sur la carte
     * 
     * @type {L.PathOptions}
     * @memberof Geocropping
     */
    _style_invalid: L.PathOptions = {
        fillColor: "#000000",
        color: "#000000",
        weight: 2,
        opacity: 1,
        fillOpacity: 0.0,
        fill: true
    };

    /**
     * Format d'élément par défaut
     * 
     * @readonly
     * @static
     * @type {Geocropping.DEFAULT}
     * @memberof Geocropping
     */
    static get DEFAULT(): Geocropping.DEFAULT {
        return {
            USER: {
                account_created: new Date(0, 0, 0, 0, 0, 0, 0),
                display_name: "",
                id: -1,
                img: "",
                description: "",
                blocks: {
                    received: {
                        active: -1,
                        count: -1
                    }
                },
                changesets: {
                    count: -1
                },
                contributorTerms: {
                    agreed: false,
                    pd: false
                },
                languages: [],
                message: {
                    received: {
                        count: -1,
                        unread: -1
                    },
                    sent: {
                        count: -1
                    }
                },
                role: null,
                traces: {
                    count: -1
                }
            }
        };
    }

    /**
     * Creates an instance of Geocropping.
     * @memberof Geocropping
     */
    constructor() {
        /**
         * Initialisation
         */
        this._osm = new OauthOsm();
        this._userMarker = L.marker([0, 0], {
            clickable: false,
            keyboard: false,
            title: 'User',
            icon: this._icon.blue
        });
        this._geoJSONLayers = L.geoJSON();
        // ENCOURS
        this._heatLayerFar = L.heatLayer([]);
        
        this.setDisplayModeFar('validated');

        // vérifier si l'utilisateur est connecté et récuppérer ses informations
        this.updateUserDetails();

        // Affectation du premier serveur
        this._app_server_interpreter_index = Object.keys(this._app_server_interpreter_list)[0];

        /**
         * Attach events
         */
        let $osm = $(this._osm);
        $osm.on("requestError", (data) => {
            console.error('requestError', data);

            $(this).trigger('requestError', [data]);
        });

        $osm.on('disconnected', () => {
            $(this).trigger('userDisconnected');
            this._user = Geocropping.DEFAULT.USER;
        });

        $osm.on('connected', () => {
            $(this).trigger('userConnected');
            //console.trace('myGeocropping', 'connected');
            this.updateUserDetails();
            this._getStats();
        });

        /**
         * Affichage des différentes couleurs du marker pour indiquer l'état des requete  
         */

         /** Non utilisé, nombre de requete en cour */
        let queryPending = 0;
        let timer = null;

        /**
         * Une requete fini
         */
        $(document).ajaxSuccess((e)=>{
            queryPending -= 1;
            if(this._userMarker){
                this._userMarker.setIcon(this._icon.green)
            }
        });
        $(document).ajaxError((e)=>{
            queryPending -= 1;
            if(this._userMarker){
                this._userMarker.setIcon(this._icon.red)
            }
        });
        /**
         * Une requete envoyé
         */
        $(document).ajaxSend((e)=>{
            queryPending += 1;
            if(this._userMarker){
                this._userMarker.setIcon(this._icon.blue)
            }
        });
        /**
         * Tout les requetes de la file d'attente sont terminé
         */
        $(document).ajaxComplete((e)=>{
            timer = setTimeout(()=>{
                if(this._userMarker){
                    //this._userMarker.setIcon(this._icon.blue)
                }
            },2000)
        });
        /**
         * Première requete de la file d'attente ajouté
         */
        $(document).ajaxStart(()=>{
            clearTimeout(timer);
        });

    }

    /**
     * Connect l'utilisateur a OSM
     * 
     * @returns {JQueryPromise<{}>} 
     * @memberof Geocropping
     */
    connect(): JQueryPromise<{}> {
        let promise = this._osm.connect();
        // Notifier la connection
        promise.then(() => { this.notify.ConnectionOK() }, () => { this.notify.ConnectionNOK() });
        return promise;
    }

    /**
     * Donnect l'utilisateur a OSM
     * 
     * @returns {JQueryPromise<{}>} 
     * @memberof Geocropping
     */
    userDisconnect(): JQueryPromise<{}> {
        let promise = this._osm.disconnect();
        // Notifier la déconnection
        promise.then(() => { this.notify.DisconnectionOK() }, () => { this.notify.DisconnectionNOK() });
        return promise;
    }

    /**
     * Savoir si l'utilisateur est connecté
     * 
     * @readonly
     * @type {boolean}
     * @memberof Geocropping
     */
    get isConnected(): boolean {
        return this._osm.isConnected;
    }


    /**
     * Mettre a jour les informations de l'utilisateur a partir de OSM
     * 
     * @returns {JQueryPromise<{}>} 
     * @memberof Geocropping
     */
    updateUserDetails(): JQueryPromise<{}> {
        let promise = this._osm.getUserDetails();
        promise.then(
            (data: any) => {
                this._user.account_created = data.user['@attributes'].account_created;
                this._user.display_name = data.user['@attributes'].display_name;
                this._user.id = data.user['@attributes'].id;
                this._user.blocks.received.active = data.user.blocks.received['@attributes'].active;
                this._user.blocks.received.count = data.user.blocks.received['@attributes'].count;
                this._user.changesets.count = data.user.changesets['@attributes'].count;
                this._user.contributorTerms.agreed = data.user['contributor-terms']['@attributes'].agreed;
                this._user.contributorTerms.pd = data.user['contributor-terms']['@attributes'].pd;
                this._user.description = data.user.description || "";
                this._user.img = typeof (data.user.img) === "undefined" ? "" : data.user.img['@attributes'].href;
                this._user.languages = data.user.languages.lang;
                this._user.message.received.count = data.user.messages.received['@attributes'].count;
                this._user.message.received.unread = data.user.messages.received['@attributes'].unread;
                this._user.message.sent.count = data.user.messages.sent['@attributes'].count;
                this._user.role = data.user.role || "";
                this._user.traces.count = data.user.traces['@attributes'].count;
                $(this).trigger('userDetailsUpdated', [this._user]);
            }
        );
        return promise;
    }

    /**
     * Récupérer les information de l'utilisateur si existant
     * 
     * @returns {(Geocropping.USER|null)} 
     * @memberof Geocropping
     */
    getUserDetails(): Geocropping.USER | null {
        return this._user.id !== -1 ? this._user : null;
    }

    /**
     * Savoir si la map est crée
     * 
     * @returns {boolean} 
     * @memberof Geocropping
     */
    isMapCreated(): boolean {
        return this._map instanceof L.Map;
    }

    /**
     * Créer la carte leaflet
     * 
     * @param {(string|HTMLElement)} map Element ou id de l'élément où doit être créer la carte
     * @returns {boolean}
     * @memberof Geocropping
     */
    createMap(map: string | HTMLElement): boolean {
        if (this.isMapCreated() || !(map instanceof HTMLElement) && document.getElementById(map) === undefined) {
            return false;
        }

        // Créer la carte
        this._map = L.map(map, {
            //attributionControl     : false,
            //zoomControl: false,
            //
            //closePopupOnClick      : false,
            //zoomSnap               : 1,
            //zoomDelta              : 1,
            //trackResize            : 1,
            //boxZoom: false,
            //doubleClickZoom: false,
            //dragging: false,
            //
            //center: [44, 0],
            //zoom: 3,
            //minZoom                : undefied,
            //maxZoom                : undefined,
            //layers                 : [],
            //maxBounds              : null,
            //renderer               : *
            //
            //fadeAnimation          : true,
            //markerZoomAnimation    : true,
            //transform3DLimit       : 2^23
            //zoomAnimation          : true,
            //zoomAnimationThreshold : 4
            //
            //inertia                : *,
            //inertiaDeceleration    : 3000,
            //inertiaMaxSpeed        : Infinity,
            //easeLinearity          : 0.2,
            worldCopyJump: true,
            //maxBoundsViscosity     : 0.0,
            //
            //keyboard: false,
            //keyboardPanDelta       : 80,
            //
            //scrollWheelZoom: false,
            //wheelDebounceTime      : 40,
            //weelPxPerZoomLevel     : 60,
            //
            //tap                    : true,
            //tapTolerence           : 15,
            //touchZoom: false
            //bounceAtZoomLimits     : true,
        }).fitWorld();

        // Créer le notification sur la carte
        this.notify = new MyNotification(this._map);

        // Ajouter le fond de la carte
        L.tileLayer('https://osmtiles.xsalto.com/osm_tiles/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            maxZoom: this._map_maxZoom
        }).addTo(this._map);

        // Affecter les events dans la popup qui vient de s'ouvrir
        this._map.on('popupopen', (popupEvent: L.PopupEvent) => {

            /** Données geojson de l'élement */
            let feature = (popupEvent.popup as any)._source.feature;

            /**
             * Mettre a jour la popup en fonction de son contenu
             * 
             */
            let updatePopup = () => {
                (popupEvent.popup as any)._updateLayout();
                (popupEvent.popup as any)._updatePosition();
            }

            /** Propriété OSM de l'élément */
            let properties: any = feature.properties;
            let tags = properties.tags;

            /**
             * Créer un tableau avec seulement les données traduisible (fr.js)
             * 
             * @param {Object} object Utilisation de clé + valeur
             * @returns {string} html du tableau
             */
            let toTable = (object: Object): string => {
                let out = "<div class='table-responsive'><table class='table'>";
                $.each(object, (index, value) => {
                    let [i, v] = [index, value];
                    let tag = `<!--tr><td>${i}</td><td>${v}</td></tr-->`;

                    /// trouver les traduction de ID
                    if (presets) {
                        if (presets[`${index}/${value}`] || presets[index]) {
                            if (presets[`${index}/${value}`]) {
                                v = presets[`${index}/${value}`]['name'];
                            } else {
                                v = ((value === "yes") ? "Oui" : (value === "no" ? "Non" : v));
                            }
                            if (presets[index]) {
                                i = presets[index]['name'];
                            }
                            tag = `<tr><td>${i}</td><td>${v}</td></tr>`;
                        }
                    }
                    out += tag;
                });
                return out += '</table></div>';
            }

            let content = `
            <div class='popup-info'>
                <div class='row valide-element'>`;
            if (this.isConnected) {
                    content += `
                    <div class="col-xs-12" role="group">
                        <button type="button" id="btnValidate" data-id="${properties.id}" data-type="${properties.type}" class='btn btn-success col-xs-4'><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
                        <button data-toggle="button"  aria-pressed"false" type="button" id="btnError" class='btn btn-danger col-xs-4'><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <button type="button" id="btnSendToOSM" class='btn btn-warning col-xs-4' onclick="window.location.href='https://www.openstreetmap.org/edit?${properties.type}=${properties.id}'"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>
                        <!--<a target="_blank" id="btnSendToOSM" class='btn btn-warning col-xs-4' role='button' href="https://www.openstreetmap.org/edit?${properties.type}=${properties.id}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>-->
                    </div>
                </div>
                <div class='element-info row'>
                    <!--div><span class='label label-info'>type</span> ${properties.type}</div-->
                    <!--div><span class='label label-info'>id</span> ${properties.id}</div-->
                    <div class="col-xs-12">
                        <!--span class='label label-info'>tags</span-->${toTable(properties.tags)}
                    </div><br/><hr>
                    <!--div><span class='label label-info'>relations</span> ${properties.relations.join(', ')}</div-->
                    <!--div><span class='label label-info'>meta</span> ${toTable(properties.meta)}</div-->
                    <!--pre>${JSON.stringify(properties, null, 4)}</pre-->`
                    if (tags.fixme && tags.fixme !== "" && tags.note !== undefined) {
                        content += `
                    <a style='color:red;' href="https://www.openstreetmap.org/edit?${properties.type}=${properties.id}" target="_blank"><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> L'objet a déjà été déclaré comme erronné, raison : "${tags.note}"</a>`
                    } else if (tags.fixme && tags.fixme !== "") {
                        content += `
                    <a style='color:red;' href= "https://www.openstreetmap.org/edit?${properties.type}=${properties.id}" target= "_blank" > <span class="glyphicon glyphicon-warning-sign" aria- hidden="true" > </span> L'objet a déjà été déclaré comme erronné, raison : "${tags.fixme}"</a>`
                    }
                content += `
                <div class='row valide-element'>
                    <div class='collapse element-signal col-xs-12 toto' id='${properties.id}Signal'>
                        <form onsubmit="return false;" role="form" data-id="${properties.id}" data-type="${properties.type}">
                            <h3 class="signalTitle">Signaler une erreur</h3>
                            <div class="form-group">
                                <select name="errorType" id="errorType" class="form-control" required="required">
                                    <option value="" hidden disabled selected>Choisir l'erreur...</option>
                                    <option value="errorPos">Position incorrecte</option>
                                    <option value="errorExist">N'existe pas</option>
                                    <option value="errorInfo">Informations érronées</option>
                                    <option value="errorWorks">En cours de transformation</option>
                                    <option value="other">Autre</option>
                                </select>
                                <textarea name="comment" id="comment" maxlength="255" class="form-control" rows="1" placeholder="Commentaire" style="resize:none; overflow:hidden; display:none;"></textarea>
                            </div>
                            <div class='valide-element checkbox'>
                                <label><input type="checkbox" id="followChanges"> Je souhaite être notifié de la prochaine mise à jour de cet élément.</label>
                            </div>
                            <div align="center">
                                <input value="Signaler" type="submit" class="btn btn-primary">
                                <input value="Annuler" type="button" id="btnAnnulation" class="btn btn-primary">
                            </div>
                        </form>
                    </div>` 
            } else {
                content += `
                    <div class="col-xs-12" role="group">
                        <button class="btn btn-info" id="login">Connexion</button>
                    </div>`
            }
            content += `
                </div>
            </div>`;

            popupEvent.popup.setContent(content);

            $(popupEvent.popup.getElement()).on("click", (e) => {
                if ($('.valide-element').has(e.target).length === 0) {
                    // Si on a pas cliqué sur sur la zone de validation, on ferme la popup
                    this._map.closePopup();
                }
            });

            // pour si l'utilisateur n'est pas connecté
            $('button#login', popupEvent.popup.getElement()).click(e => { this.connect() });

            ////////
            // Partie signalisation d'erreur
            $('button#btnError', popupEvent.popup.getElement()).on('click', e => {
                let $valid = $(`#btnValidate`, popupEvent.popup.getElement());
                let $error = $(`#btnError`, popupEvent.popup.getElement());
                let $sendTo = $(`#btnSendToOSM`, popupEvent.popup.getElement());
                $(`#${feature.properties.id}Signal`, popupEvent.popup.getElement()).toggle(0, updatePopup);
                if (typeof(tags.fixme) == "undefined" || tags.fixme === ""){
                    $valid.prop('disabled', true);
                }
                $error.prop('disabled', true);
                $sendTo.prop('disabled', true);
            });

            // Dans le formulaire de signalisation d'erreur, permet de le fermet et de revenir au popup de base
            $('#btnAnnulation', popupEvent.popup.getElement()).on('click', e => {
                let $valid = $(`#btnValidate`, popupEvent.popup.getElement());
                let $sendTo = $(`#btnSendToOSM`, popupEvent.popup.getElement());
                let $error = $(`#btnError`, popupEvent.popup.getElement());
                $(`#${feature.properties.id}Signal`, popupEvent.popup.getElement()).toggle(0, updatePopup);
                if (typeof (tags.fixme) == "undefined" || tags.fixme === "") {
                    $valid.prop('disabled', false);
                }
                $sendTo.prop('disabled', false);
                $error.prop('disabled', false);
            });



            /**
             * Permet de mettre a jour l'élément et ferme la popup
             * 
             */
            let elementChanged = () => {
                //mettre a jour le style et les donnée
                let req = OauthOsm.apiRequest.GetElementId;
                req.params.id = properties.id
                req.params.type = properties.type
                this._searchInfoElementsOSMAround().then(data => {
                    this._updateElementsDisplayedAround(data);
                });
                popupEvent.target.closePopup();
            }

            let $errorForm = $(`#${properties.id}Signal form`, popupEvent.popup.getElement()); 

            $errorForm.on('submit', e => {

                // Invalider l'élément

                let id = $errorForm.data('id');
                let type = $errorForm.data('type');
                let fixme = ``;
                let note = ``;
                switch ((e.target as any).elements.errorType.value) {             // Lors d'ajout, suppression d'options, penser à modifier la fonction this.isFixmeFromUs()
                    case 'errorPos':
                        fixme += `location`;
                        note += `Location is incorrect or moved`;
                        break;
                    case 'errorExist':
                        fixme += `absent`;
                        note += `The object dispappeared or never happened to be there`;
                        break;
                    case 'errorInfo':
                        fixme += `tags`;
                        note += `Object tags should be updated`;
                        break;
                    case 'errorWorks':
                        fixme = `transient`;
                        note = `Ongoing transformation`;
                        break;
                    case 'other':
                        fixme = `resurvey`;
                        note += (e.target as any).elements.comment.value
                        break;
                    default:
                        return;
                }

                // 1: Récupérer les info avant modification
                let req = OauthOsm.apiRequest.GetElementId;
                let $currentTarget = $(e.currentTarget);
                req.params.id = id;
                req.params.type = type;
                this._osm._sendRequest(req).then( (beforeToAfter) => {

                    // 2: Créer le groupe de modification
                    let req = OauthOsm.apiRequest.CreateChangeset;
                    req.params.element = JSON.stringify({ //JSON.parse fait l'inverse
                        changeset: {
                            tag: [
                                { '@attributes': { 'k': 'comment', 'v': 'Surveyed or verified in person' } },
                                { '@attributes': { 'k': 'host', 'v': window.location.origin + window.location.pathname } }
                            ]
                        }
                    })
                    this._osm._sendRequest(req).then((changesetId: number) => {

                        // 3: Mettre à jour l'élément avec le nouveau tag
                        let req = OauthOsm.apiRequest.UpdateElement;
                        req.params.before = JSON.stringify($.extend({}, beforeToAfter));

                        let updated = false;

                        /// mettre à jour les tags avec le nouveau tag
                        let element = beforeToAfter[type];
                        // transformer les tags en tableau de tag
                        if (element) {
                            if (!(element.tag instanceof Array)) {
                                if (element.tag instanceof Object) {
                                    element.tag = [element.tag];
                                } else {
                                    element.tag = [];
                                }
                            }

                            let indexS; // index du potentiel tag surver:date à remove
                            let indexF, indexN = -1; // index du potentiel tag fixme déjà existant à potentielement modifier (si il a été mis par geocropping)
                            let fixmeFromUs = false;
                            for (let index = 0; index < element.tag.length; index++) {
                                let tag = element.tag[index];
                                if (tag['@attributes'].k === 'fixme' && !this.isFixmeFromUs(tag['@attributes'].v)) {
                                    // TODODO: On fais quoi ? -> pour l'instant : si ça vient de geocropping, ecraser, et sinon laisser. Comment savoir?
                                    // solution mise en place : tester les valeurs des fixme avec nos normes.
                                    updated = true;
                                    this.notify.SignalisationExterieur();
                                    break;
                                }
                                if (tag['@attributes'].k === 'survey:date'){
                                    indexS = index;
                                }
                                if (tag['@attributes'].k === 'fixme' && this.isFixmeFromUs(tag['@attributes'].v)) {
                                    indexF = index;
                                    fixmeFromUs = true;
                                }
                                if (fixmeFromUs && tag['@attributes'].k === "note") {
                                    indexN = index;
                                }
                            }
                            // Si il y a un tag survey:date
                            if(indexS !== undefined){
                                element.tag.splice(indexS, 1);
                            }
                            // Si il y a déjà un tag fixme et note mis par geocropping
                            if (fixmeFromUs && indexF !== undefined) {
                                if (indexF < indexN) {
                                    element.tag.splice(indexN, 1);
                                    element.tag.splice(indexF, 1);
                                } else {
                                    element.tag.splice(indexF, 1);
                                    if (indexN !== -1) {
                                        element.tag.splice(indexN, 1);
                                    }
                                }
                            }

                            if (!updated) {
                                element.tag.push({ '@attributes': { k: "fixme", v: fixme } });
                                element.tag.push({ '@attributes': { k: "note", v: note } });
                            }

                        } else {
                            // l'élément n'existe pas
                            beforeToAfter = {};
                        }
                        req.params.after = JSON.stringify(beforeToAfter);
                        req.params.changeset = changesetId;
                        if(!updated)
                            this._osm._sendRequest(req).then(e => {

                                // 4: Fermer le groupe de modification
                                let req = OauthOsm.apiRequest.CloseChangeset;
                                req.params.id = changesetId;
                                this._osm._sendRequest(req).then(e => {
                                    //Si followChanges est coché, on ajoute l'élément aux préférences
                                    if($('#followChanges').prop('checked',true)){ 
                                        let req = OauthOsm.apiRequest.GetElementId;
                                        req.params.id = $errorForm.data('id');
                                        this._osm._sendRequest(req).then((data : any) => { this._ajoutPreferenceUnique(data.node["@attributes"].id, data.node["@attributes"].version); }, () => { });
                                    }
                                    elementChanged();
                                    // OKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                                    this.notify.InvalidatedOK();
                                    if (this._userNbInvalidated !== -1)
                                        this._userNbInvalidated++;
                                    else
                                        this._userNbInvalidated = 1;
                                    this._ajoutPreferenceUnique("nbInvalid", `${this._userNbInvalidated}`);
                                    $(this).trigger('updateStats');

                                }, e => {
                                    // ERR CloseChangeset
                                    this.notify.ValidatedNOK();
                                })
                            }, e => {
                                // ERR UpdateElement
                                this.notify.ValidatedNOK();
                            })
                    }, e => {
                        // ERR CreateChangeset
                        this.notify.ValidatedNOK();
                    })
                }, e => {
                    // ERR GetElementId
                    this.notify.ValidatedNOK();
                });
                
            });

            // Afficher/cacher le textarea pour les commentaire
            $errorForm.find('#errorType').on('change', (e) => {
                if ($(e.target).val() === 'other') {
                    $errorForm.find('#comment').show().prop('required', true);
                } else {
                    $errorForm.find('#comment').hide().prop('required', false);
                }
                updatePopup();
            })
            // Mettre a jour la taille du textarea
            $errorForm.find('#comment').on('change cut paste drop keydown', (e) => {
                (e.target as any).style.height = 'auto';
                (e.target as any).style.height = e.target.scrollHeight + 'px';
                updatePopup();
            })


            // Valider un élément
            //let note = this._featureDisplayed[`${properties.type}/${properties.id}`].note
            // if (tags.fixme && tags.fixme !== "") {
            //     $('button#btnValidate', popupEvent.popup.getElement()).prop('disabled',true)
            // } else {
            $('button#btnValidate', popupEvent.popup.getElement()).on('click', e => {

                // 1: Récupérer les info avant modification
                let req = OauthOsm.apiRequest.GetElementId;
                let $currentTarget = $(e.currentTarget);
                let id = req.params.id = $currentTarget.data('id');
                let type = req.params.type = $currentTarget.data('type');
                let errorReq = (e) => { }
                this._osm._sendRequest(req).then((beforeToAfter) => {

                    // 2: Créer le groupe de modification
                    let req = OauthOsm.apiRequest.CreateChangeset;
                    req.params.element = JSON.stringify({
                        changeset: {
                            tag: [
                                { '@attributes': { 'k': 'comment', 'v': 'Surveyed or verified in person' } },
                                { '@attributes': { 'k': 'host', 'v': window.location.origin + window.location.pathname } }
                            ]
                        }
                    })
                    this._osm._sendRequest(req).then((changesetId: number) => {

                        // 3: Mettre ajour l'élément avec le nouveau tag
                        let req = OauthOsm.apiRequest.UpdateElement;
                        req.params.before = JSON.stringify($.extend({}, beforeToAfter));

                        /// mettre a jour les tag avec le nouveau tag
                        let element = beforeToAfter[type];
                        // transformer les tag en tableau de tag
                        if (element) {
                            if (!(element.tag instanceof Array)) {
                                if (element.tag instanceof Object) {
                                    element.tag = [element.tag];
                                } else {
                                    element.tag = [];
                                }
                            }

                            let updated = false;
                            /** index des source : source=survey  &  source:Date=.* */
                            let sourceS, sourceD;
                            let indexF, indexN = -1; // indexF : index du potentiel tag fixme, indexN : index du potentiel tag note
                            let fixmeFromUs = false;
                            for (let index = 0; index < element.tag.length; index++) {
                                let tag = element.tag[index];
                                if (tag['@attributes'].k === 'source' && tag['@attributes'].v === 'survey') {
                                    sourceS = index;
                                }
                                if (tag['@attributes'].k === 'source' && tag['@attributes'].v === 'date') {
                                    sourceD = index;
                                }
                                if (tag['@attributes'].k === 'survey:date') {
                                    tag['@attributes'].v = (new Date).toISOString().replace(/T.*$/, '');
                                    updated = true;
                                }
                                if (tag['@attributes'].k === 'fixme' && this.isFixmeFromUs(tag['@attributes'].v)) {
                                    indexF = index;
                                    fixmeFromUs = true;
                                }
                                if(fixmeFromUs && tag['@attributes'].k === "note"){
                                    indexN = index;
                                }
                            }

                            // Si il y a un tag fixme avec une note mise par geocropping
                            if (indexF !== undefined) {
                                if (indexF < indexN) {
                                    element.tag.splice(indexN, 1);
                                    element.tag.splice(indexF, 1);
                                } else {
                                    element.tag.splice(indexF, 1);
                                    if (indexN !== -1) {
                                        element.tag.splice(indexN, 1);
                                    }
                                }
                            }

                            // Si le tag n'existe pas, le créer
                            if (!updated) {
                                element.tag.push({ '@attributes': { k: "survey:date", v: (new Date).toISOString().replace(/T.*$/, '') } })
                            }

                            // source:Date=.* remplacé par survey:date
                            if (sourceD && sourceS) {
                                element.tag.splice(sourceD, 1);
                            }

                        } else {
                            // l'élément n'existe pas
                            beforeToAfter = {};
                        }
                        req.params.after = JSON.stringify(beforeToAfter);
                        req.params.changeset = changesetId;
                        this._osm._sendRequest(req).then(e => {

                            // 4: Fermer le groupe de modification
                            let req = OauthOsm.apiRequest.CloseChangeset;
                            req.params.id = changesetId;
                            this._osm._sendRequest(req).then(e => {
                                elementChanged();

                                // OKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                                this.notify.ValidatedOK();
                                if (this._userNbValidated !== -1)
                                    this._userNbValidated++;
                                else
                                    this._userNbValidated = 1;
                                this._ajoutPreferenceUnique("nbValid", `${this._userNbValidated}`);
                                $(this).trigger('updateStats');

                            }, e => {
                                // ERR CloseChangeset
                                this.notify.ValidatedNOK();
                            })
                        }, e => {
                            // ERR UpdateElement
                            this.notify.ValidatedNOK();
                        })
                    }, e => {
                        // ERR CreateChangeset
                        this.notify.ValidatedNOK();
                    })
                }, e => {
                    // ERR GetElementId
                    this.notify.ValidatedNOK();
                });
            });
            // }


            // Changer la couleur de l'élément
            (popupEvent.popup as any)._source.setStyle($.extend({}, this._style_selected, { fill: (feature.geometry.type !== "LineString") }));

            // Supprimer le bouton par défaut
            $((popupEvent.popup as any)._closeButton).remove();

            // Mettre a jour la popup si on change les dimenssion de la fenetre
            $(window).on('resize orientationchange', updatePopup);
        })
        this._map.on('popupclose', (popupEvent: L.PopupEvent) => {

            // appel la fonction style dans geojson
            this._geoJSONLayers.resetStyle((popupEvent.popup as any)._source);
            console.log((popupEvent.popup as any)._source);

            // TODO: arrêter seulement sur l'évenement updatePopup et pas sur tout l'évent
            $(window).off('resize orientationchange');
        })

        //L.control.scale().addTo(this._map);

        /**
         * Créer la couche géojson ajouté a la carte
         * 
         * 
         */
        let filter = (featureData: any) => {
            // Pour chaque element dire si il doit être affiché
            let display = true;

            let hasATagTranslatable = (tags) => {
                let tagTranslated = false;
                $.each(tags, (index, value) => {
                    if (tagTranslated) {
                        return;
                    }
                    if (presets) {
                        if (presets[`${index}/${value}`] || presets[index]) {
                            tagTranslated = true;
                        }
                    }
                });
                return tagTranslated;
            }

            // Si il existe déjà, ne pas l'afficher une deuxième fois
            if (this._featureDisplayedAround[featureData.id] !== undefined ||
                featureData.properties.type !== 'node' ||
                !hasATagTranslatable(featureData.properties.tags)) {
                display = false;
            }


            /*for (let tag in featureData.properties.tags) {
                if (presets[`${tag}/${featureData.properties.tags[tag]}`] || presets[tag]) {
                    console.log(`${tag}/${featureData.properties.tags[tag]}`, presets[`${tag}/${featureData.properties.tags[tag]}`], tag, presets[tag]);
                    display = true;
                    break;
                } else {
                    console.info(`${tag}/${featureData.properties.tags[tag]}`, presets[`${tag}/${featureData.properties.tags[tag]}`], tag, presets[tag]);
                }
            }*/
            return display;
        };
        let style = (feature) => {
            // Style des élément qui seront affiché

            let style: L.PathOptions = {};
            if ((feature.properties as any).tags) {
                let tags = (feature.properties as any).tags;
                let dateVerified: Date;
                let dateChange = new Date((feature.properties as any).meta.timestamp);
                if (tags["survey:date"]) {
                    dateVerified = new Date(tags["survey:date"]);
                } else if (tags["source"] === "survey" && tags["source:date"]) {
                    dateVerified = new Date(tags["source:date"]);
                }

                // Date déterminant un élément comme ancien
                let dateOld = new Date();
                let year = dateOld.getUTCFullYear();
                dateOld.setUTCFullYear(year - 2);

                let isFar = this._app_display_far ? '_far' : ''; 

                // A été invalidé 
                if (/**this._featureDisplayedAround[feature.id] &&*/ tags.fixme) {
                    // noir
                    style = $.extend({}, this[`_style_invalid${isFar}`]); // $.extend pour ne pas modifier les valeur de base (ne pas faire une référence)
                }
                // Jamais validé et ancien
                else if (!dateVerified && dateChange < dateOld) {
                    //red
                    style = $.extend({}, this[`_style_alert${isFar}`]); // $.extend pour ne pas modifier les valeur de base (ne pas faire une référence)
                }
                // Jamais validé et récent
                else if (!dateVerified && dateChange > dateOld) {
                    //orange
                    style = $.extend({}, this[`_style_warning${isFar}`]); // $.extend pour ne pas modifier les valeur de base (ne pas faire une référence)
                }
                // Validé et ancien
                else if (dateVerified && dateChange < dateOld) {
                    //orange
                    style = $.extend({}, this[`_style_warning${isFar}`]); // $.extend pour ne pas modifier les valeur de base (ne pas faire une référence)
                }
                // Validé et récent
                else {
                    //gree
                    style = $.extend({}, this[`_style_ok${isFar}`]); // $.extend pour ne pas modifier les valeur de base (ne pas faire une référence)
                }
            }
            /*if (feature.geometry.type === "Point") {
                style.fill = true;

            } else {
                style.weight = 3;
            }*/
            return style;
        }
        this._geoJSONLayers = L.geoJSON(null, {
            // Mettre un cercle pour les point a la place des marker
            pointToLayer: (feature, latlng) => {
                return new L.CircleMarker(latlng);
            },
            filter: filter,
            style: style,
            onEachFeature: (feature, layer) => {
                // Affecter la popup sur les elements
                if (feature.properties) {
                    let popup = L.responsivePopup({
                        hasTip: false/*,
                        minWidth: window.innerWidth / 1.7,
                        maxWidth: window.innerWidth - 40,
                        maxHeight: window.innerHeight - 40*/
                    });
                    layer.bindPopup(popup, {
                        keepInView: false,
                        //closeOnClick: false
                    });
                }
            }

        });
        
        this._geoJSONLayers.addTo(this._map);
    
        // ENCOURS
        this._map.on('zoomend', (e) => {
            if(this._map.getZoom() <=17 && this._app_display_far !== true){
                
                /**
                 * Affichage large
                 */
                this._map.removeLayer(this._geoJSONLayers);
                this._heatLayerFar.addTo(this._map);
                this._app_display_far = true;
                if(this._lastUserLocation){
                    this._searchData(this._lastUserLocation);                    
                }
            }else if(this._map.getZoom() > 17 && this._app_display_far === true){
                this._map.removeLayer(this._heatLayerFar);
                this._geoJSONLayers.addTo(this._map);
                this._app_display_far = false;
                if(this._lastUserLocation){
                    this._searchData(this._lastUserLocation);                    
                }
            }
        });

        this._checkChanges();

    }

    /**
     * Activer/désactiver la geolocalisation, fonction utile pour l'application mobile car s'occupe aussi de décocher automatiquement la checkbox de localisation
     *
     * @param {Boolean} active
     * @memberof Geocropping
     */
    MobileSetGPSPosition(active: Boolean){
        $('#menu #clickPos').prop('checked', (active?true:false));
        this.setGPSPosition(active);
        active ? geocropping.startLocateUser() : geocropping.stopLocateUser();
    }

    /**
     * Activer/Désactiver la position au click
     * 
     * @param {Boolean} active
     * @memberof Geocropping
     */
    setGPSPosition(active: Boolean) {
        if (!active) {
            this._app_click_position_actived = true;
            this.notify.ClicPositionON();
            this._map.on('click', this._locationFoundClickFunction()).setMaxBounds(null);
        } else {
            this._app_click_position_actived = false;
            this.notify.ClicPositionOFF();
            this._map.off('click', this._locationFoundClickFunction());
        }
    }
    /**
     * Créer ou récupérer la fonction de call back du clic (pour pouvoir arrêter l'event)
     * 
     * @returns {L.EventHandlerFn} 
     * @memberof Geocropping
     */
    _locationFoundClickFunction(): L.EventHandlerFn {
        if (!(this.__locationFoundClick instanceof Function)) {
            this.__locationFoundClick = (data) => {
                this._locationFound(data as L.LocationEvent);
            };
        }
        return this.__locationFoundClick;
    }

    /**
     * Démarrer la géolocalisation
     * 
     * @returns {boolean} 
     * @memberof Geocropping
     */
    startLocateUser(): boolean {
        if (!this.isMapCreated()) {
            return false;
        }
        let options = {
            setView: false, // choisir la position nous même
            maxZoom: this._map_maxZoom, // avec setView
            enableHighAccuracy: true,
            forceWatch: true //navigtor.geolocation.watchPosition ne fonctionne pas
        }; -
            this._map.locate(options).on('locationfound', (data: L.LocationEvent) => {
                this._locationFound(data, options);
            }).on('locationerror', (e: L.ErrorEvent) => {
                this._locationError(e, options);
            });
        return true;
    }

    /**
     * Arrêter la géoliocalisation
     * 
     * @memberof Geocropping
     */
    stopLocateUser() {
        if (this.isMapCreated()) {
            this._map.stopLocate();
            this._map.off('locationfound').off('locationerror');
        }

    }

    /**
     * Définir la position du marker de l'utilisateur et faire la recherche des éléments autour
     * 
     * @param {L.LocationEvent} data 
     * @param {any} [options] Options de la géolocalisation (pour la relancer)
     * @memberof Geocropping
     */
    _locationFound(data: L.LocationEvent, options?) {
        //TODO: Vérifier si on ne sort pas des limite géographique (ex: -300 , 40)
        if (this._lastUserLocation === null) {
            this._map.setMinZoom(this._map_minZoom).setMaxZoom(this._map_maxZoom).setView(data.latlng, this._map_maxZoom);
        }
        if (!(data.latlng instanceof L.LatLng)) {
            return;
        }
        // si on a changé de position sur une distance minimum
        if (this._lastUserLocation === undefined || this._lastUserLocation === null || this._lastUserLocation.distanceTo(data.latlng) > this._app_distance_travel_to_search) {
            this._lastUserLocation = data.latlng;
            this._userMarker.setLatLng(data.latlng).addTo(this._map);
            this._searchData(data.latlng);
        }
        if (options && options.forceWatch === true) { // relancer la géolocalisation
            setTimeout(() => {
                this._map.locate(options);
            }, this._app_time_locate_interval);
        } else {
            this.stopLocateUser();
        }
    }

    /**
     * Afficher l'erreur de géolocalisation
     * 
     * @param {L.ErrorEvent} e 
     * @param {any} [options] Options de la géolocalisation (pour la relancer)
     * @memberof Geocropping
     */
    _locationError(e: L.ErrorEvent, options?) {
        console.log(e.message);
        this.notify.LocalisationNOK();

        if (options && options.forceWatch === true) { // relancer la géolocalisation
            setTimeout(() => {
                this._map.locate(options);
            }, this._app_time_locate_interval);
        } else {
            this.stopLocateUser();
        }
    }

    /**
     * Rechercher des éléments autour d'une position
     * 
     * @param {L.LatLng} pos Position de la recherche
     * @memberof Geocropping
     */
    _searchDataAround(pos: L.LatLng) {
        if (!(pos instanceof L.LatLng))
            return;

        let lat = pos.lat, lon = pos.lng;
       
        // Vérifier si la distance pour cacher les élément est plus petite que celle pour les afficher
        let distance = this._app_distance_hide < this._app_distance_search ? this._app_distance_hide : this._app_distance_search;

        // TODO : filtrage ici
        // nature : natural=tree
        // aménagements : amenity=..
        // highway ??
        // Requete overpass (actuellement récuépérer les node ayant au moins un tag)
        let query = `[out:json];node[~"."~"."](around:${distance},${lat},${lon})->.nodes;/*way(around:${distance},${lat},${lon})->.ways;*/.nodes out meta;/*.ways out geom meta;*/`;
        if (!this._app_click_position_actived) {
            this._map.setMaxBounds(L.latLngBounds(this._lastUserLocation, this._lastUserLocation));
        }
        $.ajax({
            context: this,
            url: this._app_server_interpreter_list[this._app_server_interpreter_index],
            data: {
                data: query
            },
            success: (data, textStatus, jqXHR) => {

                // Traiter les éléments si ils existent
                if (data.elements.length > 0) {
                    let geojson = osmtogeojson(data);
                    //Ajouter les données sur la carte
                    this._geoJSONLayers.addData(geojson);

                    // Reset la liste des données affichées
                    this._featureDisplayedAround = {};
                    this._geoJSONLayers.getLayers().forEach((layer: L.Layer, index, array) => {

                        /**
                         * Supprimer les éléments distant
                         */
                        let remove = false;
                        if (layer instanceof L.CircleMarker) {
                            let dist = layer.getLatLng().distanceTo(this._lastUserLocation);
                            if (dist > this._app_distance_hide) {
                                remove = true;
                            }
                        } else if (layer instanceof L.Polyline) {
                            let nodes: any = layer.getLatLngs();
                            if (nodes[0] instanceof Array) {
                                nodes = nodes[0];
                            }
                            remove = true;
                            // Ne pas supprimer si un des points est a portée
                            for (let i = 0; i < nodes.length; i++) {
                                let dist = L.latLng(nodes[i].lat, nodes[i].lng).distanceTo(pos);
                                if (dist < this._app_distance_hide) {
                                    remove = false;
                                    break;
                                }
                            }
                        }
                        if (remove) {
                            this._geoJSONLayers.removeLayer(layer);
                        } else {
                            this._featureDisplayedAround[((layer as any).feature as any).id] = { layer: layer };
                        }
                    });

                    // récupérer les information des Point affiché directement sur osm (ne pas avoir le problème de cache des serveur Overpass)
                    //if(this._app_server_interpreter_index !== 'Mondial'){
                        this._searchInfoElementsOSMAround().then(data => {
                            this._updateElementsDisplayedAround(data);
                        });
                    //}

                    ///////////////////////
                    //fixer la carte sur les élément trouvés ?

                    //this._map.setMaxBounds(this._geoJSONLayers.getBounds());
                    //} else {
                    //this._map.panTo(pos);
                    // --------------------------- CHANGER ICI ------------------------
                    // if (!this._app_click_position_actived) {
                    //     this._map.setMaxBounds(L.latLngBounds(this._lastUserLocation, this._lastUserLocation));
                    // }
                    // --------------------------- mettre ça au début de la fonction

                    // Récupérer les notes des élément
                    //this._updateNoteOfElements();
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Si il y a une erreur sur le server Overpass, passer au suivant
                /*console.log(errorThrown);
                this.notify.AutoServerChanged();

                let keys = Object.keys(this._app_server_interpreter_list);
                if (this._app_server_interpreter_index === keys[keys.length - 1]) {
                    this._app_server_interpreter_index = keys[0];
                } else {
                    this._app_server_interpreter_index = keys[keys.indexOf(this._app_server_interpreter_index) + 1];
                }
                $(this).trigger('urlInterpreterUpdated', [this._app_server_interpreter_index]);*/
            }
        });
    }

    /**
     * Récupérer les données des éléments affichés directement sur OSM
     * 
     * @returns {JQueryPromise<{}>} pid
     * @memberof Geocropping
     */
    _searchInfoElementsOSMAround(): JQueryPromise<{}> {
        let ids = [];
        // Récupérer les ids des nodes affichés
        for (let key in this._featureDisplayedAround) {
            if (this._featureDisplayedAround.hasOwnProperty(key)) {
                let regexp = key.match(/^node\/(\d+)/);
                if (regexp && regexp[1]) {
                    ids.push(regexp[1]);
                }
            }
        }
        let req = OauthOsm.apiRequest.GetElementsId;
        req.params.type = "nodes";
        req.params.ids = ids;
        return this._osm._sendRequest(req);
    }

    /**
     * Mettre a jour les données des éléments affichés et le style
     * 
     * @param {{ any?}} data Données récupérer sur OSM
     * @memberof Geocropping
     */
    _updateElementsDisplayedAround(data: { any?}) {
        let type;
        let obj = [];

        //// Transformer les données en données Overpass pour les changer en geoJSON
        for (let key in data) {
            if (data.hasOwnProperty(key) && key !== "@attributes") {
                type = key;
                obj = data[type];
            }
        }
        if (!(obj instanceof Array) && obj instanceof Object) {
            obj = [obj];
        }
        for (let index = 0; index < obj.length; index++) {
            let element = obj[index];

            // Récupérer l'élément afficher
            let feature = this._featureDisplayedAround[`${type}/${element["@attributes"].id}`]
            if (feature && element["@attributes"]) {
 
                // Faire les modification necessaire

                if (element["@attributes"].visible) {
                    delete element["@attributes"].visible;
                }
                let osm = {
                    type: type
                };
                for (let key in element["@attributes"]) {
                    if (element["@attributes"].hasOwnProperty(key)) {
                        let attribute = element["@attributes"][key];
                        if ((Number as any).isInteger(attribute)) {
                            attribute = Number(attribute);
                        }
                        osm[key] = attribute;
                    }
                }
                let tags = element['tag'];
                if (tags) {
                    osm['tags'] = {};
                    if (!(tags instanceof Array) && tags instanceof Object) {
                        tags = [tags];
                    }
                    for (let i = 0; i < tags.length; i++) {
                        let tag = tags[i];
                        osm['tags'][tag["@attributes"].k] = tag["@attributes"].v;
                    }
                }

                // Affecter les nouvelles données
                (feature.layer as any).feature = osmtogeojson({ elements: [osm] }).features[0];
                this._geoJSONLayers.resetStyle(feature.layer);
                
            }
        }
    }

    // 
    /**
     * Parcours les éléments que l'utilisateur a coché pour le notifier s'il sont modifiés
     * 
     * @memberof Geocropping
     */
    _checkChanges(){

        let req = OauthOsm.apiRequest.GetUserPreferences; //récupération des nodes que l'utilisateur veux suivre
            
        this._osm._sendRequest(req).done((data : any) => { 
            
            let resPref = data.preferences.preference; // le résultat de la requête
            let ids = [];                              // les identifiants qui interressent l'utilisateur
            let pref:IDictionary<string> = {};         // clé:valeur avec clé = identifiants et valeur = version

            // ici si resPref ne contient qu'un élément, c'est un objet, non un tableau d'objets, on résoud le problème :
            if (!(resPref instanceof Array)) {
                if (resPref instanceof Object) {
                    resPref = [resPref];
                } else {
                    resPref = [];
                }
            }

            for (let i in resPref) {

                if(resPref[i]['@attributes'].k !== "nbValid" && resPref[i]['@attributes'].k !== "nbInvalid"){

                    ids.push(resPref[i]['@attributes'].k);
                    pref[resPref[i]['@attributes'].k] = resPref[i]['@attributes'].v;
                    
                }
            }

            let req = OauthOsm.apiRequest.GetElementsId; // récupération des infos des éléments que l'utilisateur a demandé à suivre
            req.params.type = "nodes";                   // paramétrage de la requête
            req.params.ids = ids;
            let resNodes;                                // le résultat de la requête

            this._osm._sendRequest(req).then((data : any ) => { 
                resNodes = data.node;
                let differents = [];
                if (!(resNodes instanceof Array)) {
                    if (resNodes instanceof Object) {
                        resNodes = [resNodes];
                    } else {
                        resNodes = [];
                    }
                }
                for(let i in resNodes){
                    if(pref[ resNodes[i]['@attributes'].id ] < resNodes[i]['@attributes'].version){ // si la version actuelle de l'élément est plus grande que celui enregistré : on garde le node de côté
                        differents.push(resNodes[i]['@attributes'].id);
                    }
                }
                this.notify.ChangementsTrouve(differents);
                this._removeChanges(resPref,differents);
                
            });
        });    

    }

    /**
     * 
     * Enlève des préférences les nodes ayant été modifiés
     * TODODO gérer quand il y plus de 150 preférences (apparement patché)
     * 
     * @param {any} resPref 
     * @param {number[]} changes 
     * @memberof Geocropping
     */
    _removeChanges(resPref, changes: number[]) {
        let newPref = {
            preferences:{
                preference:[]
            }
        };
        for (let i in resPref){
            if(changes.indexOf(resPref[i]['@attributes'].k) === -1 ){
                newPref.preferences.preference.push({ '@attributes': { k: resPref[i]['@attributes'].k, v: resPref[i]['@attributes'].v } });
            }
        }
        let req = OauthOsm.apiRequest.UpdateUserPreferences;
        req.params.content = JSON.stringify(newPref);
        this._osm._sendRequest(req);
    }

    /**
     * Ajout/mise à jour d'une preference
     * 
     * @param {any} id 
     * @param {any} version 
     * @memberof Geocropping
     */
    _ajoutPreferenceUnique(id, version){
        let req = OauthOsm.apiRequest.UpdateSingleUserPreference;
        req.params.key = id;
        req.params.value = version;
        geocropping._osm._sendRequest(req).then(() => {}, () => { this.notify.AjoutSinglePrefNOK() });
    }

    /**
     * Récupérer les statistiques dans les préférences de l'utilisateur. Si elles n'existe pas, les créée
     * 
     * @memberof Geocropping
     */
    _getStats() {
        let req = OauthOsm.apiRequest.GetUserPreferences; //récupération des nodes que l'utilisateur veux suivre

        this._osm._sendRequest(req).done((data: any) => {

            let resPref = data.preferences.preference; // le résultat de la requête
            let valid, invalid;

            // ici si resPref ne contient qu'un élément, c'est un objet, non un tableau d'objets, on résoud le problème :
            if (!(resPref instanceof Array)) {
                if (resPref instanceof Object) {
                    resPref = [resPref];
                } else {
                    resPref = [];
                }
            }

            // On récupère les préférences
            for (let i in resPref) {

                switch(resPref[i]['@attributes'].k) {
                    case "nbValid":
                        valid = resPref[i]['@attributes'].v;
                        break;
                    case "nbInvalid":
                        invalid = resPref[i]['@attributes'].v;
                        break;
                    default: break;
                }

            }

            // On vérifie qu'il y a bien des préférences. si non on les crée.
            // Lorsque l'utilisateur n'a pas encore validé ou signalé, ces préférences sont à -1 car les mettre à "0" entraine un bug (erreur 400 : bad request).
            if(valid === undefined)
                this._ajoutPreferenceUnique("nbValid", "-1");
            if (invalid === undefined)
                this._ajoutPreferenceUnique("nbInvalid", "-1");
            // On traite donc ici le cas le cas où elles sont à -1 (<=> 0)
            if (valid === "-1" || valid === undefined)
                valid = 0;
            if (invalid === "-1" || invalid === undefined)
                invalid = 0;

            // Mise à jour des données affichées
            this._userNbValidated = valid;
            this._userNbInvalidated = invalid;
            $(this).trigger('updateStats');

        });
    }

    /**
     * Vérifier si le fixme provient de géocropping
     * 
     * @param {string} contenuDuFixme 
     * @returns 
     * @memberof Geocropping
     */
    isFixmeFromUs(contenuDuFixme: string){
        // à défault de mieux
        return contenuDuFixme === 'location' || contenuDuFixme === 'absent' || contenuDuFixme === 'tags' || contenuDuFixme === 'transient' || contenuDuFixme === 'resurvey';
    }

    /**
     * 
     * Chercher les elements distant et les ajouter au layer
     * 
     * @param {L.LatLng} pos 
     * @memberof Geocropping
     */
    _searchDataFar(pos: L.LatLng) {

        let lat, lon;
        lat = pos.lat;
        lon = pos.lng;
        
        // Requete overpass (actuellement récuépérer les node ayant au moins un tag)
        let query = `[out:json];node[~"."~"."](around:${this._app_distance_search_far},${lat},${lon});out meta;`;

        $.ajax({
            context: this,
            url: this._app_server_interpreter_list[this._app_server_interpreter_index],
            data: {
                data: query
            },
            success: (data, textStatus, jqXHR) => {

                // Traiter les éléments si ils existent
                //Ajouter les données sur la carte si la carte est affecté (autrement provoque une erreur)
                if((this._heatLayerFar as any)._map){
                    let points = this._getPositionColorFar(data);
                    this._heatLayerFar.setLatLngs(points);
                    // TODO: mettre a jour les données ?
                    /*if(this._app_server_interpreter_index !== 'Mondial'){
                        this._searchInfoElementsOSMFar().forEach(
                            (promise)=>promise.done(
                                (data)=>this._updateElementsDisplayedFar(data)
                            )
                        );
                    }*/
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
        
    }

    /**
     * Formater et enregistrer les éléments dans this._featureDisplayedFar
     * 
     * @param {*} data Données de overpass
     * @returns 
     * @memberof Geocropping
     */
    _getPositionColorFar(data: any){
        let points:L.LatLng[] = [];
        if (data.elements.length > 0) {
            this._featureDisplayedFar = {};
            
            let hasATagTranslatable = (tags) => {
                let tagTranslated = false;
                $.each(tags, (index, value) => {
                    if (tagTranslated) {
                        return;
                    }
                    if (presets) {
                        if (presets[`${index}/${value}`] || presets[index]) {
                            tagTranslated = true;
                        }
                    }
                });
                return tagTranslated;
            }

            /**
             * intencité de l'élément qui sera affiché (appelé a chaque mise a jour)
             * 
             * @param feature 
             */
            let intensity = (feature) => {
    
                let intensity: number = 0;
                if (feature.tags) {
                    let tags = feature.tags;
                    let dateVerified: Date;
                    let dateChange = new Date(feature.timestamp);
                    if (tags["survey:date"]) {
                        dateVerified = new Date(tags["survey:date"]);
                    } else if (tags["source"] === "survey" && tags["source:date"]) {
                        dateVerified = new Date(tags["source:date"]);
                    }
    
                    // Date déterminant un élément comme ancien
                    let dateOld = new Date();
                    let year = dateOld.getUTCFullYear();
                    dateOld.setUTCFullYear(year - 2);
    
                    // A été invalidé 
                    if (tags.fixme) {
                        // noir
                        intensity = this._style_invalid_far.display ? this._style_invalid_far.value : 0;
                    }
                    // Jamais validé et ancien
                    else if (!dateVerified && dateChange < dateOld) {
                        //red
                        intensity = this._style_alert_far.display ? this._style_alert_far.value : 0;
                    }
                    // Jamais validé et récent
                    else if (!dateVerified && dateChange > dateOld) {
                        //orange
                        intensity = this._style_warning_far.display ? this._style_warning_far.value : 0;
                    }
                    // Validé et ancien
                    else if (dateVerified && dateChange < dateOld) {
                        //orange
                        intensity = this._style_warning_far.display ? this._style_warning_far.value : 0; 
                    }
                    // Validé et récent
                    else {
                        //gree
                        intensity = this._style_ok_far.display ? this._style_ok_far.value : 0; 
                    }                                                
                }
                return intensity;
            }

            for (let index = 0; index < data.elements.length; index++) {
                let element = data.elements[index];
    
                // Afficher seulement si c'est un node et si un tag et traduisible
                if (element.type === 'node' && hasATagTranslatable(element.tags)) {

                    let alt =  intensity(element);
                    let latLng = L.latLng([element.lat,element.lon, alt]);
                    
                    if(alt >= 0 && alt <= 1){
                        // Enregistrer les données et la fonction de calcul de l'intencité
                        (latLng as any).geocropping = {data:element, intensity:intensity};
                        this._featureDisplayedFar[`${element.type}/${element.id}`] = {latlng:latLng};
                    }
                    if(alt > 0 && alt <= 1){
                        points.push(latLng);                        
                    }
                }
                
            }
        }
        return points;
    }

    // ENCOURS
    /*_updateElementsDisplayedFar(data: { any?}){

        let type;
        let obj = [];

        //// Transformer les données en données Overpass pour les changer en geoJSON
        for (let key in data) {
            if (data.hasOwnProperty(key) && key === "elements") {
                type = key;
                obj = data[type];
            }
        }
        if (!(obj instanceof Array) && obj instanceof Object) {
            obj = [obj];
        }
        for (let index = 0; index < obj.length; index++) {
            let element = obj[index];

            // Récupérer l'élément afficher
            let feature = this._featureDisplayed[`${type}/${element.id}`]
            if (feature && element) {

                // Faire les modification necessaire
                if (element.visible) {
                    delete element.visible;
                }
                let osm = {
                    type: type
                };
                for (let key in element) {
                    if (element.hasOwnProperty(key)) {
                        let attribute = element.key;
                        if ((Number as any).isInteger(attribute)) {
                            attribute = Number(attribute);
                        }
                        osm[key] = attribute;
                    }
                }
                let tags = element['tag'];
                if (tags) {
                    osm['tags'] = {};
                    if (!(tags instanceof Array) && tags instanceof Object) {
                        tags = [tags];
                    }
                    for (let i = 0; i < tags.length; i++) {
                        let tag = tags[i];
                        osm['tags'][tag["@attributes"].k] = tag["@attributes"].v;
                    }
                }

                // Affecter les nouvelles données
                (feature.layer as any).feature = osmtogeojson({ elements: [osm] }).features[0];
                this._geoJSONLayers.resetStyle(feature.layer);
            }
        }

    }*/

    /**
     * Rechercher les données et les affichers
     * 
     * @param pos Position de recherche
     * @memberof Geocropping
     */
    _searchData(pos: L.LatLng){
        if(this._app_display_far){
            this._searchDataFar(pos);
        }else{
            this._searchDataAround(pos);
        }
    }

    /**
     * 
     * Type d'éléments a afficher sur la vue global
     * 
     * @param {string} type 
     * @returns 
     * @memberof Geocropping
     */
    setDisplayModeFar(type:string){
        this._resetStyleFar();
        let options = $.extend({},this._app_heatLayer_options);
        if(type	=== 'validated'){
            this._style_ok_far.display = true;
            options.gradient = this._getGradientFar();
        }else if(type === 'invalidated'){
            this._style_invalid_far.display = true;
            options.gradient = this._getGradientFar();
        }else if(type === 'warned'){
            this._style_warning_far.display = true;
            options.gradient = this._getGradientFar();
        }else if(type === 'alert'){
            this._style_alert_far.display = true;
            options.gradient = this._getGradientFar();
        }else{
            return false;
        }
        if(this._heatLayerFar){
            this._heatLayerFar.setOptions(options);
            this._updateElementsDisplayedFar();
        }
        return true;
    }

    /**
     * 
     * Récupérer le gradiant de l'affichage large (_heatLayerFar)
     * 
     * @returns 
     * @memberof Geocropping
     */
    _getGradientFar(){
        let styles = [
            this._style_alert_far,
            this._style_invalid_far,
            this._style_ok_far,
            this._style_warning_far
        ];
        let gradient = {};
        for (let index = 0; index < styles.length; index++) {
            let style = styles[index];
            if(style.display){
                gradient[style.value] = style.color;
            }
        }
        return gradient;
    }

    /**
     * 
     * 
     * @memberof Geocropping
     */
    _resetStyleFar(){
        this._style_alert_far.display = false;
        this._style_invalid_far.display = false;
        this._style_ok_far.display = false;
        this._style_warning_far.display = false;
    }

    /**
     * Récupérer les données des éléments affichés directement sur OSM
     * 
     * @returns {JQueryPromise<{}>[]} pid
     * @memberof Geocropping
     */
    _searchInfoElementsOSMFar(): JQueryPromise<{}>[] {
        let promise = [];
        let ids = [];
        // Récupérer les ids des nodes affichés
        for (let key in this._featureDisplayedFar) {
            if (this._featureDisplayedFar.hasOwnProperty(key)) {
                let regexp = key.match(/^node\/(\d+)/);
                if (regexp && regexp[1]) {
                    ids.push(regexp[1]);
                }
            }
        }
        let arrays = [], size = 10;
        
        while (ids.length > 0){
            arrays.push(ids.splice(0, size));
            let req = OauthOsm.apiRequest.GetElementsId;
            req.params.type = "nodes";
            req.params.ids = ids;
            promise.push(this._osm._sendRequest(req));
        }
        return promise;   
    }

    /**
     * Mettre a jour les données des éléments affichés
     * 
     * @param {any} [data] Données OSM
     * @memberof Geocropping
     */
    _updateElementsDisplayedFar(data?) {

        /**
         * Si il y a de nouvelles données, les mettre a jours
         */
        if(data instanceof Object){
            let type;
            let obj = [];
    
            //// Transformer les données en données Overpass pour les changer en geoJSON
            for (let key in data) {
                if (data.hasOwnProperty(key) && key !== "@attributes") {
                    type = key;
                    obj = data[type];
                }
            }
            if (!(obj instanceof Array) && obj instanceof Object) {
                obj = [obj];
            }
            for (let index = 0; index < obj.length; index++) {
                let element = obj[index];
    
                // Récupérer l'élément afficher
                let latLng = this._featureDisplayedFar[`${type}/${element["@attributes"].id}`].latlng
                if (latLng && element["@attributes"]) {
     
                    // Faire les modification necessaire
    
                    if (element["@attributes"].visible) {
                        delete element["@attributes"].visible;
                    }
                    let osm = {
                        type: type
                    };
                    for (let key in element["@attributes"]) {
                        if (element["@attributes"].hasOwnProperty(key)) {
                            let attribute = element["@attributes"][key];
                            if ((Number as any).isInteger(attribute)) {
                                attribute = Number(attribute);
                            }
                            osm[key] = attribute;
                        }
                    }
                    let tags = element['tag'];
                    if (tags) {
                        osm['tags'] = {};
                        if (!(tags instanceof Array) && tags instanceof Object) {
                            tags = [tags];
                        }
                        for (let i = 0; i < tags.length; i++) {
                            let tag = tags[i];
                            osm['tags'][tag["@attributes"].k] = tag["@attributes"].v;
                        }
                    }
    
                    // Affecter les nouvelles données
                    (latLng as any).geocropping.data = osmtogeojson({ elements: [osm] }).features[0].properties;
                }
            }
        }

        /**
         * Mettre a jour les styles des élément affiché basé sur l'intensité (altitude)
         */
        let toDisplay = []
        for (var key in this._featureDisplayedFar) {
            if (this._featureDisplayedFar.hasOwnProperty(key)) {
                var latlng = this._featureDisplayedFar[key].latlng;
                let alt = (latlng as any).geocropping.intensity((latlng as any).geocropping.data);
                latlng.alt = alt;
                if(alt > 0 && alt <= 1){
                    toDisplay.push(latlng);
                }
            }
        }
        /*for (let index = 0; index < this._heatLayerFar._latlngs.length; index++) {
            let latlng = this._heatLayerFar._latlngs[index];
            let alt = (latlng as any).geocropping.intensity((latlng as any).geocropping.data);
            latlng.alt = alt;
            if(alt > 0 && alt <= 1){
                toDisplay.push(latlng);
            }
        }*/
        this._heatLayerFar.setLatLngs(toDisplay);
    }

    /**
     * Est appelé quand on est sur l'application mobile
     * 
     * @memberof Geocropping
     */
     _setOnMobileApp(){
         this._onMobileApp = true;
     }

}
