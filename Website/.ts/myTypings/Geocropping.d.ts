declare namespace Geocropping {
    interface USER {
        account_created: Date,
        display_name: string,
        id: number,
        img: string,
        description: string,
        blocks: {
            received: {
                active: number,
                count: number
            }
        },
        changesets: {
            count: number
        },
        contributorTerms: {
            agreed: false,
            pd: false
        },
        languages: Array<string>,
        message: {
            received: {
                count: number,
                unread: number
            },
            sent: {
                count: number
            }
        },
        role: number,
        traces: {
            count: number
        }
    }
    interface DEFAULT {
        USER: Geocropping.USER
    }
}

declare var fields: any;
declare var presets: any;

declare namespace L {
    export class ResponsivePopup extends Popup {

    }
    export function responsivePopup(options?: ResponsivePopupOptions, source?: Layer): ResponsivePopup

    export class ResponsiveCustomPopup extends ResponsivePopup {

    }
    export interface ResponsivePopupOptions extends PopupOptions {
        hasTip?: Boolean;
    }
}