declare namespace OauthOsm {
    interface ApiRequest {
        method: string,
        url: string,
        request: string,
        params: IDictionary<any>,
        need_connection?: boolean
    }
}