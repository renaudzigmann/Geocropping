/// <reference path="OauthOsm.d.ts" />
/// <reference path="Geocropping.d.ts" />
/// <reference path="leaflet.heat.d.ts" />

declare function osmtogeojson(any): any
interface IDictionary<TValue> {
    [key: string]: TValue
}