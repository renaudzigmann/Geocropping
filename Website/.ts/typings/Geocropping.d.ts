/// <reference path="tsd.d.ts" />
/// <reference path="../myTypings/tsd.d.ts" />
/// <reference types="leaflet" />
/// <reference types="jquery" />
declare class Geocropping {
    _onMobileApp: boolean;
    _userNbValidated: number;
    _userNbInvalidated: number;
    _osm: OauthOsm;
    _map?: L.Map;
    _icon: {
        red: L.Icon;
        green: L.Icon;
        blue: L.Icon;
    };
    _user: Geocropping.USER;
    _userMarker: L.Marker;
    _geoJSONLayers: L.GeoJSON;
    _heatLayerFar: L.HeatLayer;
    _app_heatLayer_options: {
        minOpacity: number;
        radius: number;
        blur: number;
        gradient: {
            0.9: string;
            0.95: string;
            1: string;
        };
    };
    _lastUserLocation?: L.LatLng;
    _featureDisplayedAround: IDictionary<{
        layer: L.Layer;
    }>;
    _featureDisplayedFar: IDictionary<{
        latlng: L.LatLng;
    }>;
    __locationFoundClick: L.EventHandlerFn;
    _map_minZoom: number;
    _map_maxZoom: number;
    _app_server_interpreter_list: IDictionary<string>;
    _app_server_interpreter_index: string;
    _app_distance_search: number;
    _app_distance_hide: number;
    _app_distance_search_far: number;
    _app_display_far: boolean;
    _app_distance_travel_to_search: number;
    _app_limit_feature_displayed: number;
    _app_time_locate_interval: number;
    _app_click_position_actived: boolean;
    notify: MyNotification;
    _style_alert_far: {
        value: number;
        color: string;
        display: boolean;
    };
    _style_warning_far: {
        value: number;
        color: string;
        display: boolean;
    };
    _style_ok_far: {
        value: number;
        color: string;
        display: boolean;
    };
    _style_invalid_far: {
        value: number;
        color: string;
        display: boolean;
    };
    _style_alert: L.PathOptions;
    _style_warning: L.PathOptions;
    _style_ok: L.PathOptions;
    _style_selected: L.PathOptions;
    _style_invalid: L.PathOptions;
    static readonly DEFAULT: Geocropping.DEFAULT;
    constructor();
    connect(): JQueryPromise<{}>;
    userDisconnect(): JQueryPromise<{}>;
    readonly isConnected: boolean;
    updateUserDetails(): JQueryPromise<{}>;
    getUserDetails(): Geocropping.USER | null;
    isMapCreated(): boolean;
    createMap(map: string | HTMLElement): boolean;
    MobileSetGPSPosition(active: Boolean): void;
    setGPSPosition(active: Boolean): void;
    _locationFoundClickFunction(): L.EventHandlerFn;
    startLocateUser(): boolean;
    stopLocateUser(): void;
    _locationFound(data: L.LocationEvent, options?: any): void;
    _locationError(e: L.ErrorEvent, options?: any): void;
    _searchDataAround(pos: L.LatLng): void;
    _searchInfoElementsOSMAround(): JQueryPromise<{}>;
    _updateElementsDisplayedAround(data: {
        any?;
    }): void;
    _checkChanges(): void;
    _removeChanges(resPref: any, changes: number[]): void;
    _ajoutPreferenceUnique(id: any, version: any): void;
    _getStats(): void;
    isFixmeFromUs(contenuDuFixme: string): boolean;
    _searchDataFar(pos: L.LatLng): void;
    _getPositionColorFar(data: any): L.LatLng[];
    _searchData(pos: L.LatLng): void;
    setDisplayModeFar(type: string): boolean;
    _getGradientFar(): {};
    _resetStyleFar(): void;
    _searchInfoElementsOSMFar(): JQueryPromise<{}>[];
    _updateElementsDisplayedFar(data?: any): void;
    _setOnMobileApp(): void;
}
