/// <reference path="tsd.d.ts" />
/// <reference path="../myTypings/tsd.d.ts" />
declare function toggleRightMenu(): void;
declare function toggleLeftMenu(): void;
declare function closeLeftMenu(): void;
declare function openLeftMenu(width?: number): void;
declare function closeRightMenu(): void;
declare function openRightMenu(width?: number): void;
declare let geocropping: Geocropping;
