/// <reference path="tsd.d.ts" />
/// <reference path="../myTypings/tsd.d.ts" />
/// <reference types="leaflet" />
declare class MyNotification {
    options: {
        timeout: number;
        position: string;
    };
    notif: any;
    box: any;
    constructor(map: L.Map);
    ConnectionOK(): void;
    ConnectionNOK(): void;
    DisconnectionOK(): void;
    DisconnectionNOK(): void;
    ValidatedOK(): void;
    ValidatedNOK(): void;
    InvalidatedOK(): void;
    InvalidatedNOK(): void;
    LocalisationNOK(): void;
    AutoServerChanged(): void;
    ServerChanged(): void;
    ClicPositionON(): void;
    ClicPositionOFF(): void;
    ChangementsTrouve(changes: string[] | number[]): void;
    AjoutSinglePrefNOK(): void;
    AjoutSinglePrefOK(): void;
    SignalisationExterieur(): void;
}
