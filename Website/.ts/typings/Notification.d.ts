/// <reference path="tsd.d.ts" />
/// <reference path="../myTypings/tsd.d.ts" />
/// <reference types="leaflet" />
declare class MyNotification {
    options: {
        timeout: number;
    };
    notif: any;
    box: any;
    constructor(map: L.Map);
    ConnectionOK(): void;
    ConnectionNOK(): void;
    DisconnectionOK(): void;
    DisconnectionNOK(): void;
    ValidatedOK(): void;
    ValidatedNOK(): void;
    LocalisationNOK(): void;
    AutoServerChanged(): void;
    ServerChanged(): void;
    ClicPositionON(): void;
    ClicPositionOFF(): void;
}
