/// <reference path="tsd.d.ts" />
/// <reference path="../myTypings/tsd.d.ts" />
/// <reference path="../myTypings/OauthOsm.d.ts" />
/// <reference types="jquery" />
declare class OauthOsm {
    _baseUrl: string;
    _devApi: boolean;
    _connected: boolean;
    readonly isConnected: boolean;
    constructor();
    static readonly apiRequest: IDictionary<OauthOsm.ApiRequest>;
    _sendRequest(apiRequest: OauthOsm.ApiRequest): JQueryPromise<{}>;
    getServerDetails(): JQueryPromise<{}>;
    getUserDetails(): JQueryPromise<{}>;
    connect(): JQueryPromise<{}>;
    disconnect(): JQueryPromise<{}>;
}
declare function getCookies(name?: string | string[]): {
    key: string;
    value: string;
}[];
declare function deleteCookie(name: string, path?: string): void;
declare function localSave(name: string, value: string, options?: {
    useLocalStorageOrCookie?: boolean;
    useLocalStorage?: boolean;
    useCookie?: boolean;
    systemUsed?: any;
    cookie?: {
        expires?: string;
        path?: string;
    };
}): void;
declare function localLoad(name: string, options?: {
    useLocalStorageOrCookie?: boolean;
    useLocalStorage?: boolean;
    useCookie?: boolean;
    cookie?: {};
    systemUsed?: any;
}): string;
declare function localDelete(name: string, options?: {
    useLocalStorage?: boolean;
    useCookie?: boolean;
    cookie?: {
        path?: string;
    };
}): void;
