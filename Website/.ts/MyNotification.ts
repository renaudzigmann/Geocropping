/// <reference path="typings/tsd.d.ts" />
/// <reference path="myTypings/tsd.d.ts" />

class MyNotification {

	options = { timeout: 4000, position: "bottomleft" };
	notif;
	box;

	constructor(map: L.Map) {
		this.box = (L.control as any).messagebox(this.options).addTo(map);
		this.notif = {
			ConnectionOK: "Connexion au compte OSM réussie",													//DONE
			ConnectionNOK: "Echec de la connexion au compte OSM",												//DONE BUT NOT TESTED
			DisconnectionOK: "Déconnexion d'OSM réussie",														//DONE
			DisconnectionNOK: "Echec de la deconnexion du compte OSM",											//DONE BUT NOT TESTED
			ValidatedOK: "Element validé",																		//DONE
			ValidatedNOK: "Echec de la validation de l'élement",												//DONE BUT NOT TESTED
			InvalidatedOK: "Element signalé",
			InvalidatedNOK: "Echec de la signalisation de l'élement",
			LocalisationNOK: "Echec de la localisation",														//DONE
			AutoServerChanged: "Problème de connexion avec le server, changement automatique",					//DONE BUT NOT TESTED
			ServerChanged: "Changement de server éffectué",														//DONE
			ClicPositionON: "Arrêt de la geolocalisation, position au clic.",									//DONE
			ClicPositionOFF: "Reprise de la geolocalisation",													//DONE
			AjoutSinglePrefNOK: "Echec de l'ajout de la préférence",
			AjoutSinglePrefOK: "Ajout de la préférence éffectué",
			ChangementsTrouve: "Les éléments suivants ont été modifiés : ",
			SignalisationExterieur: "L'objet a déjà été signalisé par quelqu'un d'exterieur à Geocropping. Pas de changement opéré."
		};
	}

	ConnectionOK() {
		this.box.show(this.notif.ConnectionOK);
	}
	ConnectionNOK() {
		this.box.show(this.notif.ConnectionNOK);
	}
	DisconnectionOK() {
		this.box.show(this.notif.DisconnectionOK);
	}
	DisconnectionNOK() {
		this.box.show(this.notif.DisconnectionNOK);
	}
	ValidatedOK() {
		this.box.show(this.notif.ValidatedOK);
	}
	ValidatedNOK() {
		this.box.show(this.notif.ValidatedNOK);
	}
	InvalidatedOK(){
		this.box.show(this.notif.InvalidatedOK);
	}
	InvalidatedNOK(){
		this.box.show(this.notif.InvalidatedNOK);
	}
	LocalisationNOK() {
		this.box.show(this.notif.LocalisationNOK);
	}
	AutoServerChanged() {
		this.box.show(this.notif.AutoServerChanged);
	}
	ServerChanged() {
		this.box.show(this.notif.ServerChanged);
	}
	ClicPositionON() {
		this.box.show(this.notif.ClicPositionON);
	}
	ClicPositionOFF() {
		this.box.show(this.notif.ClicPositionOFF);
	}
	ChangementsTrouve(changes: string[] | number[]) {
		if (changes.length === 0) {} 
		else {
			let text = this.notif.ChangementsTrouve;
			for (let i = 0; i < changes.length; i++) {
				text += `<a style='color: blue; ' href="https://www.openstreetmap.org/edit?node=${changes[i]}" target="_blank"> ${changes[i]}  </a>`;
			}
			this.box.options.timeout += changes.length * 1000;
			this.box.show(text);
			this.box.options.timeout = 4000;
		}
	}
	AjoutSinglePrefNOK(){
		this.box.show(this.notif.AjoutSinglePrefNOK);
	}
	AjoutSinglePrefOK(){
		this.box.show(this.notif.AjoutSinglePrefOK);
	}
	SignalisationExterieur(){
		this.box.show(this.notif.SignalisationExterieur);
	}
}