<?php
require_once ('OAuth.php');
require_once ('Array2XML.php');
require_once ('OsmConsumer.php');
class Geocropping
{

    private $oauthConsumer;
    public $oauthRequestToken;
    public $oauthAccessToken;

    private $authUrl;
    private $apiUrl;
    private $oauth_consumer_key;
    private $oauth_secret;

    function __construct($type = 'PROD')
    {

        // récuper les tokens de l'application
        global $consumer;
        if ($type === 'DEV') {
            // DEV


            $this->authUrl = 'https://master.apis.dev.openstreetmap.org';
            $this->apiUrl = 'https://master.apis.dev.openstreetmap.org'; 
            $this->oauth_consumer_key = $consumer['DEV']['key']; // récupéré sur osm
            $this->oauth_secret = $consumer['DEV']['secret']; // récupéré sur osm

        }
        elseif ($type === 'PROD') {
            // PROD

            $this->authUrl = 'https://www.openstreetmap.org';
            $this->apiUrl = 'https://www.openstreetmap.org';
            $this->oauth_consumer_key = $consumer['PROD']['key']; // récupéré sur osm
            $this->oauth_secret = $consumer['PROD']['secret']; // récupéré sur osm

        }

        $this->oauthConsumer = new OAuthConsumer($this->oauth_consumer_key, $this->oauth_secret);
        $this->oauthRequestToken = new OAuthToken('', '');
        $this->oauthAccessToken = new OAuthToken('', '');
    }

    public function encrypteTokenValue($tokenValue)
    {
        return openssl_encrypt($tokenValue, "AES-128-ECB", $this->oauthConsumer->key);
    }

    public function decrypteTokenValue($tokenValue)
    {
        $tokenValue = str_replace(' ', '+', $tokenValue);
        return openssl_decrypt($tokenValue, "AES-128-ECB", $this->oauthConsumer->key);
    }


    // Oauth 1 : récupérer un token pour identifier une session pour la connexion
    public function requestToken($params = [])
    {
        $url = $this->authUrl . '/oauth/request_token';
        // préconfiguration de la requete
        $req = OAuthRequest::from_consumer_and_token($this->oauthConsumer, null, "GET", $url, $params);
        // signer la requete avec le type de signature en utilisant seulement le consumer comme clé
        $req->sign_request(new OAuthSignatureMethod_HMAC_SHA1, $this->oauthConsumer, null);
        $str = file_get_contents($req->to_url());

        if ($str != false) {
            parse_str($str, $data);
            $this->oauthRequestToken->key = $data['oauth_token'];
            $this->oauthRequestToken->secret = $data['oauth_token_secret'];
            return true;
        }
        return [$http_response_header, $req->to_url()];
    }

    // Oauth 2 : récupérer l'adresse pour la connexion de l'utilisateur (sur OSM)
    // le callback par défaut est l'url actuel avec l'action access_token
    public function getAuthorizeUrl($token = null, $callback = "")
    {
        // récupérer le token de la class si aucun n'est passé en paramètre
        $token = empty($token) ? $this->oauthRequestToken : $token;
        $url = $this->authUrl . '/oauth/authorize';
        // callback = url vers le getAccessToken
        if(!is_string($callback) || empty($callback)){
            $callback = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_URI'] . '/auth/?' . http_build_query([action => 'access_token'], '', '&');
        }
        return ($url . '?' . http_build_query([oauth_token => $token->key, oauth_callback => $callback], '', '&'));
    }

    // Oauth 3 : récupérer la session OSM de l'utilisateur
    public function getAccessToken($token = null)
    {
        $consumer = $this->oauthConsumer;
        $token = empty($token) ? $this->oauthRequestToken : $token;
        // parametres ajouté a la signature
        $params = ['oauth_token' => $token->key];
        $endpoint = $this->authUrl . '/oauth/access_token';
        // préconfiguration de la requete
        $req = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $endpoint, $params);
        // signer la requete avec le type de signature en utilisant le consumer et le request_token comme clé
        $req->sign_request(new OAuthSignatureMethod_HMAC_SHA1, $consumer, $token);
        $str = file_get_contents($req->to_url());

        if ($str != false) {
            parse_str($str, $data);
            $this->oauthAccessToken->key = $data['oauth_token'];
            $this->oauthAccessToken->secret = $data['oauth_token_secret'];
        }

        return $this->oauthAccessToken;
    }


    function __toString()
    {
        $req_secret = $this->oauthRequestToken->secret;;
        $access_secret = $this->oauthAccessToken->secret;;

        return json_encode([
            'oauth_request_token' => [
                'key' => $this->oauthRequestToken->key,
                'secret' => $req_secret
            ],
            'oauth_access_token' => [
                'key' => $this->oauthAccessToken->key,
                'secret' => $access_secret
            ]
        ]);
    }

    // faire des requete auprès de OSM
    private function requestAPI($endpoint, $token = null, $params = array(), $method = 'GET', $content = '')
    {
        $consumer = $this->oauthConsumer;
        $token = empty($token) ? $this->oauthAccessToken : $token;
        // parametres ajouté a la signature
        $params['oauth_token'] = $token->key;
        $endpoint = $this->apiUrl . $endpoint;
        // préconfiguration de la requete
        $req = OAuthRequest::from_consumer_and_token($consumer, $token, $method, $endpoint, $params);
        // signer la requete avec le type de signature en utilisant le consumer et le request_token comme clé
        $req->sign_request(new OAuthSignatureMethod_HMAC_SHA1, $consumer, $token);

        // informations nécessaire pour le requete
        $options = [
            'http' => [
                'method' => $method,
                'header' => $req->to_header() . "\r\n",
                'ignore_errors' => true
            ]
        ];
        $url = $req->to_url();

        // ajouter le contenu a la requete
        if (!empty($content)) {
            $options['http']['content'] = $content;
            $options['http']['header'] .= 'Content-Type: ' . self::getMime($content) . "\r\n";
        }

        $str = file_get_contents($url, false, stream_context_create($options));
        /*if ($str==false) {
            return $this->parseHeaders($http_response_header);
        }*/
        $headers = $this->parseHeaders($http_response_header);

        // si la réponse est en xml, transformer en json (plus facile a traiter en javascript)
        if ($headers['Content-Type'] && (strpos($headers['Content-Type'], 'application/xml') !== false || strpos($headers['Content-Type'], 'text/xml') !== false)) {
            $headers['Content-Type'] = 'application/json';
            $str = simplexml_load_string($str);
        }
        return [res => [body => $str, headers => $headers], req => [body => $content/*, header => $options['http']['header'], url => $url*/]];
    }

    static function getMime($data)
    {
        return (new finfo(FILEINFO_MIME_TYPE))->buffer($data);
    }

    private function parseHeaders($headers)
    {
        $head = [];
        foreach ($headers as $k => $v) {
            $t = explode(':', $v, 2);
            if (isset($t[1])) {
                $head[trim($t[0])] = trim($t[1]);
            }
            else {
                $head[] = $v;
                $out = null;
                if (preg_match("#HTTP/[0-9\.]+\s+([0-9]+)#", $v, $out)) {
                    $head["response_code"] = intval($out[1]);
                }
            }
        }
        return $head;
    }

    // gérer les différente requete (peut être transformé en plusieur fonction)
    public function handleRequestAPI($command, $options, $content)
    {
        if (!is_array($options)) {
            $options = [];
        }
        // http://wiki.openstreetmap.org/wiki/API_v0.6


        switch ($command) {
            /////////
            /// GET


            case 'GetServerCapabilities' :
                $url = '/api/capabilities';
                return $this->requestAPI($url, null, $options, 'GET');

            case 'GetPermissions' :
                $url = '/api/0.6/permissions';
                return $this->requestAPI($url, null, $options, 'GET');

            case 'GetDataBoundsBox' :
                $left = $options['left'] ? $options['left'] : '0';
                $bottom = $options['bottom'] ? $options['bottom'] : '0';
                $right = $options['right'] ? $options['right'] : '0.5';
                $top = $options['top'] ? $options['top'] : '0.5';
                $url = '/api/0.6/map?bbox=' . $left . ',' . $bottom . ',' . $right . ',' . $top;
                return $this->requestAPI($url, null, $options, 'GET');

            case 'GetUserDetails' :
                $url = '/api/0.6/user/details';
                return $this->requestAPI($url, null, $options, 'GET');

            case 'GetUserPreferences' :
                $url = '/api/0.6/user/preferences';
                return $this->requestAPI($url, null, $options, 'GET');
            case 'UpdateUserPreferences':
                $url = '/api/0.6/user/preferences';

                // pour éviter le problème sur le format des données

                if (!is_string($content)) {
                    $content = json_encode($content);
                }
                $content = json_decode($content, true);

                $xml = Array2XML::createXML('osm', $content);
                $content = $xml->saveXML();
                //var_dump($options,$content);
                return $this->requestAPI($url, null, $options, 'PUT', $content);
                //TODODO écrire pref 
            case 'UpdateSingleUserPreference':
                $url = '/api/0.6/user/preferences/' . $options['key'];
                $content = (!empty($content)) ? $content : $options['value'];
                return $this->requestAPI($url, null, $options, 'PUT', $content);
            case 'GetUserId' :
                $id = $options['id'] ? $options['id'] : '1';
                $url = '/api/0.6/user/' . $id;
                return $this->requestAPI($url, null, $options, 'GET');

            case 'GetNoteId' :
                $id = $options['id'] ? $options['id'] : '1';
                $format = $options['format'] ? '.' . $options['format'] : '';
                $url = '/api/0.6/notes/' . $id . $format;
                return $this->requestAPI($url, null, $options, 'GET');

            case 'GetNotesBoundsBox' :
                $left = $options['left'] ? $options['left'] : '0';
                $bottom = $options['bottom'] ? $options['bottom'] : '0';
                $right = $options['right'] ? $options['right'] : '10';
                $top = $options['top'] ? $options['top'] : '10';
                $format = $options['format'] ? '.' . $options['format'] : '';
                $url = '/api/0.6/notes' . $format . '?bbox=' . $left . ',' . $bottom . ',' . $right . ',' . $top;
                return $this->requestAPI($url, null, $options, 'GET');

            case 'SearchNotes' :
                $query = $options['query'] ? $options['query'] : 'hello';
                $limit = $options['limit'] ? $options['limit'] : '100';// 1 -> 10000

                $closed = $options['closed'] ? $options['closed'] : '7';// Specifies the number of days a bug needs to be closed to no longer be returned
                // A value of 0 means only open bugs are returned.
                // A value of -1 means all bugs are returned.

                $format = $options['format'] ? '.' . $options['format'] : '';

                $url = '/api/0.6/notes/search' . $format . '?q=' . $query . '&limit=' . $limit . '&closed=' . $closed;
                return $this->requestAPI($url, null, $options, 'GET');

            case 'GetChangesetId' :
                $id = $options['id'] ? $options['id'] : '1';
                $discussion = $options['discussion'] ? $options['discussion'] : 'true';
                $url = '/api/0.6/changeset/' . $id . '?include_discussion=' . $discussion;
                return $this->requestAPI($url, null, $options, 'GET');

            case 'GetChangeOfChangesetId' :
                $id = $options['id'] ? $options['id'] : '1';
                $url = '/api/0.6/changeset/' . $id . '/download';
                return $this->requestAPI($url, null, $options, 'GET');

            case 'GetElementId' :
                $id = $options['id'] ? $options['id'] : '1';
                $type = strtolower($options['type']);// [node|way|relation]


                if (!preg_match('/^(node|way|relations)$/', $type)) {
                    $type = 'node';
                }
                $url = '/api/0.6/' . $type . '/' . $id;
                return $this->requestAPI($url, null, $options, 'GET');
            case 'GetElementIdHistory' :
                $id = $options['id'] ? $options['id'] : '1';
                $type = strtolower($options['type']);// [node|way|relation]


                if (!preg_match('/^(node|way|relations)$/', $type)) {
                    $type = 'node';
                }
                $url = '/api/0.6/' . $type . '/' . $id . '/history';
                return $this->requestAPI($url, null, $options, 'GET');
            case 'GetElementIdVersion' :
                $id = $options['id'] ? $options['id'] : '1';
                $type = strtolower($options['type']);// [node|way|relation]


                if (!preg_match('/^(node|way|relations)$/', $type)) {
                    $type = 'node';
                }
                $url = '/api/0.6/' . $type . '/' . $id . '/version';
                return $this->requestAPI($url, null, $options, 'GET');
            case 'GetElementsId' : // ?


                $id = $options['ids'] ? $options['ids'] : ['100', '102'];
                if (is_array($id)) {
                    $id = implode(',', $id);
                }
                $type = strtolower($options['type']);// [node|way|relation]


                if (!preg_match('/^(nodes|ways|relations)$/', $type)) {
                    $type = 'nodes';
                }
                $url = '/api/0.6/' . $type . '?' . $type . '=' . $id;
                return $this->requestAPI($url, null, $options, 'GET');
            case 'GetElementIdRelations' :
                $id = $options['id'] ? $options['id'] : '100';
                $type = strtolower($options['type']);// [node|way|relation]


                if (!preg_match('/^(node|way|relations)$/', $type)) {
                    $type = 'node';
                }
                $url = '/api/0.6/' . $type . '/' . $id . '/relations';
                return $this->requestAPI($url, null, $options, 'GET');
            case 'GetNodeIdWays' : // ?


                $id = $options['id'] ? $options['id'] : '1';
                $url = '/api/0.6/ways/' . $id . '/ways';
                return $this->requestAPI($url, null, $options, 'GET');
            case 'GetElementIdFull' :// ?


                $id = $options['id'] ? $options['id'] : '1';
                $type = strtolower($options['type']);// [way|relation]


                if (!preg_match('/^(way|relations)$/', $type)) {
                    $type = 'way';
                }
                $url = '/api/0.6/' . $type . '/' . $id . '/full';
                return $this->requestAPI($url, null, $options, 'GET');

            /////////
            /// POST



            case 'CreateNote' :
                $lat = $options['lat'];
                $lon = $options['lon'];
                $text = $options['text'];
                $url = '/api/0.6/notes';
                return $this->requestAPI($url, null, $options, 'POST');

            case 'CreateNoteComment' :
                $id = $options['id'];
                $text = $options['text'];
                $url = '/api/0.6/notes/' . $id . '/comment';
                return $this->requestAPI($url, null, $options, 'POST');

            case 'CloseNote' :
                $id = $options['id'];
                $text = $options['text'];
                $url = '/api/0.6/notes/' . $id . '/close';
                return $this->requestAPI($url, null, $options, 'POST');

            case 'ReopenNote' :
                $id = $options['id'];
                $text = $options['text'];
                $url = '/api/0.6/notes/' . $id . '/reopen';
                return $this->requestAPI($url, null, $options, 'POST');

            /////////
            /// PUT


            case 'CreateChangeset' :
                $element = $options['element'];
                unset($options['element']);
                                // pour éviter le problème sur le format des données


                if (!is_string($element)) {
                    $element = json_encode($element);
                }
                $element = json_decode($element, true);

                if (isset($element['changeset'])) {
                    if (!is_array($element['changeset']['tag'])) { // si un tableau de tag n'est pas présent, en créer un


                        $element['changeset']['tag'] = [];
                    }
                    $element['changeset']['tag'][] = ['@attributes' => ['k' => 'created_by', 'v' => 'Geocropping']];
                }
                /*$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><osm/>');

                $this->json2xml($element, $xml);*/

                $xml = Array2XML::createXML('osm', $element);
                $element = $xml->saveXML();
                $url = '/api/0.6/changeset/create';
                return $this->requestAPI($url, null, $options, 'PUT', $element);

            case 'UpdateChangeset' :
                $id = $options['id'];
                $element = $options['element'];
                unset($options['element']);

                // pour éviter le problème sur le format des données


                if (!is_string($element)) {
                    $element = json_encode($element);
                }
                $element = json_decode($element, true);

                if (isset($element['changeset'])) {
                    if (!is_array($element['changeset']['tag'])) { // si un tableau de tag n'est pas présent, en créer un


                        if (is_object($element['changeset']['tag'])) { // Mais si un tag est présent


                            $element['changeset']['tag'] = [$element['changeset']['tag']];
                        }
                        else {
                            $element['changeset']['tag'] = [];
                        }
                    }

                    $element['changeset']['tag'][] = ['@attributes' => ['k' => 'created_by', 'v' => 'Geocropping']];
                }

                /*$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><osm/>');

                $this->json2xml($element, $xml);*/

                $xml = Array2XML::createXML('osm', $element);
                $element = $xml->saveXML();
                $url = '/api/0.6/changeset/' . $id;
                return $this->requestAPI($url, null, $options, 'PUT', $element);

            case 'CloseChangeset' :
                $id = $options['id'];

                $url = '/api/0.6/changeset/' . $id . '/close';
                return $this->requestAPI($url, null, $options, 'PUT');

            case 'CreateElement' :
                $type = $options['type'];
                $element = $options['element'];
                unset($options['element']);

                // pour éviter le problème sur le format des données


                if (!is_string($element)) {
                    $element = json_encode($element);
                }
                $element = json_decode($element, true);

                /*$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><osm/>');

                $this->json2xml($element, $xml);*/
                $xml = Array2XML::createXML('osm', $element);
                $element = $xml->saveXML();
                $url = '/api/0.6/' . $type . '/create';
                return $this->requestAPI($url, null, $options, 'PUT', $element);

            case 'UpdateElement' :
                $before = $options['before'];
                $after = $options['after'];

                // pour éviter le problème sur le format des données


                if (!is_string($before)) {
                    $before = json_encode($before);
                }
                $before = json_decode($before, true);
                if (!is_string($after)) {
                    $after = json_encode($after);
                }
                $after = json_decode($after, true);

                unset($after['@attributes']);
                unset($before['@attributes']);

                $type = array_keys($before)[0];
                $id = '';
                $changeset = $options['changeset'];

                if (empty($after[$type]['@attributes'])) {
                    $after[$type]['@attributes'] = [];
                }

                if (!empty($before[$type]['@attributes'])) {
                    if (!empty($before[$type]['@attributes']['version'])) {
                        $after[$type]['@attributes']['version'] = $before[$type]['@attributes']['version'];
                    }
                    if (!empty($before[$type]['@attributes']['id'])) {
                        $id = $after[$type]['@attributes']['id'] = $before[$type]['@attributes']['id'];
                    }
                }

                $after[$type]['@attributes']['changeset'] = $changeset;

                /*$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><osm/>');

                $this->json2xml($after, $xml);*/
                $xml = Array2XML::createXML('osm', $after);
                $element = $xml->saveXML();
                $url = '/api/0.6/' . $type . '/' . $id;
                return $this->requestAPI($url, null, $options, 'PUT', $element);

            /////////
            /// DELETE



            default :
                http_response_code(400);
                return 'Invalide Command: ' . $command;
        }
    }
}
