<?php
require_once ('geocropping.php');
require_once ('test.php');

                // d * h  * min * s
$timeoutSession = 30 * 24 * 60 * 60; // ~1 mois
ini_set("session.gc_maxlifetime", $timeoutSession);
session_set_cookie_params($timeoutSession);
session_name('OSM');

session_start();

if ($_REQUEST['devMode']) {
    $_SESSION['devMode'] = $_REQUEST['devMode'];
}

// my_trace(var_export($_REQUEST,true));

// gère les differentes requetes utilisant OAuth
$geocropping = new Geocropping($_SESSION['devMode'] === 'true' ? 'DEV' : 'PROD');
switch ($_REQUEST['action']) {
    case null :
    case '' :
        // REQUEST TOKEN
        if ( ($err = $geocropping->requestToken()) === true) { // 1
            // garder le token pour le réustiliser dans le access token
            $_SESSION['request_token'] = $geocropping->oauthRequestToken;
            $location = $geocropping->getAuthorizeUrl();
            header('Location: ' . $location); // 2


        }
        else {
            // afficher tout en cas d'erreur
            header('Content-Type: application/json');
            die(json_encode($err));
        }
        break;
    case 'access_token' :
        // ACCESS TOKEN
        if (!empty($_REQUEST['oauth_token']) && !empty($_SESSION['request_token'])) {
            //TODO: vérifier si le token de la requete et le même que celui de la session ?
            $geocropping->oauthRequestToken = $_SESSION['request_token'];
            unset($_SESSION['request_token']);
            // beforeSend:(jqxhr, settings) =>{
            //     console.log([jqxhr,settings]);
            //     jqxhr.setRequestHeader("Access-Control-Allow-Origin","geocropping.xsalto.com");
            // },
            header('Access-Control-Allow-Origin:*');
            $tokenAccess = $geocropping->getAccessToken(); // 3
            if (!empty($tokenAccess->key) && !empty($tokenAccess->secret)) {
                //sauvegarde le token de l'utilisateur et retourner sur la carte
                $_SESSION['access_token'] = $tokenAccess;
                $callback = '../';
                header('Location: ' . $callback);
            }
        }
        else {
            http_response_code(400);
        }
        break;
    case 'request_api' :
        // API

        // faire la requete avec les paramètres associer
        $geocropping->oauthAccessToken = $_SESSION['access_token'];
        $content = '';
        if(!empty($_REQUEST['options']) && !empty($_REQUEST['options']['content'])){
            $content = $_REQUEST['options']['content'];
            unset($_REQUEST['options']['content']);
        }
        $data = $geocropping->handleRequestAPI($_REQUEST['command'], $_REQUEST['options'], $content); // 4

        /*if ($_SESSION['devMode'] && $data['res']['headers']['response_code'] == 401) {
            $data['res']['headers']['response_code'] = 400;
        }*/
        
        // définir le status de retour comme celui de la requete OSM
        http_response_code($data['res']['headers']['response_code']);
        
        // réponse ok
        if ($data['res']['headers']['response_code'] >= 200 && $data['res']['headers']['response_code'] <= 299) {

            // afficher les donné du retour si pas d'erreur

            //définir le content-type de retour comme celui de la requete OSM
            header('Content-Type: ' . $data['res']['headers']['Content-Type']);
            if (strpos($data['res']['headers']['Content-Type'], 'application/json') !== false) {
                // le contenu est du json, l'encoder
                die(json_encode($data['res']['body']));
            }
            elseif (!empty($data['res']['body'])) {
                die($data['res']['body']);
            }
            else {
                // si aucun contenu, afficher tout les données ?
                header('Content-Type: application/json');
                die(json_encode($data));
            }
        }
        else {
            // afficher tout en cas d'erreur
            header('Content-Type: application/json');
            die(json_encode($data));
        }

        break;
    case 'is_connected' :
        // vérifier si un token est présent
        $connected = empty($_SESSION['access_token']) ? 'false' : 'true';
        header('Content-Type: application/json');
        die(json_encode([connected => $connected]));
        break;
    case 'logout' :
        // supprimer la session
        if (!empty($_SESSION['access_token'])) {
            session_destroy();
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        }
        http_response_code(204);
        break;
    default :
        http_response_code(400);
}
