<?php
if (empty($_SERVER['HTTPS'])) {
    header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    die();
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv='content-type' content='text/html; charset=utf-8' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Geocropping</title>

    <!-- LEAFLET -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-responsive-popup@0.2.0/leaflet.responsive.popup.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet-responsive-popup@0.2.0/leaflet.responsive.popup.css" />

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
        crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

    <!--
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
        crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
        -->

    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/leaflet-messagebox.css" />
    <script src="js/osmtogeojson.js"></script>
    <!-- <script src="js/leaflet.pattern.js"></script> -->
    <script src="js/leaflet-heat.js"></script>
    <script src="js/fr.js"></script>
    <script src="js/OauthOsm.js"></script>
    <script src="js/MyNotification.js"></script>
    <script src="js/Geocropping.js"></script>
    <script src="js/index.js"></script>
    <script src="js/leaflet-messagebox.js"></script>
</head>

<body>
    <div class="side-nav fit-left fit-top menu" style="left:-250px;width:250px">
        <div class="side-nav-body">
        </div>
    </div>
    <div id="mapArea">
        <div id="map"></div>
        <div class="menu map-menu fit-right fit-top">
            <div>
                <a href="javascript:toggleRightMenu();">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </a>
            </div>
            <div class="map-menu-item" style="display:none;">
                <hr>
                <img id="userImage" class="img-responsive" alt="User Image" title="User Image" src="" />
            </div>
        </div>
    </div>
    <div class="side-nav fit-right fit-top menu" style="right:-250px;width:250px">
        <div class="side-nav-body">
            <div id="userAction">
                <button class="btn" id="login">Connexion</button>
                <button class="btn" id="logout" style="display:none;">Déconnexion</button>
            </div>
            <div id="userInfo"><br>
                <div id="userName">User Name</div><br>
                <div id="userNbValid">Validatons : </div>
                <div id="userNbInvalid">Signalements : </div>
            </div>
            <hr>
            <div id="menu">
                <div class="form-group" id="modeDisplayFar">
                    <label>Type d'élément sur affichage global</label>
                    <div class="radio">
                        <label><input type="radio" name="modeDisplayFar" value="validated" checked>Validé</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="modeDisplayFar" value="warned">Indécis</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="modeDisplayFar"  value="alert">A valider</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="modeDisplayFar"  value="invalidated">Signalé</label>
                    </div>
                </div>
                <!--div class="form-group">
                    <label for="urlInterpreter">Serveur:</label>
                    <select class="form-control" id="urlInterpreter">
                    </select>
                </div-->
                <div class="checkbox">
                    <label><input type="checkbox" id="clickPos" checked="true"> Utiliser la localisation automatique</label>
                </div>
                <!-- Option se trouvant das le menu de droite permettant de se connecter au server dev d'OSM pour ne rien casser sur le vrai. Décommenter pour utiliser -->
               <!--  <div class="checkbox">
                    <label><input type="checkbox" id="devMode"> Dev API</label>
                </div> -->
            </div>
            <div class="log">
                <a href="guide.html">Mode d'emploi</a>
                <a href="https://framagit.org/renaudzigmann/Geocropping">Framagit</a>
                <a href="https://framagit.org/renaudzigmann/Geocropping/commits/master">Logs des mises à jour</a>
                <hr />
                <p>Geocropping - Version alpha</p>
            </div>
        </div>
    </div>

</body>

</html>