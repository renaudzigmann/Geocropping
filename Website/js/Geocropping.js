"use strict";
var Geocropping = (function () {
    function Geocropping() {
        var _this = this;
        this._onMobileApp = false;
        this._map = null;
        this._icon = {
            red: L.icon({
                iconUrl: './img/marker/red.png',
                iconSize: [32, 50],
                iconAnchor: [16, 50]
            }),
            green: L.icon({
                iconUrl: './img/marker/green.png',
                iconSize: [32, 50],
                iconAnchor: [16, 50]
            }),
            blue: L.icon({
                iconUrl: './img/marker/blue.png',
                iconSize: [32, 50],
                iconAnchor: [16, 50]
            })
        };
        this._user = Geocropping.DEFAULT.USER;
        this._app_heatLayer_options = {
            minOpacity: 0.9,
            radius: 10,
            blur: 5,
            gradient: {
                0.9: 'white',
                0.95: 'cyan',
                1: 'blue'
            }
        };
        this._lastUserLocation = null;
        this._featureDisplayedAround = {};
        this._featureDisplayedFar = {};
        this._map_minZoom = 10;
        this._map_maxZoom = 19;
        this._app_server_interpreter_list = {
            Principal: "/overpass-interpreter.php",
        };
        this._app_distance_search = 20;
        this._app_distance_hide = 55;
        this._app_distance_search_far = 1000;
        this._app_display_far = null;
        this._app_distance_travel_to_search = 5;
        this._app_limit_feature_displayed = -1;
        this._app_time_locate_interval = 5000;
        this._app_click_position_actived = false;
        this._style_alert_far = {
            value: 1,
            color: "#FF0000",
            display: true
        };
        this._style_warning_far = {
            value: 0.99,
            color: "#FF7800",
            display: true
        };
        this._style_ok_far = {
            value: 0.98,
            color: "#00FF00",
            display: true
        };
        this._style_invalid_far = {
            value: 0.97,
            color: "#000000",
            display: true
        };
        this._style_alert = {
            fillColor: "#ee0000",
            color: "#ff0000",
            weight: 2,
            opacity: 1,
            fillOpacity: 0.0,
            fill: true
        };
        this._style_warning = {
            fillColor: "#ee6700",
            color: "#ff7800",
            weight: 2,
            opacity: 1,
            fillOpacity: 0.0,
            fill: true
        };
        this._style_ok = {
            fillColor: "#00ee00",
            color: "#00ff00",
            weight: 2,
            opacity: 1,
            fillOpacity: 0.0,
            fill: true
        };
        this._style_selected = {
            fillColor: "#0000ee",
            color: "#0000ff",
            weight: 2,
            opacity: 0.3,
            fillOpacity: 0.3,
            fill: true
        };
        this._style_invalid = {
            fillColor: "#000000",
            color: "#000000",
            weight: 2,
            opacity: 1,
            fillOpacity: 0.0,
            fill: true
        };
        this._osm = new OauthOsm();
        this._userMarker = L.marker([0, 0], {
            clickable: false,
            keyboard: false,
            title: 'User',
            icon: this._icon.blue
        });
        this._geoJSONLayers = L.geoJSON();
        this._heatLayerFar = L.heatLayer([]);
        this.setDisplayModeFar('validated');
        this.updateUserDetails();
        this._app_server_interpreter_index = Object.keys(this._app_server_interpreter_list)[0];
        var $osm = $(this._osm);
        $osm.on("requestError", function (data) {
            console.error('requestError', data);
            $(_this).trigger('requestError', [data]);
        });
        $osm.on('disconnected', function () {
            $(_this).trigger('userDisconnected');
            _this._user = Geocropping.DEFAULT.USER;
        });
        $osm.on('connected', function () {
            $(_this).trigger('userConnected');
            _this.updateUserDetails();
            _this._getStats();
        });
        var queryPending = 0;
        var timer = null;
        $(document).ajaxSuccess(function (e) {
            queryPending -= 1;
            if (_this._userMarker) {
                _this._userMarker.setIcon(_this._icon.green);
            }
        });
        $(document).ajaxError(function (e) {
            queryPending -= 1;
            if (_this._userMarker) {
                _this._userMarker.setIcon(_this._icon.red);
            }
        });
        $(document).ajaxSend(function (e) {
            queryPending += 1;
            if (_this._userMarker) {
                _this._userMarker.setIcon(_this._icon.blue);
            }
        });
        $(document).ajaxComplete(function (e) {
            timer = setTimeout(function () {
                if (_this._userMarker) {
                }
            }, 2000);
        });
        $(document).ajaxStart(function () {
            clearTimeout(timer);
        });
    }
    Object.defineProperty(Geocropping, "DEFAULT", {
        get: function () {
            return {
                USER: {
                    account_created: new Date(0, 0, 0, 0, 0, 0, 0),
                    display_name: "",
                    id: -1,
                    img: "",
                    description: "",
                    blocks: {
                        received: {
                            active: -1,
                            count: -1
                        }
                    },
                    changesets: {
                        count: -1
                    },
                    contributorTerms: {
                        agreed: false,
                        pd: false
                    },
                    languages: [],
                    message: {
                        received: {
                            count: -1,
                            unread: -1
                        },
                        sent: {
                            count: -1
                        }
                    },
                    role: null,
                    traces: {
                        count: -1
                    }
                }
            };
        },
        enumerable: true,
        configurable: true
    });
    Geocropping.prototype.connect = function () {
        var _this = this;
        var promise = this._osm.connect();
        promise.then(function () { _this.notify.ConnectionOK(); }, function () { _this.notify.ConnectionNOK(); });
        return promise;
    };
    Geocropping.prototype.userDisconnect = function () {
        var _this = this;
        var promise = this._osm.disconnect();
        promise.then(function () { _this.notify.DisconnectionOK(); }, function () { _this.notify.DisconnectionNOK(); });
        return promise;
    };
    Object.defineProperty(Geocropping.prototype, "isConnected", {
        get: function () {
            return this._osm.isConnected;
        },
        enumerable: true,
        configurable: true
    });
    Geocropping.prototype.updateUserDetails = function () {
        var _this = this;
        var promise = this._osm.getUserDetails();
        promise.then(function (data) {
            _this._user.account_created = data.user['@attributes'].account_created;
            _this._user.display_name = data.user['@attributes'].display_name;
            _this._user.id = data.user['@attributes'].id;
            _this._user.blocks.received.active = data.user.blocks.received['@attributes'].active;
            _this._user.blocks.received.count = data.user.blocks.received['@attributes'].count;
            _this._user.changesets.count = data.user.changesets['@attributes'].count;
            _this._user.contributorTerms.agreed = data.user['contributor-terms']['@attributes'].agreed;
            _this._user.contributorTerms.pd = data.user['contributor-terms']['@attributes'].pd;
            _this._user.description = data.user.description || "";
            _this._user.img = typeof (data.user.img) === "undefined" ? "" : data.user.img['@attributes'].href;
            _this._user.languages = data.user.languages.lang;
            _this._user.message.received.count = data.user.messages.received['@attributes'].count;
            _this._user.message.received.unread = data.user.messages.received['@attributes'].unread;
            _this._user.message.sent.count = data.user.messages.sent['@attributes'].count;
            _this._user.role = data.user.role || "";
            _this._user.traces.count = data.user.traces['@attributes'].count;
            $(_this).trigger('userDetailsUpdated', [_this._user]);
        });
        return promise;
    };
    Geocropping.prototype.getUserDetails = function () {
        return this._user.id !== -1 ? this._user : null;
    };
    Geocropping.prototype.isMapCreated = function () {
        return this._map instanceof L.Map;
    };
    Geocropping.prototype.createMap = function (map) {
        var _this = this;
        if (this.isMapCreated() || !(map instanceof HTMLElement) && document.getElementById(map) === undefined) {
            return false;
        }
        this._map = L.map(map, {
            worldCopyJump: true,
        }).fitWorld();
        this.notify = new MyNotification(this._map);
        L.tileLayer('https://osmtiles.xsalto.com/osm_tiles/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            maxZoom: this._map_maxZoom
        }).addTo(this._map);
        this._map.on('popupopen', function (popupEvent) {
            var feature = popupEvent.popup._source.feature;
            var updatePopup = function () {
                popupEvent.popup._updateLayout();
                popupEvent.popup._updatePosition();
            };
            var properties = feature.properties;
            var tags = properties.tags;
            var toTable = function (object) {
                var out = "<div class='table-responsive'><table class='table'>";
                $.each(object, function (index, value) {
                    var _a = [index, value], i = _a[0], v = _a[1];
                    var tag = "<!--tr><td>" + i + "</td><td>" + v + "</td></tr-->";
                    if (presets) {
                        if (presets[index + "/" + value] || presets[index]) {
                            if (presets[index + "/" + value]) {
                                v = presets[index + "/" + value]['name'];
                            }
                            else {
                                v = ((value === "yes") ? "Oui" : (value === "no" ? "Non" : v));
                            }
                            if (presets[index]) {
                                i = presets[index]['name'];
                            }
                            tag = "<tr><td>" + i + "</td><td>" + v + "</td></tr>";
                        }
                    }
                    out += tag;
                });
                return out += '</table></div>';
            };
            var content = "\n            <div class='popup-info'>\n                <div class='row valide-element'>";
            if (_this.isConnected) {
                content += "\n                    <div class=\"col-xs-12\" role=\"group\">\n                        <button type=\"button\" id=\"btnValidate\" data-id=\"" + properties.id + "\" data-type=\"" + properties.type + "\" class='btn btn-success col-xs-4'><span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span></button>\n                        <button data-toggle=\"button\"  aria-pressed\"false\" type=\"button\" id=\"btnError\" class='btn btn-danger col-xs-4'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>\n                        <button type=\"button\" id=\"btnSendToOSM\" class='btn btn-warning col-xs-4' onclick=\"window.location.href='https://www.openstreetmap.org/edit?" + properties.type + "=" + properties.id + "'\"><span class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\"></span></button>\n                        <!--<a target=\"_blank\" id=\"btnSendToOSM\" class='btn btn-warning col-xs-4' role='button' href=\"https://www.openstreetmap.org/edit?" + properties.type + "=" + properties.id + "\"><span class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\"></span></a>-->\n                    </div>\n                </div>\n                <div class='element-info row'>\n                    <!--div><span class='label label-info'>type</span> " + properties.type + "</div-->\n                    <!--div><span class='label label-info'>id</span> " + properties.id + "</div-->\n                    <div class=\"col-xs-12\">\n                        <!--span class='label label-info'>tags</span-->" + toTable(properties.tags) + "\n                    </div><br/><hr>\n                    <!--div><span class='label label-info'>relations</span> " + properties.relations.join(', ') + "</div-->\n                    <!--div><span class='label label-info'>meta</span> " + toTable(properties.meta) + "</div-->\n                    <!--pre>" + JSON.stringify(properties, null, 4) + "</pre-->";
                if (tags.fixme && tags.fixme !== "" && tags.note !== undefined) {
                    content += "\n                    <a style='color:red;' href=\"https://www.openstreetmap.org/edit?" + properties.type + "=" + properties.id + "\" target=\"_blank\"><span class=\"glyphicon glyphicon-warning-sign\" aria-hidden=\"true\"></span> L'objet a d\u00E9j\u00E0 \u00E9t\u00E9 d\u00E9clar\u00E9 comme erronn\u00E9, raison : \"" + tags.note + "\"</a>";
                }
                else if (tags.fixme && tags.fixme !== "") {
                    content += "\n                    <a style='color:red;' href= \"https://www.openstreetmap.org/edit?" + properties.type + "=" + properties.id + "\" target= \"_blank\" > <span class=\"glyphicon glyphicon-warning-sign\" aria- hidden=\"true\" > </span> L'objet a d\u00E9j\u00E0 \u00E9t\u00E9 d\u00E9clar\u00E9 comme erronn\u00E9, raison : \"" + tags.fixme + "\"</a>";
                }
                content += "\n                <div class='row valide-element'>\n                    <div class='collapse element-signal col-xs-12 toto' id='" + properties.id + "Signal'>\n                        <form onsubmit=\"return false;\" role=\"form\" data-id=\"" + properties.id + "\" data-type=\"" + properties.type + "\">\n                            <h3 class=\"signalTitle\">Signaler une erreur</h3>\n                            <div class=\"form-group\">\n                                <select name=\"errorType\" id=\"errorType\" class=\"form-control\" required=\"required\">\n                                    <option value=\"\" hidden disabled selected>Choisir l'erreur...</option>\n                                    <option value=\"errorPos\">Position incorrecte</option>\n                                    <option value=\"errorExist\">N'existe pas</option>\n                                    <option value=\"errorInfo\">Informations \u00E9rron\u00E9es</option>\n                                    <option value=\"errorWorks\">En cours de transformation</option>\n                                    <option value=\"other\">Autre</option>\n                                </select>\n                                <textarea name=\"comment\" id=\"comment\" maxlength=\"255\" class=\"form-control\" rows=\"1\" placeholder=\"Commentaire\" style=\"resize:none; overflow:hidden; display:none;\"></textarea>\n                            </div>\n                            <div class='valide-element checkbox'>\n                                <label><input type=\"checkbox\" id=\"followChanges\"> Je souhaite \u00EAtre notifi\u00E9 de la prochaine mise \u00E0 jour de cet \u00E9l\u00E9ment.</label>\n                            </div>\n                            <div align=\"center\">\n                                <input value=\"Signaler\" type=\"submit\" class=\"btn btn-primary\">\n                                <input value=\"Annuler\" type=\"button\" id=\"btnAnnulation\" class=\"btn btn-primary\">\n                            </div>\n                        </form>\n                    </div>";
            }
            else {
                content += "\n                    <div class=\"col-xs-12\" role=\"group\">\n                        <button class=\"btn btn-info\" id=\"login\">Connexion</button>\n                    </div>";
            }
            content += "\n                </div>\n            </div>";
            popupEvent.popup.setContent(content);
            $(popupEvent.popup.getElement()).on("click", function (e) {
                if ($('.valide-element').has(e.target).length === 0) {
                    _this._map.closePopup();
                }
            });
            $('button#login', popupEvent.popup.getElement()).click(function (e) { _this.connect(); });
            $('button#btnError', popupEvent.popup.getElement()).on('click', function (e) {
                var $valid = $("#btnValidate", popupEvent.popup.getElement());
                var $error = $("#btnError", popupEvent.popup.getElement());
                var $sendTo = $("#btnSendToOSM", popupEvent.popup.getElement());
                $("#" + feature.properties.id + "Signal", popupEvent.popup.getElement()).toggle(0, updatePopup);
                if (typeof (tags.fixme) == "undefined" || tags.fixme === "") {
                    $valid.prop('disabled', true);
                }
                $error.prop('disabled', true);
                $sendTo.prop('disabled', true);
            });
            $('#btnAnnulation', popupEvent.popup.getElement()).on('click', function (e) {
                var $valid = $("#btnValidate", popupEvent.popup.getElement());
                var $sendTo = $("#btnSendToOSM", popupEvent.popup.getElement());
                var $error = $("#btnError", popupEvent.popup.getElement());
                $("#" + feature.properties.id + "Signal", popupEvent.popup.getElement()).toggle(0, updatePopup);
                if (typeof (tags.fixme) == "undefined" || tags.fixme === "") {
                    $valid.prop('disabled', false);
                }
                $sendTo.prop('disabled', false);
                $error.prop('disabled', false);
            });
            var elementChanged = function () {
                var req = OauthOsm.apiRequest.GetElementId;
                req.params.id = properties.id;
                req.params.type = properties.type;
                _this._searchInfoElementsOSMAround().then(function (data) {
                    _this._updateElementsDisplayedAround(data);
                });
                popupEvent.target.closePopup();
            };
            var $errorForm = $("#" + properties.id + "Signal form", popupEvent.popup.getElement());
            $errorForm.on('submit', function (e) {
                var id = $errorForm.data('id');
                var type = $errorForm.data('type');
                var fixme = "";
                var note = "";
                switch (e.target.elements.errorType.value) {
                    case 'errorPos':
                        fixme += "location";
                        note += "Location is incorrect or moved";
                        break;
                    case 'errorExist':
                        fixme += "absent";
                        note += "The object dispappeared or never happened to be there";
                        break;
                    case 'errorInfo':
                        fixme += "tags";
                        note += "Object tags should be updated";
                        break;
                    case 'errorWorks':
                        fixme = "transient";
                        note = "Ongoing transformation";
                        break;
                    case 'other':
                        fixme = "resurvey";
                        note += e.target.elements.comment.value;
                        break;
                    default:
                        return;
                }
                var req = OauthOsm.apiRequest.GetElementId;
                var $currentTarget = $(e.currentTarget);
                req.params.id = id;
                req.params.type = type;
                _this._osm._sendRequest(req).then(function (beforeToAfter) {
                    var req = OauthOsm.apiRequest.CreateChangeset;
                    req.params.element = JSON.stringify({
                        changeset: {
                            tag: [
                                { '@attributes': { 'k': 'comment', 'v': 'Surveyed or verified in person' } },
                                { '@attributes': { 'k': 'host', 'v': window.location.origin + window.location.pathname } }
                            ]
                        }
                    });
                    _this._osm._sendRequest(req).then(function (changesetId) {
                        var req = OauthOsm.apiRequest.UpdateElement;
                        req.params.before = JSON.stringify($.extend({}, beforeToAfter));
                        var updated = false;
                        var element = beforeToAfter[type];
                        if (element) {
                            if (!(element.tag instanceof Array)) {
                                if (element.tag instanceof Object) {
                                    element.tag = [element.tag];
                                }
                                else {
                                    element.tag = [];
                                }
                            }
                            var indexS = void 0;
                            var indexF = void 0, indexN = -1;
                            var fixmeFromUs = false;
                            for (var index = 0; index < element.tag.length; index++) {
                                var tag = element.tag[index];
                                if (tag['@attributes'].k === 'fixme' && !_this.isFixmeFromUs(tag['@attributes'].v)) {
                                    updated = true;
                                    _this.notify.SignalisationExterieur();
                                    break;
                                }
                                if (tag['@attributes'].k === 'survey:date') {
                                    indexS = index;
                                }
                                if (tag['@attributes'].k === 'fixme' && _this.isFixmeFromUs(tag['@attributes'].v)) {
                                    indexF = index;
                                    fixmeFromUs = true;
                                }
                                if (fixmeFromUs && tag['@attributes'].k === "note") {
                                    indexN = index;
                                }
                            }
                            if (indexS !== undefined) {
                                element.tag.splice(indexS, 1);
                            }
                            if (fixmeFromUs && indexF !== undefined) {
                                if (indexF < indexN) {
                                    element.tag.splice(indexN, 1);
                                    element.tag.splice(indexF, 1);
                                }
                                else {
                                    element.tag.splice(indexF, 1);
                                    if (indexN !== -1) {
                                        element.tag.splice(indexN, 1);
                                    }
                                }
                            }
                            if (!updated) {
                                element.tag.push({ '@attributes': { k: "fixme", v: fixme } });
                                element.tag.push({ '@attributes': { k: "note", v: note } });
                            }
                        }
                        else {
                            beforeToAfter = {};
                        }
                        req.params.after = JSON.stringify(beforeToAfter);
                        req.params.changeset = changesetId;
                        if (!updated)
                            _this._osm._sendRequest(req).then(function (e) {
                                var req = OauthOsm.apiRequest.CloseChangeset;
                                req.params.id = changesetId;
                                _this._osm._sendRequest(req).then(function (e) {
                                    if ($('#followChanges').prop('checked', true)) {
                                        var req_1 = OauthOsm.apiRequest.GetElementId;
                                        req_1.params.id = $errorForm.data('id');
                                        _this._osm._sendRequest(req_1).then(function (data) { _this._ajoutPreferenceUnique(data.node["@attributes"].id, data.node["@attributes"].version); }, function () { });
                                    }
                                    elementChanged();
                                    _this.notify.InvalidatedOK();
                                    if (_this._userNbInvalidated !== -1)
                                        _this._userNbInvalidated++;
                                    else
                                        _this._userNbInvalidated = 1;
                                    _this._ajoutPreferenceUnique("nbInvalid", "" + _this._userNbInvalidated);
                                    $(_this).trigger('updateStats');
                                }, function (e) {
                                    _this.notify.ValidatedNOK();
                                });
                            }, function (e) {
                                _this.notify.ValidatedNOK();
                            });
                    }, function (e) {
                        _this.notify.ValidatedNOK();
                    });
                }, function (e) {
                    _this.notify.ValidatedNOK();
                });
            });
            $errorForm.find('#errorType').on('change', function (e) {
                if ($(e.target).val() === 'other') {
                    $errorForm.find('#comment').show().prop('required', true);
                }
                else {
                    $errorForm.find('#comment').hide().prop('required', false);
                }
                updatePopup();
            });
            $errorForm.find('#comment').on('change cut paste drop keydown', function (e) {
                e.target.style.height = 'auto';
                e.target.style.height = e.target.scrollHeight + 'px';
                updatePopup();
            });
            $('button#btnValidate', popupEvent.popup.getElement()).on('click', function (e) {
                var req = OauthOsm.apiRequest.GetElementId;
                var $currentTarget = $(e.currentTarget);
                var id = req.params.id = $currentTarget.data('id');
                var type = req.params.type = $currentTarget.data('type');
                var errorReq = function (e) { };
                _this._osm._sendRequest(req).then(function (beforeToAfter) {
                    var req = OauthOsm.apiRequest.CreateChangeset;
                    req.params.element = JSON.stringify({
                        changeset: {
                            tag: [
                                { '@attributes': { 'k': 'comment', 'v': 'Surveyed or verified in person' } },
                                { '@attributes': { 'k': 'host', 'v': window.location.origin + window.location.pathname } }
                            ]
                        }
                    });
                    _this._osm._sendRequest(req).then(function (changesetId) {
                        var req = OauthOsm.apiRequest.UpdateElement;
                        req.params.before = JSON.stringify($.extend({}, beforeToAfter));
                        var element = beforeToAfter[type];
                        if (element) {
                            if (!(element.tag instanceof Array)) {
                                if (element.tag instanceof Object) {
                                    element.tag = [element.tag];
                                }
                                else {
                                    element.tag = [];
                                }
                            }
                            var updated = false;
                            var sourceS = void 0, sourceD = void 0;
                            var indexF = void 0, indexN = -1;
                            var fixmeFromUs = false;
                            for (var index = 0; index < element.tag.length; index++) {
                                var tag = element.tag[index];
                                if (tag['@attributes'].k === 'source' && tag['@attributes'].v === 'survey') {
                                    sourceS = index;
                                }
                                if (tag['@attributes'].k === 'source' && tag['@attributes'].v === 'date') {
                                    sourceD = index;
                                }
                                if (tag['@attributes'].k === 'survey:date') {
                                    tag['@attributes'].v = (new Date).toISOString().replace(/T.*$/, '');
                                    updated = true;
                                }
                                if (tag['@attributes'].k === 'fixme' && _this.isFixmeFromUs(tag['@attributes'].v)) {
                                    indexF = index;
                                    fixmeFromUs = true;
                                }
                                if (fixmeFromUs && tag['@attributes'].k === "note") {
                                    indexN = index;
                                }
                            }
                            if (indexF !== undefined) {
                                if (indexF < indexN) {
                                    element.tag.splice(indexN, 1);
                                    element.tag.splice(indexF, 1);
                                }
                                else {
                                    element.tag.splice(indexF, 1);
                                    if (indexN !== -1) {
                                        element.tag.splice(indexN, 1);
                                    }
                                }
                            }
                            if (!updated) {
                                element.tag.push({ '@attributes': { k: "survey:date", v: (new Date).toISOString().replace(/T.*$/, '') } });
                            }
                            if (sourceD && sourceS) {
                                element.tag.splice(sourceD, 1);
                            }
                        }
                        else {
                            beforeToAfter = {};
                        }
                        req.params.after = JSON.stringify(beforeToAfter);
                        req.params.changeset = changesetId;
                        _this._osm._sendRequest(req).then(function (e) {
                            var req = OauthOsm.apiRequest.CloseChangeset;
                            req.params.id = changesetId;
                            _this._osm._sendRequest(req).then(function (e) {
                                elementChanged();
                                _this.notify.ValidatedOK();
                                if (_this._userNbValidated !== -1)
                                    _this._userNbValidated++;
                                else
                                    _this._userNbValidated = 1;
                                _this._ajoutPreferenceUnique("nbValid", "" + _this._userNbValidated);
                                $(_this).trigger('updateStats');
                            }, function (e) {
                                _this.notify.ValidatedNOK();
                            });
                        }, function (e) {
                            _this.notify.ValidatedNOK();
                        });
                    }, function (e) {
                        _this.notify.ValidatedNOK();
                    });
                }, function (e) {
                    _this.notify.ValidatedNOK();
                });
            });
            popupEvent.popup._source.setStyle($.extend({}, _this._style_selected, { fill: (feature.geometry.type !== "LineString") }));
            $(popupEvent.popup._closeButton).remove();
            $(window).on('resize orientationchange', updatePopup);
        });
        this._map.on('popupclose', function (popupEvent) {
            _this._geoJSONLayers.resetStyle(popupEvent.popup._source);
            console.log(popupEvent.popup._source);
            $(window).off('resize orientationchange');
        });
        var filter = function (featureData) {
            var display = true;
            var hasATagTranslatable = function (tags) {
                var tagTranslated = false;
                $.each(tags, function (index, value) {
                    if (tagTranslated) {
                        return;
                    }
                    if (presets) {
                        if (presets[index + "/" + value] || presets[index]) {
                            tagTranslated = true;
                        }
                    }
                });
                return tagTranslated;
            };
            if (_this._featureDisplayedAround[featureData.id] !== undefined ||
                featureData.properties.type !== 'node' ||
                !hasATagTranslatable(featureData.properties.tags)) {
                display = false;
            }
            return display;
        };
        var style = function (feature) {
            var style = {};
            if (feature.properties.tags) {
                var tags = feature.properties.tags;
                var dateVerified = void 0;
                var dateChange = new Date(feature.properties.meta.timestamp);
                if (tags["survey:date"]) {
                    dateVerified = new Date(tags["survey:date"]);
                }
                else if (tags["source"] === "survey" && tags["source:date"]) {
                    dateVerified = new Date(tags["source:date"]);
                }
                var dateOld = new Date();
                var year = dateOld.getUTCFullYear();
                dateOld.setUTCFullYear(year - 2);
                var isFar = _this._app_display_far ? '_far' : '';
                if (tags.fixme) {
                    style = $.extend({}, _this["_style_invalid" + isFar]);
                }
                else if (!dateVerified && dateChange < dateOld) {
                    style = $.extend({}, _this["_style_alert" + isFar]);
                }
                else if (!dateVerified && dateChange > dateOld) {
                    style = $.extend({}, _this["_style_warning" + isFar]);
                }
                else if (dateVerified && dateChange < dateOld) {
                    style = $.extend({}, _this["_style_warning" + isFar]);
                }
                else {
                    style = $.extend({}, _this["_style_ok" + isFar]);
                }
            }
            return style;
        };
        this._geoJSONLayers = L.geoJSON(null, {
            pointToLayer: function (feature, latlng) {
                return new L.CircleMarker(latlng);
            },
            filter: filter,
            style: style,
            onEachFeature: function (feature, layer) {
                if (feature.properties) {
                    var popup = L.responsivePopup({
                        hasTip: false
                    });
                    layer.bindPopup(popup, {
                        keepInView: false,
                    });
                }
            }
        });
        this._geoJSONLayers.addTo(this._map);
        this._map.on('zoomend', function (e) {
            if (_this._map.getZoom() <= 17 && _this._app_display_far !== true) {
                _this._map.removeLayer(_this._geoJSONLayers);
                _this._heatLayerFar.addTo(_this._map);
                _this._app_display_far = true;
                if (_this._lastUserLocation) {
                    _this._searchData(_this._lastUserLocation);
                }
            }
            else if (_this._map.getZoom() > 17 && _this._app_display_far === true) {
                _this._map.removeLayer(_this._heatLayerFar);
                _this._geoJSONLayers.addTo(_this._map);
                _this._app_display_far = false;
                if (_this._lastUserLocation) {
                    _this._searchData(_this._lastUserLocation);
                }
            }
        });
        this._checkChanges();
    };
    Geocropping.prototype.MobileSetGPSPosition = function (active) {
        $('#menu #clickPos').prop('checked', (active ? true : false));
        this.setGPSPosition(active);
        active ? geocropping.startLocateUser() : geocropping.stopLocateUser();
    };
    Geocropping.prototype.setGPSPosition = function (active) {
        if (!active) {
            this._app_click_position_actived = true;
            this.notify.ClicPositionON();
            this._map.on('click', this._locationFoundClickFunction()).setMaxBounds(null);
        }
        else {
            this._app_click_position_actived = false;
            this.notify.ClicPositionOFF();
            this._map.off('click', this._locationFoundClickFunction());
        }
    };
    Geocropping.prototype._locationFoundClickFunction = function () {
        var _this = this;
        if (!(this.__locationFoundClick instanceof Function)) {
            this.__locationFoundClick = function (data) {
                _this._locationFound(data);
            };
        }
        return this.__locationFoundClick;
    };
    Geocropping.prototype.startLocateUser = function () {
        var _this = this;
        if (!this.isMapCreated()) {
            return false;
        }
        var options = {
            setView: false,
            maxZoom: this._map_maxZoom,
            enableHighAccuracy: true,
            forceWatch: true
        };
        -this._map.locate(options).on('locationfound', function (data) {
            _this._locationFound(data, options);
        }).on('locationerror', function (e) {
            _this._locationError(e, options);
        });
        return true;
    };
    Geocropping.prototype.stopLocateUser = function () {
        if (this.isMapCreated()) {
            this._map.stopLocate();
            this._map.off('locationfound').off('locationerror');
        }
    };
    Geocropping.prototype._locationFound = function (data, options) {
        var _this = this;
        if (this._lastUserLocation === null) {
            this._map.setMinZoom(this._map_minZoom).setMaxZoom(this._map_maxZoom).setView(data.latlng, this._map_maxZoom);
        }
        if (!(data.latlng instanceof L.LatLng)) {
            return;
        }
        if (this._lastUserLocation === undefined || this._lastUserLocation === null || this._lastUserLocation.distanceTo(data.latlng) > this._app_distance_travel_to_search) {
            this._lastUserLocation = data.latlng;
            this._userMarker.setLatLng(data.latlng).addTo(this._map);
            this._searchData(data.latlng);
        }
        if (options && options.forceWatch === true) {
            setTimeout(function () {
                _this._map.locate(options);
            }, this._app_time_locate_interval);
        }
        else {
            this.stopLocateUser();
        }
    };
    Geocropping.prototype._locationError = function (e, options) {
        var _this = this;
        console.log(e.message);
        this.notify.LocalisationNOK();
        if (options && options.forceWatch === true) {
            setTimeout(function () {
                _this._map.locate(options);
            }, this._app_time_locate_interval);
        }
        else {
            this.stopLocateUser();
        }
    };
    Geocropping.prototype._searchDataAround = function (pos) {
        var _this = this;
        if (!(pos instanceof L.LatLng))
            return;
        var lat = pos.lat, lon = pos.lng;
        var distance = this._app_distance_hide < this._app_distance_search ? this._app_distance_hide : this._app_distance_search;
        var query = "[out:json];node[~\".\"~\".\"](around:" + distance + "," + lat + "," + lon + ")->.nodes;/*way(around:" + distance + "," + lat + "," + lon + ")->.ways;*/.nodes out meta;/*.ways out geom meta;*/";
        if (!this._app_click_position_actived) {
            this._map.setMaxBounds(L.latLngBounds(this._lastUserLocation, this._lastUserLocation));
        }
        $.ajax({
            context: this,
            url: this._app_server_interpreter_list[this._app_server_interpreter_index],
            data: {
                data: query
            },
            success: function (data, textStatus, jqXHR) {
                if (data.elements.length > 0) {
                    var geojson = osmtogeojson(data);
                    _this._geoJSONLayers.addData(geojson);
                    _this._featureDisplayedAround = {};
                    _this._geoJSONLayers.getLayers().forEach(function (layer, index, array) {
                        var remove = false;
                        if (layer instanceof L.CircleMarker) {
                            var dist = layer.getLatLng().distanceTo(_this._lastUserLocation);
                            if (dist > _this._app_distance_hide) {
                                remove = true;
                            }
                        }
                        else if (layer instanceof L.Polyline) {
                            var nodes = layer.getLatLngs();
                            if (nodes[0] instanceof Array) {
                                nodes = nodes[0];
                            }
                            remove = true;
                            for (var i = 0; i < nodes.length; i++) {
                                var dist = L.latLng(nodes[i].lat, nodes[i].lng).distanceTo(pos);
                                if (dist < _this._app_distance_hide) {
                                    remove = false;
                                    break;
                                }
                            }
                        }
                        if (remove) {
                            _this._geoJSONLayers.removeLayer(layer);
                        }
                        else {
                            _this._featureDisplayedAround[layer.feature.id] = { layer: layer };
                        }
                    });
                    _this._searchInfoElementsOSMAround().then(function (data) {
                        _this._updateElementsDisplayedAround(data);
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
    };
    Geocropping.prototype._searchInfoElementsOSMAround = function () {
        var ids = [];
        for (var key in this._featureDisplayedAround) {
            if (this._featureDisplayedAround.hasOwnProperty(key)) {
                var regexp = key.match(/^node\/(\d+)/);
                if (regexp && regexp[1]) {
                    ids.push(regexp[1]);
                }
            }
        }
        var req = OauthOsm.apiRequest.GetElementsId;
        req.params.type = "nodes";
        req.params.ids = ids;
        return this._osm._sendRequest(req);
    };
    Geocropping.prototype._updateElementsDisplayedAround = function (data) {
        var type;
        var obj = [];
        for (var key in data) {
            if (data.hasOwnProperty(key) && key !== "@attributes") {
                type = key;
                obj = data[type];
            }
        }
        if (!(obj instanceof Array) && obj instanceof Object) {
            obj = [obj];
        }
        for (var index = 0; index < obj.length; index++) {
            var element = obj[index];
            var feature = this._featureDisplayedAround[type + "/" + element["@attributes"].id];
            if (feature && element["@attributes"]) {
                if (element["@attributes"].visible) {
                    delete element["@attributes"].visible;
                }
                var osm = {
                    type: type
                };
                for (var key in element["@attributes"]) {
                    if (element["@attributes"].hasOwnProperty(key)) {
                        var attribute = element["@attributes"][key];
                        if (Number.isInteger(attribute)) {
                            attribute = Number(attribute);
                        }
                        osm[key] = attribute;
                    }
                }
                var tags = element['tag'];
                if (tags) {
                    osm['tags'] = {};
                    if (!(tags instanceof Array) && tags instanceof Object) {
                        tags = [tags];
                    }
                    for (var i = 0; i < tags.length; i++) {
                        var tag = tags[i];
                        osm['tags'][tag["@attributes"].k] = tag["@attributes"].v;
                    }
                }
                feature.layer.feature = osmtogeojson({ elements: [osm] }).features[0];
                this._geoJSONLayers.resetStyle(feature.layer);
            }
        }
    };
    Geocropping.prototype._checkChanges = function () {
        var _this = this;
        var req = OauthOsm.apiRequest.GetUserPreferences;
        this._osm._sendRequest(req).done(function (data) {
            var resPref = data.preferences.preference;
            var ids = [];
            var pref = {};
            if (!(resPref instanceof Array)) {
                if (resPref instanceof Object) {
                    resPref = [resPref];
                }
                else {
                    resPref = [];
                }
            }
            for (var i in resPref) {
                if (resPref[i]['@attributes'].k !== "nbValid" && resPref[i]['@attributes'].k !== "nbInvalid") {
                    ids.push(resPref[i]['@attributes'].k);
                    pref[resPref[i]['@attributes'].k] = resPref[i]['@attributes'].v;
                }
            }
            var req = OauthOsm.apiRequest.GetElementsId;
            req.params.type = "nodes";
            req.params.ids = ids;
            var resNodes;
            _this._osm._sendRequest(req).then(function (data) {
                resNodes = data.node;
                var differents = [];
                if (!(resNodes instanceof Array)) {
                    if (resNodes instanceof Object) {
                        resNodes = [resNodes];
                    }
                    else {
                        resNodes = [];
                    }
                }
                for (var i in resNodes) {
                    if (pref[resNodes[i]['@attributes'].id] < resNodes[i]['@attributes'].version) {
                        differents.push(resNodes[i]['@attributes'].id);
                    }
                }
                _this.notify.ChangementsTrouve(differents);
                _this._removeChanges(resPref, differents);
            });
        });
    };
    Geocropping.prototype._removeChanges = function (resPref, changes) {
        var newPref = {
            preferences: {
                preference: []
            }
        };
        for (var i in resPref) {
            if (changes.indexOf(resPref[i]['@attributes'].k) === -1) {
                newPref.preferences.preference.push({ '@attributes': { k: resPref[i]['@attributes'].k, v: resPref[i]['@attributes'].v } });
            }
        }
        var req = OauthOsm.apiRequest.UpdateUserPreferences;
        req.params.content = JSON.stringify(newPref);
        this._osm._sendRequest(req);
    };
    Geocropping.prototype._ajoutPreferenceUnique = function (id, version) {
        var _this = this;
        var req = OauthOsm.apiRequest.UpdateSingleUserPreference;
        req.params.key = id;
        req.params.value = version;
        geocropping._osm._sendRequest(req).then(function () { }, function () { _this.notify.AjoutSinglePrefNOK(); });
    };
    Geocropping.prototype._getStats = function () {
        var _this = this;
        var req = OauthOsm.apiRequest.GetUserPreferences;
        this._osm._sendRequest(req).done(function (data) {
            var resPref = data.preferences.preference;
            var valid, invalid;
            if (!(resPref instanceof Array)) {
                if (resPref instanceof Object) {
                    resPref = [resPref];
                }
                else {
                    resPref = [];
                }
            }
            for (var i in resPref) {
                switch (resPref[i]['@attributes'].k) {
                    case "nbValid":
                        valid = resPref[i]['@attributes'].v;
                        break;
                    case "nbInvalid":
                        invalid = resPref[i]['@attributes'].v;
                        break;
                    default: break;
                }
            }
            if (valid === undefined)
                _this._ajoutPreferenceUnique("nbValid", "-1");
            if (invalid === undefined)
                _this._ajoutPreferenceUnique("nbInvalid", "-1");
            if (valid === "-1" || valid === undefined)
                valid = 0;
            if (invalid === "-1" || invalid === undefined)
                invalid = 0;
            _this._userNbValidated = valid;
            _this._userNbInvalidated = invalid;
            $(_this).trigger('updateStats');
        });
    };
    Geocropping.prototype.isFixmeFromUs = function (contenuDuFixme) {
        return contenuDuFixme === 'location' || contenuDuFixme === 'absent' || contenuDuFixme === 'tags' || contenuDuFixme === 'transient' || contenuDuFixme === 'resurvey';
    };
    Geocropping.prototype._searchDataFar = function (pos) {
        var _this = this;
        var lat, lon;
        lat = pos.lat;
        lon = pos.lng;
        var query = "[out:json];node[~\".\"~\".\"](around:" + this._app_distance_search_far + "," + lat + "," + lon + ");out meta;";
        $.ajax({
            context: this,
            url: this._app_server_interpreter_list[this._app_server_interpreter_index],
            data: {
                data: query
            },
            success: function (data, textStatus, jqXHR) {
                if (_this._heatLayerFar._map) {
                    var points = _this._getPositionColorFar(data);
                    _this._heatLayerFar.setLatLngs(points);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    };
    Geocropping.prototype._getPositionColorFar = function (data) {
        var _this = this;
        var points = [];
        if (data.elements.length > 0) {
            this._featureDisplayedFar = {};
            var hasATagTranslatable = function (tags) {
                var tagTranslated = false;
                $.each(tags, function (index, value) {
                    if (tagTranslated) {
                        return;
                    }
                    if (presets) {
                        if (presets[index + "/" + value] || presets[index]) {
                            tagTranslated = true;
                        }
                    }
                });
                return tagTranslated;
            };
            var intensity = function (feature) {
                var intensity = 0;
                if (feature.tags) {
                    var tags = feature.tags;
                    var dateVerified = void 0;
                    var dateChange = new Date(feature.timestamp);
                    if (tags["survey:date"]) {
                        dateVerified = new Date(tags["survey:date"]);
                    }
                    else if (tags["source"] === "survey" && tags["source:date"]) {
                        dateVerified = new Date(tags["source:date"]);
                    }
                    var dateOld = new Date();
                    var year = dateOld.getUTCFullYear();
                    dateOld.setUTCFullYear(year - 2);
                    if (tags.fixme) {
                        intensity = _this._style_invalid_far.display ? _this._style_invalid_far.value : 0;
                    }
                    else if (!dateVerified && dateChange < dateOld) {
                        intensity = _this._style_alert_far.display ? _this._style_alert_far.value : 0;
                    }
                    else if (!dateVerified && dateChange > dateOld) {
                        intensity = _this._style_warning_far.display ? _this._style_warning_far.value : 0;
                    }
                    else if (dateVerified && dateChange < dateOld) {
                        intensity = _this._style_warning_far.display ? _this._style_warning_far.value : 0;
                    }
                    else {
                        intensity = _this._style_ok_far.display ? _this._style_ok_far.value : 0;
                    }
                }
                return intensity;
            };
            for (var index = 0; index < data.elements.length; index++) {
                var element = data.elements[index];
                if (element.type === 'node' && hasATagTranslatable(element.tags)) {
                    var alt = intensity(element);
                    var latLng = L.latLng([element.lat, element.lon, alt]);
                    if (alt >= 0 && alt <= 1) {
                        latLng.geocropping = { data: element, intensity: intensity };
                        this._featureDisplayedFar[element.type + "/" + element.id] = { latlng: latLng };
                    }
                    if (alt > 0 && alt <= 1) {
                        points.push(latLng);
                    }
                }
            }
        }
        return points;
    };
    Geocropping.prototype._searchData = function (pos) {
        if (this._app_display_far) {
            this._searchDataFar(pos);
        }
        else {
            this._searchDataAround(pos);
        }
    };
    Geocropping.prototype.setDisplayModeFar = function (type) {
        this._resetStyleFar();
        var options = $.extend({}, this._app_heatLayer_options);
        if (type === 'validated') {
            this._style_ok_far.display = true;
            options.gradient = this._getGradientFar();
        }
        else if (type === 'invalidated') {
            this._style_invalid_far.display = true;
            options.gradient = this._getGradientFar();
        }
        else if (type === 'warned') {
            this._style_warning_far.display = true;
            options.gradient = this._getGradientFar();
        }
        else if (type === 'alert') {
            this._style_alert_far.display = true;
            options.gradient = this._getGradientFar();
        }
        else {
            return false;
        }
        if (this._heatLayerFar) {
            this._heatLayerFar.setOptions(options);
            this._updateElementsDisplayedFar();
        }
        return true;
    };
    Geocropping.prototype._getGradientFar = function () {
        var styles = [
            this._style_alert_far,
            this._style_invalid_far,
            this._style_ok_far,
            this._style_warning_far
        ];
        var gradient = {};
        for (var index = 0; index < styles.length; index++) {
            var style = styles[index];
            if (style.display) {
                gradient[style.value] = style.color;
            }
        }
        return gradient;
    };
    Geocropping.prototype._resetStyleFar = function () {
        this._style_alert_far.display = false;
        this._style_invalid_far.display = false;
        this._style_ok_far.display = false;
        this._style_warning_far.display = false;
    };
    Geocropping.prototype._searchInfoElementsOSMFar = function () {
        var promise = [];
        var ids = [];
        for (var key in this._featureDisplayedFar) {
            if (this._featureDisplayedFar.hasOwnProperty(key)) {
                var regexp = key.match(/^node\/(\d+)/);
                if (regexp && regexp[1]) {
                    ids.push(regexp[1]);
                }
            }
        }
        var arrays = [], size = 10;
        while (ids.length > 0) {
            arrays.push(ids.splice(0, size));
            var req = OauthOsm.apiRequest.GetElementsId;
            req.params.type = "nodes";
            req.params.ids = ids;
            promise.push(this._osm._sendRequest(req));
        }
        return promise;
    };
    Geocropping.prototype._updateElementsDisplayedFar = function (data) {
        if (data instanceof Object) {
            var type = void 0;
            var obj = [];
            for (var key_1 in data) {
                if (data.hasOwnProperty(key_1) && key_1 !== "@attributes") {
                    type = key_1;
                    obj = data[type];
                }
            }
            if (!(obj instanceof Array) && obj instanceof Object) {
                obj = [obj];
            }
            for (var index = 0; index < obj.length; index++) {
                var element = obj[index];
                var latLng = this._featureDisplayedFar[type + "/" + element["@attributes"].id].latlng;
                if (latLng && element["@attributes"]) {
                    if (element["@attributes"].visible) {
                        delete element["@attributes"].visible;
                    }
                    var osm = {
                        type: type
                    };
                    for (var key_2 in element["@attributes"]) {
                        if (element["@attributes"].hasOwnProperty(key_2)) {
                            var attribute = element["@attributes"][key_2];
                            if (Number.isInteger(attribute)) {
                                attribute = Number(attribute);
                            }
                            osm[key_2] = attribute;
                        }
                    }
                    var tags = element['tag'];
                    if (tags) {
                        osm['tags'] = {};
                        if (!(tags instanceof Array) && tags instanceof Object) {
                            tags = [tags];
                        }
                        for (var i = 0; i < tags.length; i++) {
                            var tag = tags[i];
                            osm['tags'][tag["@attributes"].k] = tag["@attributes"].v;
                        }
                    }
                    latLng.geocropping.data = osmtogeojson({ elements: [osm] }).features[0].properties;
                }
            }
        }
        var toDisplay = [];
        for (var key in this._featureDisplayedFar) {
            if (this._featureDisplayedFar.hasOwnProperty(key)) {
                var latlng = this._featureDisplayedFar[key].latlng;
                var alt = latlng.geocropping.intensity(latlng.geocropping.data);
                latlng.alt = alt;
                if (alt > 0 && alt <= 1) {
                    toDisplay.push(latlng);
                }
            }
        }
        this._heatLayerFar.setLatLngs(toDisplay);
    };
    Geocropping.prototype._setOnMobileApp = function () {
        this._onMobileApp = true;
    };
    return Geocropping;
}());
//# sourceMappingURL=Geocropping.js.map