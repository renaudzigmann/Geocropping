"use strict";
function toggleRightMenu() {
    var actualRight = $('body')[0].offsetWidth - $('.side-nav.fit-right')[0].offsetLeft;
    if (actualRight !== 0)
        closeRightMenu();
    else
        openRightMenu();
}
function toggleLeftMenu() {
    if ($('.side-nav.fit-left').position().left + $('.side-nav.fit-left').width() !== 0)
        closeLeftMenu();
    else
        openLeftMenu();
}
function closeLeftMenu() {
    openLeftMenu(0);
}
function openLeftMenu(width) {
    (width === undefined) && (width = $('.side-nav.fit-left').width());
    if (width === 0)
        $('.side-nav.fit-left').css('left', -$('.side-nav.fit-left').width());
    else {
        $('.side-nav.fit-left').width(width);
        $('.side-nav.fit-left').css('left', 0);
    }
    $('#mapArea').css('left', width);
}
function closeRightMenu() {
    openRightMenu(0);
}
function openRightMenu(width) {
    (width === undefined) && (width = $('.side-nav.fit-right').width());
    if (width === 0)
        $('.side-nav.fit-right').css('right', -$('.side-nav.fit-right').width());
    else {
        $('.side-nav.fit-right').width(width);
        $('.side-nav.fit-right').css('right', 0);
    }
    $('#mapArea').css('right', width);
}
var geocropping = new Geocropping();
$(document).ready(function () {
    var $geocropping = $(geocropping);
    $('#userAction #login').click(function () {
        geocropping.connect();
    });
    $('#userAction #logout').click(function () {
        geocropping.userDisconnect();
    });
    $('#menu #modeDisplayFar').on('change', 'input[name=modeDisplayFar]', function (e) {
        var $this = $(this);
        if ($this.is(':checked')) {
            geocropping.setDisplayModeFar($this.val());
        }
    });
    $('#menu #clickPos').change(function (e) {
        var val = $(this).prop('checked');
        geocropping.setGPSPosition(val);
        val ? geocropping.startLocateUser() : geocropping.stopLocateUser();
    }).prop("checked", true);
    var devMode = localLoad('devMode');
    if (devMode) {
        geocropping._osm._devApi = devMode == "true";
    }
    $('#menu #devMode').change(function (e) {
        var val = $(this).prop('checked');
        geocropping.userDisconnect();
        geocropping._osm._devApi = val;
        localSave('devMode', '' + val);
    }).prop("checked", geocropping._osm._devApi);
    $('#menu #urlInterpreter').change(function (e) {
        geocropping._app_server_interpreter_index = this.value;
        geocropping.notify.ServerChanged();
    });
    var userConnected = function () {
        $('#userAction #login').hide();
        $('#userAction #logout').show();
        $('.map-menu-item:has(#userImage)').show();
        $('#userInfo').show();
    };
    var updateStats = function () {
        $('#userNbValid').text("Validations : " + geocropping._userNbValidated);
        $('#userNbInvalid').text("Signalements : " + geocropping._userNbInvalidated);
    };
    var userDisconnected = function () {
        $('#userAction #logout').hide();
        $('#userAction #login').show();
        $('.map-menu-item:has(#userImage)').hide();
        $('#userInfo').hide();
        $('#userImage').attr('src', '');
        $('#userInfo #userName').text('');
    };
    $geocropping.on('userDisconnected', userDisconnected);
    $geocropping.on('userConnected', userConnected);
    $geocropping.on('updateStats', updateStats);
    var updateUserDetails = function (e, data) {
        $('#userImage').show();
        $('#userImage').attr('src', data.img);
        $('#userInfo #userName').text(data.display_name);
    };
    $geocropping.on('userDetailsUpdated', updateUserDetails);
    if (geocropping.isConnected) {
        userConnected();
    }
    else {
        userDisconnected();
    }
    ;
    geocropping.createMap('map');
    document.getElementById('mapArea').style.width = "auto";
    geocropping.startLocateUser();
});
//# sourceMappingURL=index.js.map