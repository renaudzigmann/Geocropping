"use strict";
var OauthOsm = (function () {
    function OauthOsm() {
        this._devApi = false;
        this._connected = false;
        this.getUserDetails();
        this._baseUrl = window.location.href + 'auth/';
    }
    Object.defineProperty(OauthOsm.prototype, "isConnected", {
        get: function () { return this._connected; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(OauthOsm, "apiRequest", {
        get: function () {
            return {
                GetServerCapabilities: {
                    method: 'GET',
                    url: '/',
                    request: 'GetServerCapabilities',
                    params: {}
                },
                GetPermissions: {
                    method: 'GET',
                    url: '/',
                    request: 'GetPermissions',
                    params: {},
                    need_connection: true
                },
                GetDataBoundsBox: {
                    method: 'GET',
                    url: '/',
                    request: 'GetDataBoundsBox',
                    params: {
                        left: 0,
                        bottom: 0,
                        right: 0.5,
                        top: 0.5
                    }
                },
                GetUserDetails: {
                    method: 'GET',
                    url: '/',
                    request: 'GetUserDetails',
                    params: {},
                    need_connection: true
                },
                GetUserPreferences: {
                    method: 'GET',
                    url: '/',
                    request: 'GetUserPreferences',
                    params: {},
                    need_connection: true
                },
                UpdateUserPreferences: {
                    method: 'POST',
                    url: '/',
                    request: 'UpdateUserPreferences',
                    params: {
                        content: JSON.stringify({
                            preferences: {
                                preference: []
                            }
                        }),
                    },
                    need_connection: true
                },
                UpdateSingleUserPreference: {
                    method: 'POST',
                    url: '/',
                    request: 'UpdateSingleUserPreference',
                    params: {
                        key: "",
                        value: ""
                    },
                    need_connection: true
                },
                GetUserId: {
                    method: 'GET',
                    url: '/',
                    request: 'GetUserId',
                    params: {
                        id: 1
                    }
                },
                GetNoteId: {
                    method: 'GET',
                    url: '/',
                    request: 'GetNoteId',
                    params: {
                        id: 1,
                        format: ''
                    }
                },
                GetNotesBoundsBox: {
                    method: 'GET',
                    url: '/',
                    request: 'GetNotesBoundsBox',
                    params: {
                        left: 0,
                        bottom: 0,
                        right: 10,
                        top: 10,
                        format: ''
                    }
                },
                SearchNotes: {
                    method: 'GET',
                    url: '/',
                    request: 'SearchNotes',
                    params: {
                        query: 'hello',
                        limit: 100,
                        closed: 7,
                        format: ''
                    }
                },
                GetChangesetId: {
                    method: 'GET',
                    url: '/',
                    request: 'GetChangesetId',
                    params: {
                        discussion: true,
                        id: 1
                    }
                },
                GetChangeOfChangesetId: {
                    method: 'GET',
                    url: '/',
                    request: 'GetChangeOfChangesetId',
                    params: {
                        id: 1
                    }
                },
                GetElementId: {
                    method: 'GET',
                    url: '/',
                    request: 'GetElementId',
                    params: {
                        type: 'node',
                        id: 1
                    }
                },
                GetElementIdHistory: {
                    method: 'GET',
                    url: '/',
                    request: 'GetElementIdHistory',
                    params: {
                        type: 'node',
                        id: 1
                    }
                },
                GetElementIdVersion: {
                    method: 'GET',
                    url: '/',
                    request: 'GetElementIdVersion',
                    params: {
                        type: 'node',
                        id: 1
                    }
                },
                GetElementsId: {
                    method: 'GET',
                    url: '/',
                    request: 'GetElementsId',
                    params: {
                        ids: [100, 102],
                        type: 'nodes'
                    }
                },
                GetElementIdRelations: {
                    method: 'GET',
                    url: '/',
                    request: 'GetElementIdRelations',
                    params: {
                        type: 'node',
                        id: 100
                    }
                },
                GetNodeIdWays: {
                    method: 'GET',
                    url: '/',
                    request: 'GetNodeIdWays',
                    params: {
                        id: 1
                    }
                },
                GetElementIdFull: {
                    method: 'GET',
                    url: '/',
                    request: 'GetElementIdFull',
                    params: {
                        type: 'way',
                        id: 1
                    }
                },
                CreateNote: {
                    method: 'POST',
                    url: '/',
                    request: 'CreateNote',
                    params: {
                        lat: 0,
                        lon: 0,
                        text: ''
                    }
                },
                CreateNoteComment: {
                    method: 'POST',
                    url: '/',
                    request: 'CreateNoteComment',
                    params: {
                        id: 0,
                        text: ''
                    }
                },
                CloseNote: {
                    method: 'POST',
                    url: '/',
                    request: 'CloseNote',
                    params: {
                        id: 0,
                        text: ''
                    }
                },
                ReopenNote: {
                    method: 'POST',
                    url: '/',
                    request: 'ReopenNote',
                    params: {
                        id: 0,
                        text: ''
                    }
                },
                CreateChangeset: {
                    method: 'POST',
                    url: '/',
                    request: 'CreateChangeset',
                    params: {
                        element: JSON.stringify({
                            changeset: {
                                tag: []
                            }
                        })
                    }
                },
                UpdateChangeset: {
                    method: 'POST',
                    url: '/',
                    request: 'UpdateChangeset',
                    params: {
                        element: JSON.stringify({
                            changeset: {
                                tag: []
                            }
                        }),
                        id: 0
                    }
                },
                CloseChangeset: {
                    method: 'POST',
                    url: '/',
                    request: 'CloseChangeset',
                    params: {
                        id: 0
                    }
                },
                CreateElement: {
                    method: 'POST',
                    url: '/',
                    request: 'CreateElement',
                    params: {
                        element: '',
                        type: ''
                    }
                },
                UpdateElement: {
                    method: 'POST',
                    url: '/',
                    request: 'UpdateElement',
                    params: {
                        before: '',
                        after: '',
                        changeset: 0
                    }
                },
            };
        },
        enumerable: true,
        configurable: true
    });
    OauthOsm.prototype._sendRequest = function (apiRequest) {
        var _this = this;
        var deferred = $.Deferred();
        if (typeof apiRequest !== 'object' || apiRequest.url === undefined || apiRequest.method === undefined) {
            deferred.reject({
                message: 'Invalid argument',
                request: apiRequest
            });
            deferred.rejectWith(this);
        }
        var data = {
            action: 'request_api',
            command: apiRequest.request,
            options: apiRequest.params
        };
        var url = this._baseUrl;
        if (this._devApi) {
            url += '?devMode=true';
        }
        $.ajax(url, {
            type: apiRequest.method,
            data: data
        }).done(function (data) {
            if (_this._connected === false && apiRequest.need_connection === true) {
                _this._connected = true;
                _this.connect();
            }
            deferred.resolveWith(_this, [data]);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 401) {
                _this.disconnect();
            }
            else if (jqXHR.status === 403) {
            }
            deferred.rejectWith(_this, [jqXHR]);
        });
        return deferred.promise();
    };
    OauthOsm.prototype.getServerDetails = function () {
        return this._sendRequest(OauthOsm.apiRequest.GetServerCapabilities);
    };
    OauthOsm.prototype.getUserDetails = function () {
        return this._sendRequest(OauthOsm.apiRequest.GetUserDetails);
    };
    OauthOsm.prototype.connect = function () {
        var deferred = $.Deferred();
        if (this.isConnected) {
            $(this).triggerHandler("connected");
            deferred.resolveWith(this);
        }
        else {
            window.open(this._baseUrl + "index.php" + (this._devApi ? '?devMode=true' : '?devMode=false'), '_self');
        }
        return deferred.promise();
    };
    OauthOsm.prototype.disconnect = function () {
        var _this = this;
        var deferred = $.Deferred();
        $.ajax({
            method: 'GET',
            url: './auth/',
            data: {
                action: 'logout'
            },
            success: function (data) {
                deferred.resolveWith(_this);
                _this._connected = false;
                $(_this).trigger("disconnected");
            },
            error: function (err) {
                deferred.rejectWith(_this);
            }
        });
        return deferred.promise();
    };
    return OauthOsm;
}());
function getCookies(name) {
    "use strict";
    if (name === void 0) { name = []; }
    if (typeof name === "string") {
        name = [name];
    }
    else if (!(name instanceof Array)) {
        throw new TypeError("Argument `name` is not a string or array");
    }
    var cookiesString = document.cookie;
    if (cookiesString.length === 0) {
        return [];
    }
    var cookiesList = cookiesString.split("; ");
    var cookies = [];
    for (var i = 0; i < cookiesList.length; i++) {
        var c = cookiesList[i].split("=");
        if (name.length === 0 || name.indexOf(c[0]) > -1) {
            cookies.push({
                key: c[0],
                value: c[1]
            });
        }
    }
    return cookies;
}
function deleteCookie(name, path) {
    "use strict";
    if (path === void 0) { path = ""; }
    if (typeof (name) !== "string" && typeof (name) !== "undefined") {
        throw "Invalide agrument";
    }
    var cookies = getCookies(name);
    var date = new Date(0);
    for (var i = 0; i < cookies.length; i++) {
        document.cookie = (cookies[i].key === "") ?
            cookies[i].key + "; expires=" + date.toUTCString() + ";" + ((path.length !== 0) ? " path=" + path + ";" : '') :
            cookies[i].key + "=; expires=" + date.toUTCString() + ";" + ((path.length !== 0) ? " path=" + path + ";" : '');
    }
}
function localSave(name, value, options) {
    "use strict";
    if (options === void 0) { options = {}; }
    if (typeof name !== "string") {
        throw new TypeError("Argument `name` is not a string");
    }
    if (typeof value !== "string") {
        throw new TypeError("Argument `value` is not a string");
    }
    var date = new Date();
    date.setMonth(date.getMonth() + 1);
    var newOptions = $.extend({}, {
        useLocalStorageOrCookie: true,
        useLocalStorage: false,
        useCookie: false,
        cookie: {
            expires: date.toUTCString(),
            path: "/"
        }
    }, options);
    options.systemUsed = "";
    if ((newOptions.useLocalStorageOrCookie === true || newOptions.useLocalStorage === true) && typeof Storage !== "undefined") {
        localStorage.setItem(name, value);
        options.systemUsed = "localStorage";
    }
    if ((newOptions.useLocalStorageOrCookie === true && typeof Storage === "undefined") || newOptions.useCookie === true) {
        document.cookie = name + "=" + value + "; expires=" + newOptions.cookie.expires + "; path=" + newOptions.cookie.path;
        options.systemUsed = "cookie";
    }
}
function localLoad(name, options) {
    "use strict";
    if (options === void 0) { options = {}; }
    if (typeof name !== "string") {
        throw new TypeError("Argument `name` is not a string");
    }
    var newOptions = $.extend({}, {
        useLocalStorageOrCookie: true,
        useLocalStorage: false,
        useCookie: false,
        cookie: {}
    }, options);
    options.systemUsed = "";
    var value;
    if ((newOptions.useLocalStorageOrCookie === true || newOptions.useLocalStorage === true) && typeof Storage !== "undefined") {
        value = localStorage.getItem(name);
        value = (value !== null) ? value : undefined;
        (value && (options.systemUsed = "localStorage"));
    }
    if ((newOptions.useLocalStorageOrCookie === true && value === undefined) || newOptions.useCookie === true) {
        var cookies = getCookies(name);
        (cookies.length && (value = cookies[0].value));
        (value && (options.systemUsed = "cookie"));
    }
    return value;
}
function localDelete(name, options) {
    "use strict";
    if (options === void 0) { options = {}; }
    if (typeof name !== "string") {
        throw new TypeError("Argument `name` is not a string");
    }
    options = $.extend({}, {
        useLocalStorage: true,
        useCookie: true,
        cookie: {
            path: "/"
        }
    }, options);
    if (options.useLocalStorage === true && typeof Storage !== "undefined") {
        localStorage.removeItem(name);
    }
    if (options.useCookie === true) {
        deleteCookie(name, options.cookie.path);
    }
}
//# sourceMappingURL=OauthOsm.js.map