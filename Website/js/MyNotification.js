"use strict";
var MyNotification = (function () {
    function MyNotification(map) {
        this.options = { timeout: 4000, position: "bottomleft" };
        this.box = L.control.messagebox(this.options).addTo(map);
        this.notif = {
            ConnectionOK: "Connexion au compte OSM réussie",
            ConnectionNOK: "Echec de la connexion au compte OSM",
            DisconnectionOK: "Déconnexion d'OSM réussie",
            DisconnectionNOK: "Echec de la deconnexion du compte OSM",
            ValidatedOK: "Element validé",
            ValidatedNOK: "Echec de la validation de l'élement",
            InvalidatedOK: "Element signalé",
            InvalidatedNOK: "Echec de la signalisation de l'élement",
            LocalisationNOK: "Echec de la localisation",
            AutoServerChanged: "Problème de connexion avec le server, changement automatique",
            ServerChanged: "Changement de server éffectué",
            ClicPositionON: "Arrêt de la geolocalisation, position au clic.",
            ClicPositionOFF: "Reprise de la geolocalisation",
            AjoutSinglePrefNOK: "Echec de l'ajout de la préférence",
            AjoutSinglePrefOK: "Ajout de la préférence éffectué",
            ChangementsTrouve: "Les éléments suivants ont été modifiés : ",
            SignalisationExterieur: "L'objet a déjà été signalisé par quelqu'un d'exterieur à Geocropping. Pas de changement opéré."
        };
    }
    MyNotification.prototype.ConnectionOK = function () {
        this.box.show(this.notif.ConnectionOK);
    };
    MyNotification.prototype.ConnectionNOK = function () {
        this.box.show(this.notif.ConnectionNOK);
    };
    MyNotification.prototype.DisconnectionOK = function () {
        this.box.show(this.notif.DisconnectionOK);
    };
    MyNotification.prototype.DisconnectionNOK = function () {
        this.box.show(this.notif.DisconnectionNOK);
    };
    MyNotification.prototype.ValidatedOK = function () {
        this.box.show(this.notif.ValidatedOK);
    };
    MyNotification.prototype.ValidatedNOK = function () {
        this.box.show(this.notif.ValidatedNOK);
    };
    MyNotification.prototype.InvalidatedOK = function () {
        this.box.show(this.notif.InvalidatedOK);
    };
    MyNotification.prototype.InvalidatedNOK = function () {
        this.box.show(this.notif.InvalidatedNOK);
    };
    MyNotification.prototype.LocalisationNOK = function () {
        this.box.show(this.notif.LocalisationNOK);
    };
    MyNotification.prototype.AutoServerChanged = function () {
        this.box.show(this.notif.AutoServerChanged);
    };
    MyNotification.prototype.ServerChanged = function () {
        this.box.show(this.notif.ServerChanged);
    };
    MyNotification.prototype.ClicPositionON = function () {
        this.box.show(this.notif.ClicPositionON);
    };
    MyNotification.prototype.ClicPositionOFF = function () {
        this.box.show(this.notif.ClicPositionOFF);
    };
    MyNotification.prototype.ChangementsTrouve = function (changes) {
        if (changes.length === 0) { }
        else {
            var text = this.notif.ChangementsTrouve;
            for (var i = 0; i < changes.length; i++) {
                text += "<a style='color: blue; ' href=\"https://www.openstreetmap.org/edit?node=" + changes[i] + "\" target=\"_blank\"> " + changes[i] + "  </a>";
            }
            this.box.options.timeout += changes.length * 1000;
            this.box.show(text);
            this.box.options.timeout = 4000;
        }
    };
    MyNotification.prototype.AjoutSinglePrefNOK = function () {
        this.box.show(this.notif.AjoutSinglePrefNOK);
    };
    MyNotification.prototype.AjoutSinglePrefOK = function () {
        this.box.show(this.notif.AjoutSinglePrefOK);
    };
    MyNotification.prototype.SignalisationExterieur = function () {
        this.box.show(this.notif.SignalisationExterieur);
    };
    return MyNotification;
}());
//# sourceMappingURL=MyNotification.js.map