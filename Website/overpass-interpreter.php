<?php
$data = http_build_query($_REQUEST);//$_REQUEST['data'];
$baseUrl = 'https://osmoverpass.xsalto.com/api/interpreter';
//$baseUrl = 'https://overpass-api.de/api/interpreter';

$url = "$baseUrl?$data";

// informations nécessaire pour le requete
$options = [
    'http' => [
        'method' => 'GET',
        'ignore_errors' => true
    ]
];

$str = file_get_contents($url, false, stream_context_create($options));

foreach($http_response_header as $key=>$header){
    header($header);
}

echo($str);
